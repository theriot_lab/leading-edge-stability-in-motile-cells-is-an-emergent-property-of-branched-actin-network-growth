This MATLAB code was written by Rikki M. Garner as a companion to the paper "Leading edge stability in motile cells in an emergent property of branched actin network growth", available online at: https://www.biorxiv.org/content/10.1101/2020.08.22.262907v1

The code is split into two folders, one for analysis of the experimental data (LamellipodiumSegmentation_ForPublication) and one for the generation and analysis of simulated leading edges (LamellipodiumSimulation_ForPublication). See individual ReadMe files within each folder for more specific information.

Please feel free to contact me (Rikki) at rikkimgarner@gmail.com for advice and discussion. If you end up using this code, I would love to know about it!