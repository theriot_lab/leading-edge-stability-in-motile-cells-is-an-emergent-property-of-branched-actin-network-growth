%% This script plots the spatial Fourier mode autocorrelation analysis fit 
% parameters, binned and averaged over all replicates

% This code was written by Rikki M. Garner as a companion to the manuscript
% titled "Leading edge stability in motile cells in an emergent property of
% branched actin network growth", and was last updated 2020/08/19.

%% Clear the system
    close all;
    clear all;
    
% Choose the datasets to analyze  
    % Example for a defined set of datasets 
        % Choose the folders containing the data
            folderPaths = {['/Users/Rikki/Documents/'...
            'LamellipodiumSegmentation_ForPublication/ExampleMovie/'...
            '180111_HighMagKW_1PercentDMSOCtrl_Analyzed/']};        
        % Choose the dataset name
            datasetName = 'Experimentally measured leading edge';
        % Choose the condition number for each folder
            conditionByFolder = [1];
        % Choose the condition names
            conditionNames = {'Experimental data'};
        % Which order to plot the parameters in
            conditionOrder2Plot = 1;
     
                
	% Plot the summary statistics for the fitted parameters  
        % Aggregate the data by condition and calculate summary statistics
            % Initialize the parameters to run the code
                % Create a struct to store teh summary starts
                    autocorrDataConcatenated = struct();
                % Load data into the autocorr structure
                    % Dataset name
                        autocorrDataConcatenated.datasetName = datasetName;
                    % Folders containing the data
                        autocorrDataConcatenated.folderPaths = folderPaths;
                    % Condition number for each folder
                        autocorrDataConcatenated.conditionByFolder = conditionByFolder;
                    % Condition names
                        autocorrDataConcatenated.conditionNames = conditionNames;
                    % Which order to plot the parameters in
                        autocorrDataConcatenated.conditionOrder2Plot = conditionOrder2Plot;
                % Choose how to bin the data
                    % Example for binning by the median diff_fVal
                        autocorrDataConcatenated.binFlag='binByMedian';
%                     % Example for manual binning
%                         autocorrDataConcatenated.binFlag='binManually';
%                         autocorrDataConcatenated.manualspatialFrequencyBinSizeInInverseNanometers = 0.2*10^(-3);
            % Aggregate the data by condition        
                autocorrDataConcatenated = ...
                    prepSpatialFFTModeAutocorrResultsByCondition_Experiments(autocorrDataConcatenated);
            % Determine summary statistics for autocorrelation analysis 
            % by binning and averaging across conditions   
                autocorrDataConcatenated = makeAutocorrSummaryStats(autocorrDataConcatenated);
                
       %% Plot the results
            % Prepare the plotting parameters
                % Choose the numbner of rows
                    numRows = 1;
                % Choose the number of columns
                    numCols = 4;
                % Choose the subplots for this dataset
                    numSubplotScatterAmp = 1;
                    numSubplotScatterDecay = 2;
                    numSubplotSummaryStatsAmp = 3;
                    numSubplotSummaryStatsDecay = 4;
                % Choose the alpha value
                    alphaVal = min(1,0.03*40/autocorrDataConcatenated.numReplicatesByCondition(1));
            % Run the plotting code
                plotSpatialFFTModeAutocorrSummaryStats
                drawnow
                

    % Link all of the axes
  
        % Define the parameter display axes
        
            % Fitted Amplitudes
                rows = 1;
                cols = [1 3];
                numPlots=0;
                for  rowNum = rows
                    for colNum = cols
                        numPlots=numPlots+1;
                        subplotNum = colNum + (numCols*(rowNum-1));
                        hAxAmpAll(numPlots) = subplot(numRows,numCols,subplotNum);
                    end
                    
                end
                
            % Spatial frequency
                rows = 1;
                cols = [1:4];
                numPlots=0;
                for  rowNum = rows
                    for colNum = cols
                        numPlots=numPlots+1;
                        subplotNum = colNum + (numCols*(rowNum-1));
                        hAxSpatialFrequency(numPlots) = subplot(numRows,numCols,subplotNum);
                    end
                    
                end
                
            % Exponential decay rates
                rows = 1;
                cols = [2 4];
                numPlots=0;
                for  rowNum = rows
                    for colNum = cols
                        numPlots=numPlots+1;
                        subplotNum = colNum + (numCols*(rowNum-1));
                        hAxDecay(numPlots) = subplot(numRows,numCols,subplotNum);
                    end
                    
                end
            
        % Link the axes
            linkAmpAll = linkprop(hAxAmpAll,{'YLim','YTick','YTickLabels'});
            linkSpatialFrequency = linkprop(hAxSpatialFrequency,{'XLim','XTick','XTickLabels'});
            linkDecay = linkprop(hAxDecay,{'YLim','YTick','YTickLabels'});
            
%         % Chooose the limits
%             % FittedAmplitudes
%                 exponentValuesAmp = 3:7.5;
%                 yLimsAmp = [10^(exponentValuesAmp(1)) 10^(7.5)];
%                 yTicksAmp = 10.^(exponentValuesAmp);
%                 yTickLabelsAmp = cellstr(strcat(repmat('10^{',length(exponentValuesAmp),1),...
%                     string(exponentValuesAmp)',repmat('}',length(exponentValuesAmp),1)));       
%             % Spatial frequency
%                 xLimsSpatialFreq = [0.15 1.6];
%                 xtickSpatialFreq = [0.05 0.1 0.2 0.4 0.8 1.6 3.2 6.4]; 
%             % Decay rates
%                 exponentValuesDecayRate = -5:-2;
%                 yLimsDecayRate = [10^(exponentValuesDecayRate(1)) 10^(exponentValuesDecayRate(end))];
%                 yTicksDecayRate = 10.^(exponentValuesDecayRate);
%                 yTickLabelsDecayRate = cellstr(strcat(repmat('10^{',length(exponentValuesDecayRate),1),...
%                     string(exponentValuesDecayRate)',repmat('}',length(exponentValuesDecayRate),1)));        
        % Chooose the limits
            % FittedAmplitudes
                exponentValuesAmp = 2:7;
                yLimsAmp = [0.5*10^(exponentValuesAmp(1)) 10^(7.5)];
                yTicksAmp = 10.^(exponentValuesAmp);
                yTickLabelsAmp = cellstr(strcat(repmat('10^{',length(exponentValuesAmp),1),...
                    string(exponentValuesAmp)',repmat('}',length(exponentValuesAmp),1)));       
            % Spatial frequency
                xLimsSpatialFreq = [0.04 3.8];
                xtickSpatialFreq = [0.05 0.1 0.2 0.4 0.8 1.6 3.2 6.4]; 
            % Decay rates
                exponentValuesDecayRate = -5:-1;
                yLimsDecayRate = [0.5*10^(exponentValuesDecayRate(1)) 0.5*10^(exponentValuesDecayRate(end))];
                yTicksDecayRate = 10.^(exponentValuesDecayRate);
                yTickLabelsDecayRate = cellstr(strcat(repmat('10^{',length(exponentValuesDecayRate),1),...
                    string(exponentValuesDecayRate)',repmat('}',length(exponentValuesDecayRate),1)));
            
            
        % Update the axes
            figure(1)
            % Choose the x-ticks for the spatial frequency and y-ticks for the amplitude
                subplot(numRows,numCols,1)
                % Spatial freuqency
                    xlim(xLimsSpatialFreq)
                    xticks(xtickSpatialFreq)
                    xticklabels(xtickSpatialFreq)
                % Example Amplitudes  
                    ylim(yLimsAmp)
                    yticks(yTicksAmp);
                    yticklabels(yTickLabelsAmp);
            % Choose the y-ticks for the decay rate
                subplot(numRows,numCols,2)
                ylim(yLimsDecayRate)
                yticks(yTicksDecayRate);
                yticklabels(yTickLabelsDecayRate);
                 
        figHandle = figure(1);
        % Maximize the image
            figHandle = figure(1);
            figHandle.Position =  [31 659 1782 372];
%         % Move the legends
%             hLeg=findobj(figHandle,'type','legend');
%             set(hLeg,'Position',[0.0005 0.65 0.1 0.3])

%     %% Save the figure
%         % Choose the filename and path for the figure
%             destinationTrack = sprintf(['/Users/Rikki/Documents/'...
%                 '/LamellipodiumSimulation_ForPublication/Figures/'...
%                 'spatialFFTTimeAutocorrelationFitSummary_%s.eps'],...
%                 dataNameForPlottingPrefix);
%         % Save the file
%             print(gcf,destinationTrack,'-depsc','-painters') 
%             
%         % Save a png version
%             % Choose the filename and path for the figure
%             destinationTrack = sprintf(['/Users/Rikki/Documents/'...
%                 '/LamellipodiumSimulation_ForPublication/Figures/'...
%                 'spatialFFTTimeAutocorrelationFitSummary_%s.png'],...
%                 dataNameForPlottingPrefix); 
%             % Save the file
%                 print(gcf,destinationTrack,'-dpng','-painters') 
          

    

     

           

            
