function [yPosFit yPosSmooth] = segmentLEDeNovo(Image,yLimsBckgnd,xLimsBckgnd,plotResults)


%% This function takes an image and determines the position of the leading edge. 

% This code was written by Rikki M. Garner as a companion to the manuscript
% titled "Leading edge stability in motile cells in an emergent property of
% branched actin network growth", and was last updated 2020/08/19.

%% Plot example fittings with maxima finding, smoothing then maxima finding,...
    % or smoothing, maxima finding, minimal gradient finding, and averaging 
    
% % Plot results?
%     plotResults = 0;
%     
% % Choose the time point to plot
%     tp = 1;
    
% Read in the image for this time point
    
    if plotResults == 1
        % Display the image
            figure(2)
            subplot(2,3,1)
            imshow(Image,[]) 
            hold on;
            plot([0 0], [0 0])
            plot([0 0], [0 0])
    end
     
% Calculate the minimum and maximum background intensity values
    % maxYVal = 45;
    bckgndImage = Image(yLimsBckgnd(1):yLimsBckgnd(2),xLimsBckgnd(1):xLimsBckgnd(2));
    maxBckgnd = max(max(bckgndImage));
    minBckgnd = min(min(bckgndImage));
    
    if plotResults==1
        subplot(2,3,4)
        plot([1 size(Image,2)], [minBckgnd minBckgnd])
        hold on;
        plot([1 size(Image,2)], [maxBckgnd maxBckgnd])
    end
    
% Pre-allocate space for the smoothed image and gradient data
    smoothImageInYDir=[];
    smoothImageInYDirDiff=[];
    
% Calculate the smoothed image and its gradient
    for m = 1:size(Image,2)
        % Smooth the gradient of pixel intensity in the y-direction
            % Smooth the pixel intensity in the y-direction for this
            % x-position
                smoothImageInYDir(:,m) = smooth(Image(:,m),10,'lowess');
                smoothImageInYDirDiff(:,m) = diff(smoothImageInYDir(:,m));
    end

    if plotResults==1
        figure(2)
        subplot(2,3,2)
        imshow(smoothImageInYDir,[])    
        hold on;
        plot([0 0], [0 0])
        plot([0 0], [0 0])
       
    end
    
% Calculate the minimum and maximum smoothed background intensity values
    bckgndImageSmooth = smoothImageInYDir(yLimsBckgnd(1):yLimsBckgnd(2),xLimsBckgnd(1):xLimsBckgnd(2));
    maxBckgndSmooth = max(max(bckgndImageSmooth));
    minBckgndSmooth = min(min(bckgndImageSmooth));
    
        
    if plotResults==1
        subplot(2,3,5)
        plot([1 size(Image,2)], [minBckgndSmooth minBckgndSmooth])
        hold on;
        plot([1 size(Image,2)], [maxBckgndSmooth maxBckgndSmooth])

    end

    
    if plotResults==1
        figure(2)
        subplot(2,3,3)
        imshow(smoothImageInYDirDiff,[])    
        hold on;
        plot([0 0], [0 0])
        plot([0 0], [0 0])
    end

    
% Calculate the minimum and maximum smoothed background intensity gradient values
    bckgndImageSmoothGradient = diff(smoothImageInYDir(yLimsBckgnd(1):yLimsBckgnd(2),xLimsBckgnd(1):xLimsBckgnd(2)),1);
    maxBckgndSmoothGradient = max(max(bckgndImageSmoothGradient));
    minBckgndSmoothGradient = min(min(bckgndImageSmoothGradient));
    
    if plotResults==1
        subplot(2,3,6)
        plot([1 size(Image,2)], [minBckgndSmoothGradient minBckgndSmoothGradient])
        hold on;
        plot([1 size(Image,2)], [maxBckgndSmoothGradient maxBckgndSmoothGradient])
    end

% How many leading edge points to you want to calculate?
    if plotResults==0
        numLEPoints2Plot = size(Image,2);
    else
        numLEPoints2Plot = 20; 
    end

% Preallocate space to store fitted leaing edge point
    yPosFit = NaN(1,size(Image,2));
    
% Find the outline of the LE and plot over the image
    for m = 1:round(size(Image,2)/numLEPoints2Plot):size(Image,2)
      % Find the first intensity maxima above background 
            if plotResults==1
                subplot(2,3,1)
                plot([m m], [1 size(Image,1)])  

                subplot(2,3,2)
                plot([m m], [1 size(Image,1)]) 

                subplot(2,3,4)
                plot(Image(:,m))
                hold on
            
                posFirstValueAboveBackgnd = find(Image(:,m)>maxBckgnd,1,'first');
                [maxValue posMaxValue] = max(Image(posFirstValueAboveBackgnd:...
                    min(posFirstValueAboveBackgnd+25,size(Image,1)),m));
                posMaxValue = posMaxValue+posFirstValueAboveBackgnd-1;
                if ~isempty(posMaxValue)
                    subplot(2,3,4)
                    plot(posMaxValue,Image(posMaxValue,m),'k*')
                    subplot(2,3,1)
                    plot(m,posMaxValue,'k*')
                end
            end            
        % Find first smoothed intensity maxima above background 
            if plotResults==1
                subplot(2,3,5)
                plot(smoothImageInYDir(:,m))
                hold on
            end
            posFirstSmoothValueAboveBackgnd = find(smoothImageInYDir(:,m)>maxBckgndSmooth & (1:length(smoothImageInYDir(:,m)))'>max(yLimsBckgnd),1,'first');
            [maxSmoothValue posMaxSmoothValue] = max(smoothImageInYDir(posFirstSmoothValueAboveBackgnd:...
                min(posFirstSmoothValueAboveBackgnd+25,size(Image,1)),m));
            posMaxSmoothValue = posMaxSmoothValue+posFirstSmoothValueAboveBackgnd-1;
            if ~isempty(posMaxSmoothValue)
                if plotResults==1
                    subplot(2,3,5)
                    plot(posMaxSmoothValue,smoothImageInYDir(posMaxSmoothValue,m),'r*')
                    subplot(2,3,1)
                    plot(m,posMaxSmoothValue,'r*')
                    subplot(2,3,2)
                    plot(m,posMaxSmoothValue,'r*')
                end
            end
            
            if plotResults==1
                subplot(2,3,6)
                plot(smoothImageInYDirDiff(:,m))
                hold on
            end
            
      % Find position of lowest (most negative) gradient within 10 pixels of the smoothed intensity maxima
            
%             posFirstSmoothGradientValueBelowBckgnd = find(smoothImageInYDirDiff(:,m)<minBckgndSmoothGradient,1,'first');
%             [maxSmoothGradientValue posMaxSmoothGradientValue] = min(smoothImageInYDirDiff(posFirstSmoothGradientValueBelowBckgnd:...
%                 min(posFirstSmoothGradientValueBelowBckgnd+5,size(Image,1)-1),m));
%             posMaxSmoothGradientValue = posMaxSmoothGradientValue+posFirstSmoothGradientValueBelowBckgnd-1;

            if ~isempty(posMaxSmoothValue)
                [maxSmoothGradientValue posMaxSmoothGradientValue] = min(smoothImageInYDirDiff(posMaxSmoothValue:...
                    min(posMaxSmoothValue+10,size(Image,1)-1),m));
                posMaxSmoothGradientValue = posMaxSmoothValue+posMaxSmoothGradientValue-1;
                if ~isempty(posMaxSmoothGradientValue)
                    if plotResults==1
                    subplot(2,3,6)
                        plot(posMaxSmoothGradientValue,smoothImageInYDirDiff(posMaxSmoothGradientValue,m),'g*')
                        subplot(2,3,1)
                        plot(m,posMaxSmoothGradientValue,'g*')
                        subplot(2,3,3)
                        plot(m,posMaxSmoothGradientValue,'g*')
                        subplot(2,3,1)
                        plot(m,nanmean([posMaxSmoothValue,posMaxSmoothGradientValue]),'b*')
                    end
                    yPosFit(m) = nanmean([posMaxSmoothValue,posMaxSmoothGradientValue]);
                end
            end
                if plotResults==1
                    drawnow
                    pause(0.2)
                end
    end
    
    if plotResults==1
        subplot(2,3,1)
        hold off;
        subplot(2,3,2)
        hold off;    
        subplot(2,3,3)
        hold off;
        subplot(2,3,4)
        hold off;
        subplot(2,3,5)
        hold off;
        subplot(2,3,6)
        hold off;
    end
    
    % Smooth the data 
    yPosSmooth = smooth(yPosFit,10);
    
    if plotResults==1
        figure(3)
        imshow(Image,[])    
        hold on;
        plot(yPosFit,'b')
        plot(yPosSmooth,'r')
        hold off;  
    end

    
     