function [yPosFitTemp, yPosSmoothTemp] = segmentLEBasedOnPreviousTimepoint(Image,yLimsBckgnd,xLimsBckgnd,yPosSmoothPrevious,plotResults)

%% This function takes an image and determines the position of the leading edge. 

% This code was written by Rikki M. Garner as a companion to the manuscript
% titled "Leading edge stability in motile cells in an emergent property of
% branched actin network growth", and was last updated 2020/08/19.

%% Plot example fittings with maxima finding, smoothing then maxima finding,...
    % or smoothing, maxima finding, minimal gradient finding, and averaging 

        % Display the image for this time point
            if plotResults == 1
                
                % Display the image
                
                    % Pre-allocate space to store the linked axes
                        hAxIm = [];
                        hAxLine = [];       
                    figure(4)
                    hAxIm(1) = subplot(2,3,1);
                    imshow(Image,[]) 
                    hold on;
                    plot(yPosSmoothPrevious,'b')
                    title('Raw Image')
                    set(gca,'FontName','Arial','FontSize',18,'FontWeight','Bold'); 
                    plot([0 0], [0 0])
                    plot([0 0], [0 0])
            end
     
        % Pull out the minimum and maximum background intensity values
            bckgndImage = Image(yLimsBckgnd(1):yLimsBckgnd(2),xLimsBckgnd(1):xLimsBckgnd(2));
            maxBckgnd = max(max(bckgndImage));
            minBckgnd = min(min(bckgndImage));

            % Plot the background limits for this time point
                if plotResults==1
                    figure(4)
                    hAxLine(1) = subplot(2,3,4);
                    plot([1 size(Image,2)], [minBckgnd minBckgnd],'r')
                    hold on;
                    plot([1 size(Image,2)], [maxBckgnd maxBckgnd],'b')
                    set(gca,'FontName','Arial','FontSize',18,'FontWeight','Bold'); 
                end
         
    % Lowess-smooth the image in the y-direction
    
        % Pre-allocate space for the smoothed image and gradient data
            smoothImageInYDir=[];
            smoothImageInYDirDiff=[];

        % Calculate the smoothed image and its gradient
            for m = 1:size(Image,2)
                % Smooth the gradient of pixel intensity in the y-direction
                    % Smooth the pixel intensity in the y-direction for this
                    % x-position
                        smoothImageInYDir(:,m) = smooth(Image(:,m),30,'loess');
                        smoothImageInYDirDiff(:,m) = diff(smoothImageInYDir(:,m));
                        smoothImageInYDirDiffDiff(:,m) = diff(smoothImageInYDirDiff(:,m));
            end


        % Display the smoothed image and bacgkround values
        
            % Display the smoothed image for this time point
                if plotResults==1
                    figure(4)
                    hAxIm(2) = subplot(2,3,2);
                    imshow(smoothImageInYDir,[])
                        hold on;
                        plot(yPosSmoothPrevious,'b')
                    title({'Y-Lowess-smoothed Image','Window = 10 pixels'})
                    set(gca,'FontName','Arial','FontSize',18,'FontWeight','Bold'); 
                    plot([0 0], [0 0])
                    plot([0 0], [0 0])

                end
    
            % Calculate the minimum and maximum smoothed background intensity values
                bckgndImageSmooth = smoothImageInYDir(yLimsBckgnd(1):yLimsBckgnd(2),xLimsBckgnd(1):xLimsBckgnd(2));
                maxBckgndSmooth = max(max(bckgndImageSmooth));
                minBckgndSmooth = min(min(bckgndImageSmooth));


                if plotResults==1
                    figure(4)
                    hAxLine(2) = subplot(2,3,5);
                    plot([1 size(Image,2)], [minBckgndSmooth minBckgndSmooth])
                    hold on;
                    plot([1 size(Image,2)], [maxBckgndSmooth maxBckgndSmooth])

                end

        % Display the derivative of the smoothed image for this time point and its background values    
            % Display the derivative of the smoothed image for this time point
                if plotResults==1
                    figure(4)
                    hAxIm(3) = subplot(2,3,3);
                    imshow(smoothImageInYDirDiff,[])
                        hold on;
                        plot(yPosSmoothPrevious,'b')   
                    title({'Y-derivative of Smoothed Image'})
                    set(gca,'FontName','Arial','FontSize',18,'FontWeight','Bold'); 
                    plot([0 0], [0 0])
                    plot([0 0], [0 0])
                end

            % Calculate the minimum and maximum smoothed background intensity gradient values
                bckgndImageSmoothGradient = diff(smoothImageInYDir(yLimsBckgnd(1):yLimsBckgnd(2),xLimsBckgnd(1):xLimsBckgnd(2)),1);
                maxBckgndSmoothGradient = max(max(bckgndImageSmoothGradient));
                minBckgndSmoothGradient = min(min(bckgndImageSmoothGradient));

                if plotResults==1
                    figure(4)
                    hAxLine(3) = subplot(2,3,6);
                    plot([1 size(Image,2)], [minBckgndSmoothGradient minBckgndSmoothGradient])
                    hold on;
                    plot([1 size(Image,2)], [maxBckgndSmoothGradient maxBckgndSmoothGradient])
                    
            % Link the axes for alll of these plots
                linkaxes(hAxIm,'xy')
                linkaxes(hAxLine,'x')
                end
                
%         % Display the derivative of the derivative of smoothed image for this time point and its background values    
%             % Calculate the minimum and maximum smoothed background intensity gradient values
%                 bckgndImageSmoothGradientGradient = diff(smoothImageInYDirDiffDiff(yLimsBckgnd(1):yLimsBckgnd(2),xLimsBckgnd(1):xLimsBckgnd(2)),1);
%                 maxBckgndSmoothGradientGradient = max(max(bckgndImageSmoothGradientGradient));
%                 minBckgndSmoothGradientGradient = min(min(bckgndImageSmoothGradientGradient));
% 
%                 if plotResults==1
%                     figure(5)
%                     plot([1 size(Image,2)], [minBckgndSmoothGradientGradient minBckgndSmoothGradientGradient])
%                     hold on;
%                     plot([1 size(Image,2)], [maxBckgndSmoothGradientGradient maxBckgndSmoothGradientGradient])
%                 end
                
            

% How many leading edge points to you want to calculate?
    if plotResults==0
        numLEPoints2Plot = size(Image,2);
        mVals = 1:round(size(Image,2)/numLEPoints2Plot):size(Image,2);
    else
        %numLEPoints2Plot = 100; 
        %mVals = 1:round(size(Image,2)/numLEPoints2Plot):size(Image,2);
        mVals = 450;
        %mVals = 223:265; 
                         CM = cool(length(mVals));
                         CMNum = 1;
        
    end

% Preallocate space to store fitted leading edge point
    yPosFitTemp = NaN(1,size(Image,2));
    
% Find the outline of the LE and plot over the image
    for m = mVals
        m;
    %for m = 192   
    %for m = [266 297]
        % Preallocate vectors to store the position of the maximum raw
        % intensity and smoothed intensity, and the maximum smoothed
        % intensity gradient magnitude
            posMaxValue=[];
            posMaxSmoothValue=[];
            posMaxSmoothGradientValue=[];
        
        % Find the first intensity maxima above background 
            if plotResults==1
                
                % Plot a line for the x-position we're fitting
                    figure(4)
                    subplot(2,3,1)
                    plot([m m], [1 size(Image,1)],'color', CM(CMNum,:))  
                    subplot(2,3,2)
                    plot([m m], [1 size(Image,1)],'color', CM(CMNum,:)) 
                    subplot(2,3,3)
                    plot([m m], [1 size(Image,1)],'color', CM(CMNum,:)) 

                % Plot the intensity values for this x-position 
                    figure(4)
                    subplot(2,3,4)
                    plot(Image(:,m),'color', CM(CMNum,:))
                    ylabel('Raw Intensity')
                    xlabel('Y-position')
                    title({'Raw intensity line profile'})
                    hold on
            
                % (Not used for calculation, only for visualization
                    % Find the first pixel above background
                        posFirstValueAboveBackgnd = find(Image(:,m)>maxBckgnd,1,'first');
                    % Within a window of this region, find the maximum
                    % intensity  value and position relative to this max
                    % value
                        [maxValue posMaxValue] = max(Image(posFirstValueAboveBackgnd:...
                            min(posFirstValueAboveBackgnd+25,size(Image,1)),m));
                    % Find the absolute position
                        posMaxValue = posMaxValue+posFirstValueAboveBackgnd-1;
                    
                % If there was a value found, plot it    
                    if ~isempty(posMaxValue)
                        subplot(2,3,4)
                        plot(posMaxValue,Image(posMaxValue,m),'k*')
                        ylabel('Raw Intensity')
                        xlabel('Y-position')
                        title({'Raw intensity line profile'})
                        set(gca,'FontName','Arial','FontSize',18,'FontWeight','Bold'); 
                        subplot(2,3,5)
                        plot(posMaxValue,smoothImageInYDir(posMaxValue,m),'k*')
                        ylabel('Smoothed Intensity')
                        xlabel('Y-position')
                        title({'Raw intensity line profile'})
                        set(gca,'FontName','Arial','FontSize',18,'FontWeight','Bold'); 
                        subplot(2,3,6)
                        plot(posMaxValue,smoothImageInYDirDiff(posMaxValue,m),'k*')
                        ylabel('Derivative')
                        xlabel('Y-position')
                        title({'Raw intensity line profile'})
                        set(gca,'FontName','Arial','FontSize',18,'FontWeight','Bold'); 
                        
                        subplot(2,3,1)
                        plot(m,posMaxValue,'k*')
                        subplot(2,3,2)
                        plot(m,posMaxValue,'k*')
                        subplot(2,3,3)
                        plot(m,posMaxValue,'k*')
                    end
                    
            end
            
        % Find first smoothed intensity maxima above background
        
            % Plot the smoothed intensity values for this x-position
                if plotResults==1
                    figure(4)
                    subplot(2,3,5)
                    plot(smoothImageInYDir(:,m),'color', CM(CMNum,:))
                    hold on
                end
                
            % Within a window of this region, find the maximum
            % intensity  value and position relative to this max
            % value
                [maxSmoothValue posMaxSmoothValue] = max(smoothImageInYDir(max(round(yPosSmoothPrevious(m))-30,1):...
                    min(round(yPosSmoothPrevious(m))+5,size(Image,1)),m));
            % Find the absolute position
                posMaxSmoothValue = posMaxSmoothValue+max(round(yPosSmoothPrevious(m))-30,1)-1;
                
                
            % If there was a value found, plot it    
                if ~isempty(posMaxSmoothValue)
                    if plotResults==1
                        figure(4)
                        subplot(2,3,4)
                        plot(posMaxSmoothValue,Image(posMaxSmoothValue,m),'r*')
                        subplot(2,3,5)
                        plot(posMaxSmoothValue,smoothImageInYDir(posMaxSmoothValue,m),'r*')
                        subplot(2,3,6)
                        plot(posMaxSmoothValue,smoothImageInYDirDiff(posMaxSmoothValue,m),'r*')
                        
                        subplot(2,3,1)
                        plot(m,posMaxSmoothValue,'r*')
                        subplot(2,3,2)
                        plot(m,posMaxSmoothValue,'r*')
                        subplot(2,3,3)
                        plot(m,posMaxSmoothValue,'r*')
                    end
                end
                
        % Find first smoothed intensity gradient magnitude maxima above background
        
            % Plot the smoothed intensity values for this x-position
                if plotResults==1
                    figure(4)
                    subplot(2,3,6)
                    plot(smoothImageInYDirDiff(:,m),'color', CM(CMNum,:))
                    hold on
                end
%                 if plotResults==1
%                     figure(5)
%                     plot(smoothImageInYDirDiffDiff(:,m),'color', CM(CMNum,:))
%                         ylabel('Derivative of Derivatie')
%                         xlabel('Y-position')
%                         title({'Derivative of Derivative Line Profile'})
%                         set(gca,'FontName','Arial','FontSize',18,'FontWeight','Bold'); 
%                     hold on
%                 end
            
      % Find position of lowest (most negative) gradient within 10 pixels of the smoothed intensity maxima
            
%             posFirstSmoothGradientValueBelowBckgnd = find(smoothImageInYDirDiff(:,m)<minBckgndSmoothGradient,1,'first');
%             [maxSmoothGradientValue posMaxSmoothGradientValue] = min(smoothImageInYDirDiff(posFirstSmoothGradientValueBelowBckgnd:...
%                 min(posFirstSmoothGradientValueBelowBckgnd+5,size(Image,1)-1),m));
%             posMaxSmoothGradientValue = posMaxSmoothGradientValue+posFirstSmoothGradientValueBelowBckgnd-1;

            if ~isempty(posMaxSmoothValue)
                
                % Find the minimum (most negative) gradient 
                    [maxSmoothGradientValue, posMaxSmoothGradientValue] = min(smoothImageInYDirDiff(posMaxSmoothValue:...
                        min(posMaxSmoothValue+15,size(Image,1)-1),m));
                    posMaxSmoothGradientValue = posMaxSmoothValue+posMaxSmoothGradientValue-1;
                    
                % Re-find the max smoothed intensity anywhere in between
                % original smoothed peak and gradient peak
                    if ~isempty(posMaxSmoothValue) && ~isempty(posMaxSmoothGradientValue) && abs(posMaxSmoothGradientValue-posMaxSmoothValue)>2
                      % Find all peaks between the intensity peak and the steepest gradient
          	                 [maxSmoothValueRedo,posMaxSmoothValueRedo] = findpeaks(smoothImageInYDir(posMaxSmoothValue:posMaxSmoothGradientValue,m));
                        % If there are any peaks, take the last one
                            if any(posMaxSmoothValueRedo)
                                posMaxSmoothValueRedo = posMaxSmoothValueRedo(end);
                                posMaxSmoothValueRedo = posMaxSmoothValue+posMaxSmoothValueRedo-1;
                            else
                                posMaxSmoothValueRedo = posMaxSmoothValue;
                            end
                    else
                        posMaxSmoothValueRedo = posMaxSmoothValue;
                    end
                
                % Determine the position at which the gradient passes through 0
                idxStart = max(1,(posMaxSmoothValueRedo-5));
                idxEnd = min(size(Image,1)-1,(posMaxSmoothValueRedo+2));
                    smoothImageInYDirDiff(idxStart:idxEnd,m);
                    posMaxSmoothValueRedoRedo = find(smoothImageInYDirDiff(idxStart:idxEnd,m)<0,1,'first');
                     posMaxSmoothValueRedoRedo = posMaxSmoothValueRedoRedo + posMaxSmoothValueRedo - 6 ;
                    %                   x = (0:10)'; 
                    x = smoothImageInYDirDiff(idxStart:idxEnd,m);
                    y = idxStart:idxEnd;
                    xi = 0;
                    posMaxSmoothValueRedoRedo = interp1(x,y,xi);
                    %ginput
                    
                % If a min was found, plot it
                    if ~isempty(posMaxSmoothGradientValue)
                        
                        if plotResults==1
                            
                            
                            figure(4)
                            subplot(2,3,4)
                            plot(posMaxSmoothGradientValue,Image(posMaxSmoothGradientValue,m),'g*')
                            subplot(2,3,5)
                            plot(posMaxSmoothGradientValue,smoothImageInYDir(posMaxSmoothGradientValue,m),'g*')
                            subplot(2,3,6)
                            plot(posMaxSmoothGradientValue,smoothImageInYDirDiff(posMaxSmoothGradientValue,m),'g*')
                            
                            subplot(2,3,1)
                            plot(m,posMaxSmoothGradientValue,'g*')
                            subplot(2,3,2)
                            plot(m,posMaxSmoothGradientValue,'g*')
                            subplot(2,3,3)
                            plot(m,posMaxSmoothGradientValue,'g*')
                            
                            figure(4)
                            subplot(2,3,4)
                            plot(posMaxSmoothValueRedo,Image(posMaxSmoothValueRedo,m),'m*')
                            subplot(2,3,5)
                            plot(posMaxSmoothValueRedo,smoothImageInYDir(posMaxSmoothValueRedo,m),'m*')
                            subplot(2,3,6)
                            plot(posMaxSmoothValueRedo,smoothImageInYDirDiff(posMaxSmoothValueRedo,m),'m*')
                            
                            subplot(2,3,1)
                            plot(m,posMaxSmoothValueRedo,'m*')
                            subplot(2,3,2)
                            plot(m,posMaxSmoothValueRedo,'m*')
                            subplot(2,3,3)
                            plot(m,posMaxSmoothValueRedo,'m*')
                            
%                             figure(4)
%                             subplot(2,3,4)
%                             plot(posMaxSmoothValueRedoRedo,Image(round(posMaxSmoothValueRedoRedo),m),'md')
%                             subplot(2,3,5)
%                             plot(posMaxSmoothValueRedoRedo,smoothImageInYDir(round(posMaxSmoothValueRedoRedo),m),'md')
%                             subplot(2,3,6)
%                             plot(posMaxSmoothValueRedoRedo,smoothImageInYDirDiff(round(posMaxSmoothValueRedoRedo),m),'md')
%                             
%                             subplot(2,3,1)
%                             plot(m,posMaxSmoothValueRedoRedo,'md')
%                             subplot(2,3,2)
%                             plot(m,posMaxSmoothValueRedoRedo,'md')
%                             subplot(2,3,3)
%                             plot(m,posMaxSmoothValueRedoRedo,'md')
                                                      
                            subplot(2,3,1)
%                             plot(m,nanmean([2*posMaxSmoothValueRedoRedo/3,4*posMaxSmoothGradientValue/3]),'b*')
%                             subplot(2,3,2)
%                             plot(m,nanmean([2*posMaxSmoothValueRedoRedo/3,4*posMaxSmoothGradientValue/3]),'b*')
%                             subplot(2,3,3)
%                             plot(m,nanmean([2*posMaxSmoothValueRedoRedo/3,4*posMaxSmoothGradientValue/3]),'b*')  
%                             
                            subplot(2,3,1)
                            plot(m,nanmean([posMaxSmoothValueRedo,posMaxSmoothGradientValue]),'b*')
                            subplot(2,3,2)
                            plot(m,nanmean([posMaxSmoothValueRedo,posMaxSmoothGradientValue]),'b*')
                            subplot(2,3,3)
                            plot(m,nanmean([posMaxSmoothValueRedo,posMaxSmoothGradientValue]),'b*')
                            
%                             figure(6)
%                             plot(smoothImageInYDir(:,m),'r')
%                             hold on;
%                             plot(smoothImageInYDirDiff(:,m),'b')
%                             plot([nanmean([posMaxSmoothValue,posMaxSmoothGradientValue]) nanmean([posMaxSmoothValue,posMaxSmoothGradientValue])],ylim,'k')
%                             plot([posMaxSmoothValue posMaxSmoothValue],ylim,'k-')
%                             plot([posMaxSmoothGradientValue posMaxSmoothGradientValue],ylim,'k-o')
%                             hold off;
                            %ginput
                            
                        end
                        
                       % yPosFitTemp(m) = nanmean([2*posMaxSmoothValueRedoRedo/3,4*posMaxSmoothGradientValue/3]);
                         yPosFitTemp(m) = nanmean([posMaxSmoothValueRedo,posMaxSmoothGradientValue]);
                    end
                    
            end
            

                if plotResults==1
                    drawnow
                    pause(0.5)
                    CMNum = CMNum + 1;
                end
        
        
%                     % Plot the smoothed intensity values for this x-position
%                 if plotResults==1
%                     figure(100)
%                     hAxLine(1) = subplot(1,4,1);
%                     plot(Image(:,m))
%                     idx2Smooth = 5:min(40,size(Image,1));
%                     span2Smooth = idx2Smooth./size(Image,1);
%                     CMNumSubset = 1;
%                     CMSubset = jet(length(idx2Smooth));
%                     diff = 0;
%                     n=1;
%                     for n = 1:length(idx2Smooth)
%                         s = length(idx2Smooth)-n+1;
%                        idx2Smooth(s)
%                         
%                         smoothImageInYDir = diff(smooth(Image(:,m),span2Smooth(s),'loess'));
%                         smoothImageInYDirDiff = diff(smoothImageInYDir);
%                         % Find the minimum (most negative) gradient 
%                             [maxSmoothGradientValue, posMaxSmoothGradientValue] = min(smoothImageInYDirDiff(posMaxSmoothValue:...
%                                 min(posMaxSmoothValue+15,size(Image,1)-1),m));
%                             posMaxSmoothGradientValue = posMaxSmoothValue+posMaxSmoothGradientValue-1;
%                         
%                         figure(100)
%                         hAxLine(2) = subplot(1,4,2);
%                         plot(smoothImageInYDir,'color', CMSubset(CMNumSubset,:));
%                         hold on
%                         hAxLine(3) = subplot(1,4,3);
%                         plot((1:(size(Image,1)-1))+0.5,smoothImageInYDirDiff,'color', CMSubset(CMNumSubset,:))
%                         hold on
%                         hAxLine(4) = subplot(1,4,4);
%                         plot(2:(size(Image,1)-1),diff(smoothImageInYDirDiff),'color', CMSubset(CMNumSubset,:))
%                         hold on
%                         CMNumSubset = CMNumSubset+1;
%                         drawnow
%                         %ginput
%                     end
%                     linkaxes(hAxLine,'x')
%                     figure(100)
%                     subplot(1,4,1)
%                     hold off;
%                     subplot(1,4,2)
%                     hold off;
%                     subplot(1,4,3)
%                     hold off;
%                     subplot(1,4,4)
%                     hold off;
%                     ginput
%                 end

% %                 % Run down the smoothing window until the negative peak
% %                 % position gets erratic
% %                 
% %                     % Choose the smoothing windows to scan
% %                         idx2Smooth = 1:min(40,size(Image,1));
% %                         span2Smooth = idx2Smooth./size(Image,1);
% %                         
% %                     % Find the first negative peak  using the largest
% %                     % smoothing window
% %                         % Pull out the largest smoothing window
% %                             s = length(idx2Smooth);
% %                         % Perform the smoothing
% %                             smoothImageInYDir(:,m) = smooth(Image(:,m),span2Smooth(s),'lowess');
% %                         % Perform the derivative
% %                             smoothImageInYDirDiff(:,m) = diff(smoothImageInYDir(:,m));
% %                         % Perform the second derivative
% %                             smoothImageInYDirDiffDiff(:,m) = diff(smoothImageInYDirDiff(:,m));
% %                         % Find the minimum (most negative) gradient
% %                             % Find the negative peak in the derivative
% %                                 [maxSmoothGradientValue, posMaxSmoothGradientValue] = min(smoothImageInYDirDiff(posMaxSmoothValue:...
% %                                     min(posMaxSmoothValue+15,size(Image,1)-1),m));
% %                                 posMaxSmoothGradientValuePrevious = posMaxSmoothValue+posMaxSmoothGradientValue;
% %                             % Find where the 2nd derivative passes through
% %                             % zero
% %                                 idxStart = max(1,(posMaxSmoothGradientValuePrevious-5));
% %                                 idxEnd = min(size(Image,1)-1,(posMaxSmoothGradientValuePrevious+5));
% %                                     if any(diff(smoothImageInYDirDiffDiff(idxStart:idxEnd,m))<0)||~any(smoothImageInYDirDiffDiff(idxStart:idxEnd,m)>0)
% %                                         idx2Smooth = nan;
% %                                         posMaxSmoothGradientValue = nan;
% % 
% %                                         if plotResults==1
% %                                             'No initial fit'
% %                                             idx2Smooth
% %                                             posMaxSmoothGradientValue
% %                                         end
% %                                         continue
% %                                     end
% %                                 smoothImageInYDirDiffDiff(idxStart:idxEnd,m);
% %                                 idxStartNew = find(smoothImageInYDirDiffDiff(idxStart:idxEnd,m)<0,1,'last');
% %                                 idxStart = idxStart + idxStartNew - 1;
% %                                 idxEnd = idxStart+1;
% %                                 x = smoothImageInYDirDiffDiff(idxStart:idxEnd,m);
% %                                 y = idxStart:idxEnd;
% %                                 xi = 0;
% %                                 posMaxSmoothGradientValuePrevious = interp1(x,y,xi);
% %                         % Find the maximum intensity 
% %                             % Find the maximum intensity
% %                                 [maxSmoothValuePrevious, posMaxSmoothValuePrevious] = max(smoothImageInYDir(max(1,posMaxSmoothValue-15):...
% %                                     ceil(posMaxSmoothGradientValuePrevious),m));
% %                                 posMaxSmoothValuePrevious = max(1,posMaxSmoothValue-15)+posMaxSmoothValuePrevious-0.5;
% %                             % Find where the 1st derivative passes through
% %                             % zero
% %                                 idxStart = max(1,(posMaxSmoothValuePrevious-5));
% %                                 idxEnd = min(size(Image,1)-1,(posMaxSmoothValuePrevious+5));
% %                                     if any(diff(smoothImageInYDirDiff(idxStart:idxEnd,m))>0)
% %                                         idx2Smooth = nan;
% %                                         posMaxSmoothGradientValue = nan;
% % 
% %                                         if plotResults==1
% %                                             'No initial fit'
% %                                             idx2Smooth
% %                                             posMaxSmoothGradientValue
% %                                         end
% %                                         continue
% %                                     end
% %                                 idxStartNew = find(smoothImageInYDirDiff(idxStart:idxEnd,m)>0,1,'last');
% %                                 idxStart = idxStart + idxStartNew - 1;
% %                                 idxEnd = idxStart+1;
% %                                 x = smoothImageInYDirDiff(idxStart:idxEnd,m);
% %                                 y = idxStart:idxEnd;
% %                                 xi = 0;
% %                                 posMaxSmoothValuePrevious = interp1(x,y,xi);
% %                         % Optionally plot the intensity values            
% %                             if plotResults==1
% %                                 % Set up the colormap
% %                                     CMNumSubset = 1;
% %                                     CMSubset = jet(length(idx2Smooth));
% %                                 % Plot the line profile
% %                                     figure(100)
% %                                     hAxLine(1) = subplot(1,4,1);
% %                                     plot(Image(:,m))  
% %                                     
% %                                 figure(100)
% %                                 hAxLine(2) = subplot(1,4,2);
% %                                 plot(smoothImageInYDir(:,m),'color', CMSubset(CMNumSubset,:));
% %                                 hold on
% %                                 plot(posMaxSmoothValuePrevious,smoothImageInYDir(round(posMaxSmoothValuePrevious),m),'m*')
% %                                 plot(posMaxSmoothGradientValuePrevious,smoothImageInYDir(round(posMaxSmoothGradientValuePrevious-0.5),m),'g*')
% %                                 hAxLine(3) = subplot(1,4,3);
% %                                 plot((1:(size(Image,1)-1))+0.5,smoothImageInYDirDiff(:,m),'color', CMSubset(CMNumSubset,:))
% %                                 hold on
% %                                 plot(posMaxSmoothValuePrevious,0,'m*')
% %                                 plot(posMaxSmoothGradientValuePrevious,smoothImageInYDirDiff(round(posMaxSmoothGradientValuePrevious-0.5),m),'g*')
% %                                 hAxLine(4) = subplot(1,4,4);
% %                                 plot(2:(size(Image,1)-1),diff(smoothImageInYDirDiff(:,m)),'color', CMSubset(CMNumSubset,:))
% %                                 hold on
% %                                 plot(posMaxSmoothValuePrevious,smoothImageInYDirDiffDiff(round(posMaxSmoothValuePrevious),m),'m*')
% %                                 plot(posMaxSmoothGradientValuePrevious,0,'g*')
% %                                 drawnow
% %                                 CMNumSubset = CMNumSubset+1;
% %                                 %ginput
% %                             end
% % 
% %                     % Set up the housekeeping parameters    
% %                         diffInNegPeak = 0;
% %                     for n = 2:length(idx2Smooth)  
% %                         
% %                         % Pull out the next largest smoothing window
% %                             s = length(idx2Smooth)-n+1;
% %                         % Perform the smoothing
% %                             smoothImageInYDir(:,m) = smooth(Image(:,m),span2Smooth(s),'lowess');
% %                         % Perform the derivative
% %                             smoothImageInYDirDiff(:,m) = diff(smoothImageInYDir(:,m));
% %                         % Perform the second derivative
% %                             smoothImageInYDirDiffDiff(:,m) = diff(smoothImageInYDirDiff(:,m));
% %                         % Find the minimum (most negative) gradient
% %                             % Find the negative peak in the derivative
% %                                 [maxSmoothGradientValue, posMaxSmoothGradientValue] = min(smoothImageInYDirDiff(posMaxSmoothValuePrevious:...
% %                                     min(posMaxSmoothValuePrevious+15,size(Image,1)-1),m));
% %                                 posMaxSmoothGradientValue = posMaxSmoothValuePrevious+posMaxSmoothGradientValue;
% %                             % Find where the 2nd derivative passes through
% %                             % zero
% %                                 idxStart = max(1,(floor(posMaxSmoothGradientValue)-2));
% %                                 idxEnd = min(size(Image,1)-1,(ceil(posMaxSmoothGradientValue)+2));
% %                                 
% %                                     if any(diff(smoothImageInYDirDiffDiff(idxStart:idxEnd,m))<0)||~any(smoothImageInYDirDiffDiff(idxStart:idxEnd,m)>0)
% %                                     %if diffInNegPeak<(-1)
% %                                     %if abs(diffInNegPeak)>1
% %                                         idx2Smooth = length(idx2Smooth)-n+2;
% %                                         posMaxSmoothGradientValue = posMaxSmoothGradientValuePrevious;
% %                                         posMaxSmoothValue = posMaxSmoothValuePrevious;
% % 
% %                                         if plotResults==1
% %                                             'Not all 2nd derivatives were positive'
% %                                             idx2Smooth
% %                                             posMaxSmoothGradientValue
% %                                         end
% %                                         break
% %                                     end
% %                             
% %                                 smoothImageInYDirDiffDiff(idxStart:idxEnd,m);
% %                                 idxStartNew = find(smoothImageInYDirDiffDiff(idxStart:idxEnd,m)>0,1,'first')-1;
% %                                 idxStart = idxStart + idxStartNew - 1;
% %                                 idxEnd = idxStart+1;
% %                                 x = smoothImageInYDirDiffDiff(idxStart:idxEnd,m);
% %                                 y = idxStart:idxEnd;
% %                                 xi = 0;
% %                                 posMaxSmoothGradientValue = interp1(x,y,xi);
% %                         % Find the maximum intensity 
% %                             % Find the maximum intensity
% %                                 [maxSmoothValue, posMaxSmoothValueRedo] = max(smoothImageInYDir(max(1,floor(posMaxSmoothValuePrevious)-15):...
% %                                     ceil(posMaxSmoothGradientValuePrevious),m));
% %                                 posMaxSmoothValue = max(1,posMaxSmoothValuePrevious-15)+posMaxSmoothValueRedo-0.5;
% %                             % Find where the 1st derivative passes through
% %                             % zero
% %                                 idxStart = max(1,(posMaxSmoothValue-2));
% %                                 idxEnd = min(size(Image,1)-1,(posMaxSmoothValue+2));
% %                                     if any(diff(smoothImageInYDirDiff(idxStart:idxEnd,m))>0)
% %                                         idx2Smooth = length(idx2Smooth)-n+2;
% %                                         posMaxSmoothGradientValue = posMaxSmoothGradientValuePrevious;
% %                                         posMaxSmoothValue = posMaxSmoothValuePrevious;
% % 
% %                                         if plotResults==1
% %                                             'Not all 1st derivatives were negative'
% %                                             idx2Smooth
% %                                             posMaxSmoothGradientValue
% %                                         end
% %                                         break
% %                                     end
% %                                 idxStartNew = find(smoothImageInYDirDiff(idxStart:idxEnd,m)>0,1,'last');
% %                                 idxStart = idxStart + idxStartNew - 1;
% %                                 idxEnd = idxStart+1;
% %                                 x = smoothImageInYDirDiff(idxStart:idxEnd,m);
% %                                 y = idxStart:idxEnd;
% %                                 xi = 0;
% %                                 posMaxSmoothValue = interp1(x,y,xi);
% %                                 
% %                         % Optionally plot the intensity values            
% %                             if plotResults==1
% %                                 figure(100)
% %                                 hAxLine(2) = subplot(1,4,2);
% %                                 plot(smoothImageInYDir(:,m),'color', CMSubset(CMNumSubset,:));
% %                                 hold on
% %                                 plot(posMaxSmoothValue,smoothImageInYDir(round(posMaxSmoothValue),m),'m*')
% %                                 plot(posMaxSmoothGradientValue,smoothImageInYDir(round(posMaxSmoothGradientValue-0.5),m),'g*')
% %                                 hAxLine(3) = subplot(1,4,3);
% %                                 plot((1:(size(Image,1)-1))+0.5,smoothImageInYDirDiff(:,m),'color', CMSubset(CMNumSubset,:))
% %                                 hold on
% %                                 plot(posMaxSmoothValue,0,'m*')
% %                                 plot(posMaxSmoothGradientValue,smoothImageInYDirDiff(round(posMaxSmoothGradientValue-0.5),m),'g*')
% %                                 hAxLine(4) = subplot(1,4,4);
% %                                 plot(2:(size(Image,1)-1),diff(smoothImageInYDirDiff(:,m)),'color', CMSubset(CMNumSubset,:))
% %                                 hold on
% %                                 plot(posMaxSmoothValue,smoothImageInYDirDiffDiff(round(posMaxSmoothValue),m),'m*')
% %                                 plot(posMaxSmoothGradientValue,0,'g*')
% %                                 drawnow
% %                                 CMNumSubset = CMNumSubset+1;
% %                                 %ginput
% %                             end
% %                             
% %                         % Calculate the difference between this peak and the previous peak position   
% %                             diffInPosPeak =  posMaxSmoothValue-posMaxSmoothValuePrevious;
% %                             diffInNegPeak = posMaxSmoothGradientValue-posMaxSmoothGradientValuePrevious;
% %                             if diffInNegPeak>1||diffInNegPeak<(-2)||diffInPosPeak<(-1)||diffInPosPeak>2
% %                             %if diffInNegPeak<(-1)
% %                             %if abs(diffInNegPeak)>1
% %                                 idx2Smooth = length(idx2Smooth)-n+2;
% %                                 posMaxSmoothGradientValue = posMaxSmoothGradientValuePrevious;
% %                                 posMaxSmoothValue = posMaxSmoothValuePrevious;
% %                                 if plotResults==1
% %                                     'Final point finished'
% %                                     idx2Smooth
% %                                     diffInPosPeak
% %                                     diffInNegPeak
% %                                     posMaxSmoothGradientValue
% %                                 end
% %                                 break
% %                             end
% %                             
% %                         % Assign the previous minimum gradient position
% %                             posMaxSmoothGradientValuePrevious = posMaxSmoothGradientValue;
% %                             posMaxSmoothValuePrevious = posMaxSmoothValue;
% %                     end
% %                     
% %                     % If it made it to the end, choose the last point
% %                             if n == length(idx2Smooth)  
% %                                 idx2Smooth = length(idx2Smooth)-n+2;
% %                                 'Smoothing window is the smallest possible';
% %                                 posMaxSmoothGradientValue = posMaxSmoothGradientValuePrevious;
% %                                 posMaxSmoothValue = posMaxSmoothValuePrevious;
% %                                 if plotResults==1
% %                                     'Final point finished'
% %                                     idx2Smooth
% %                                     diffInPosPeak
% %                                     diffInNegPeak
% %                                     posMaxSmoothGradientValue
% %                                 end
% %                             end
% %                                      
% % %                         % Now find the position of maximum intensity at this
% % %                         % smoothing window
% % %                             % Perform the smoothing
% % %                                 span2Smooth = idx2Smooth./size(Image,1);
% % %                                 smoothImageInYDir(:,m) = smooth(Image(:,m),span2Smooth,'lowess');
% % %                             % Find the minimum (most negative) gradient 
% % %                                 [maxSmoothValueRedoRedoRedo, posMaxSmoothValueRedoRedoRedo] = max(smoothImageInYDir(posMaxSmoothValue:...
% % %                                     ceil(posMaxSmoothGradientValue),m));
% % %                                 posMaxSmoothValueRedoRedoRedo = posMaxSmoothValue+posMaxSmoothValueRedoRedoRedo-1;
% %                     
% %                         % Optionally plot the intensity values            
% %                             if plotResults==1
% %                                 linkaxes(hAxLine,'x')
% %                                 figure(100)
% %                                 subplot(1,4,1)
% %                                 hold off;
% %                                 subplot(1,4,2)
% %                                 hold off;
% %                                 subplot(1,4,3)
% %                                 hold off;
% %                                 subplot(1,4,4)
% %                                 hold off;
% %                                 
% %                                     figure(4)
% %                                     subplot(2,3,1)
% %                                     plot(m,nanmean([posMaxSmoothValue,posMaxSmoothGradientValue]),'y*')
% %                                     subplot(2,3,2)
% %                                     plot(m,nanmean([posMaxSmoothValue,posMaxSmoothGradientValue]),'y*')
% %                                     subplot(2,3,3)
% %                                     plot(m,nanmean([posMaxSmoothValue,posMaxSmoothGradientValue]),'y*')
% %                                     
% %                                 %ginput
% %                             end
% %                 
% %             % Record this value
% %                 %yPosFitTemp(m) = posMaxSmoothGradientValue;
% %                 %yPosFitTemp(m) = posMaxSmoothValueRedoRedoRedo+4;
% %                 yPosFitTemp(m) = nanmean([posMaxSmoothValue,posMaxSmoothGradientValue]);
    end
    
%     if any(abs(yPosFitTemp-yPosSmoothPrevious)>5)
%         'Error: Fitted position is more than 250nm'
%         find(abs(yPosFitTemp-yPosSmoothPrevious)>5)
%         figure(1)
%         plot(yPosFitTemp,'b')
%         hold on;
%         plot(yPosSmoothPrevious,'r')
%         return
%     end
        

    if plotResults==1
        figure(4)
        subplot(2,3,1)
        hold off;
        subplot(2,3,2)
        hold off;    
        subplot(2,3,3)
        hold off;
        subplot(2,3,4)
        hold off;
        subplot(2,3,5)
        hold off;
        subplot(2,3,6)
        hold off;
    end

% Smooth the data 
    yPosSmoothTemp = smooth(yPosFitTemp,10);


end 
     