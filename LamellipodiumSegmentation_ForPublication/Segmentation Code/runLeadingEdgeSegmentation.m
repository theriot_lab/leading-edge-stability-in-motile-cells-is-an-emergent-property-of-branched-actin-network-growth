%% This script performs a segmentation on a phase-constrast movie that has
% been algined in the direction of motion of the cell and croppped so that 
% only the lamellipodium remains in the field of view. After segmentation,
% it calculate the local velocity and curvature of the leading edge over time.

% This code calls segmentBasedOnPreviousTimepoint.m, which searches for the 
% average of the position of brightest phase halo and the position of
% steepest descent of the phase intensity gradient (the transition between 
% the bright phase halo and the phase-dark cell interior). The code
% searches for this point locally around the segmentation at the previous
% time point. Therefore, this won't work if your frame rate is too slow, 
% such that the cell has undergone large shape variations in between frames. 
% In that case you would want to use the de novo segmeneter called 
% segmentLEDeNovo.m

% After segmenting, this code calls _.m to analyze the segmented leading edges.

% This code was written by Rikki M. Garner as a companion to the manuscript
% titled "Leading edge stability in motile cells in an emergent property of
% branched actin network growth", and was last updated 2020/08/19.

%% Choose data

close all;
clear all;

% Fill out the following information about where to find the images

    % Choose whether to plot the data as you segment
        plotResults = 0;

    % Tell the program where to find the image filesalyzed/';
        superFolderPath = ...
            ['/Users/Rikki/Documents/'...
            'LamellipodiumSegmentation_ForPublication/ExampleMovie/'...
            '180111_HighMagKW_1PercentDMSOCtrl_Analyzed/'];
    % Load in the folder information
        superFolderPathInfo =  dir(superFolderPath);
    % Load in the video data
        videoInfoIdx = strfind({superFolderPathInfo.name},'videoInfo');
        videoInfoIdx = find(~cellfun('isempty', videoInfoIdx));
        eval(['load(' '''' superFolderPathInfo(videoInfoIdx,1).folder '/' superFolderPathInfo(videoInfoIdx,1).name '''' ')'])
        eval(['videoInfo =' superFolderPathInfo(videoInfoIdx,1).name(1:(end-4))])
        eval(['clear ' superFolderPathInfo(videoInfoIdx,1).name(1:(end-4))])
        clear videoInfoIdx
    % Load in the alignment and cropping info
        alignmentIdx = strfind({superFolderPathInfo.name},'alignmentAndCroppingData');
        alignmentIdx = find(~cellfun('isempty', alignmentIdx));
        eval(['load(' '''' superFolderPathInfo(alignmentIdx,1).folder '/' superFolderPathInfo(alignmentIdx,1).name '''' ')'])
        eval(['alignmentAndCroppingData =' superFolderPathInfo(alignmentIdx,1).name(1:(end-4))])
        eval(['clear ' superFolderPathInfo(alignmentIdx,1).name(1:(end-4))])
        clear alignmentIdx
    % Determine where to save the analysis results
        % Write the folder to save into
            videoInfo.saveAnalysisFolderPath = [videoInfo.saveFolderPath 'HighMagAnalysisResults/'];
            % Make a folder to save the results, if one does not exist yet
                if ~exist(videoInfo.saveAnalysisFolderPath, 'dir')
                    mkdir(videoInfo.saveAnalysisFolderPath);
                end        
        % Determine the file names
            videoInfo.saveAnalysisFileNames = strcat('LEFAnalysisResults_',videoInfo.saveVideoFileNames);
            % Truncate to be ithin acceptable variable length
                for numVideo = 1:videoInfo.numVideos
                    underscoreIdx = find(videoInfo.saveAnalysisFileNames{numVideo,1}=='_');
                    keepIdx = [1:(underscoreIdx(1)-1) ((underscoreIdx(4)):length(videoInfo.saveAnalysisFileNames{numVideo,1}))];
                    videoInfo.saveAnalysisFileNames{numVideo,1} = videoInfo.saveAnalysisFileNames{numVideo,1}(keepIdx);
                end
            
    % Update the videoInfo structure
        eval([videoInfo.saveVideoInfoFileName '=videoInfo'])
        save([videoInfo.saveFolderPath videoInfo.saveVideoInfoFileName],videoInfo.saveVideoInfoFileName,'-v7.3');
        clearvars -except videoInfo alignmentAndCroppingData  
        

%% Loop through each video and run the analysis pipeline

for numVideo = 1:videoInfo.numVideos
   
    % Choose where to save results
       saveAnalysisFilePath = videoInfo.saveAnalysisFileNames{numVideo,1};
       
    % Load in the tif file name
        tifFilePathName = videoInfo.saveVideoFilePaths{numVideo,1}; 
        
    % Determine the number of timepoints   
        numTimePoints = length(imfinfo(tifFilePathName));  

    % Load in the alignment data
        videoAlignmentAndCroppingData = eval(['alignmentAndCroppingData.' videoInfo.videoNames{numVideo,1}]);
        millisecondsPerFrame  = videoAlignmentAndCroppingData.millisecondsPerFrame;   

    % Choose cutoff points where the leaging edge stops
        % Update endpoints every ~10 seconds
            idxBetweenEdgeRecord = round(10000/millisecondsPerFrame);
            timePoints2Check = 1:idxBetweenEdgeRecord:numTimePoints;
        % Pre-allocate space for cutoff points
            leadingEdgeEndPoints = NaN(length(timePoints2Check),2);
        % Loop through each check point and choose the leading edge
        % endpoints
            for timePoint2CheckNum = 1:length(timePoints2Check)
                Image = double(imread(tifFilePathName,timePoints2Check(timePoint2CheckNum)));
                figure(1)
                imshow(Image,[])
                title({'Click the leftmost, and then the rightmost, edge of the lamellipodium. Then press enter.'})
                [sidesX sidesY] = ginput;
                leadingEdgeEndPoints(timePoint2CheckNum,:) = round(sidesX); 
            end       

    %% Segment the leading edge for the first time point

        % Choose a square to determine the background signal
            % Read in the image for the last timepoint
                Image = double(imread(tifFilePathName,1));
            % Display the image
                figure(1)
                imshow(Image,[])  
                title(['Choose a rectangle in the (non-black) background'...
                    'by clicking the top left, and then the bottom right,'...
                    'corner of the rectangle'])
            % Determine the levels of background phase variation
                % Two points of opposite corners of the chosen rectangle
                    [xLimsBckgnd yLimsBckgnd] = ginput; 
                    xLimsBckgnd = round(xLimsBckgnd);
                    yLimsBckgnd = round(yLimsBckgnd);
                    
        % Click the approximate leading edge
            % Display the image
                figure(1)
                imshow(Image,[])  
                hold on
                title({['Trace the leading edge from left to right using a' ... 
                    'series of mouse clicks, and then press Enter']})
            % Have the user trace out the approximate leading edge in
            % clicks
                [xValsFirst yValsFirst] = ginput; 
            % Round these values to the nearest pixel
                xValsFirst = round(xValsFirst);
                yValsFirst = round(yValsFirst);
            % Interpolate the values for each pixel    
                xValsFirstInterp = 1:size(Image,2);
                yValsFirstInterp = interp1(xValsFirst,yValsFirst,xValsFirstInterp);
            % Plot the user-input values    
               plot(xValsFirst,yValsFirst,'b-')
            % Plot the interpolated values
               plot(xValsFirstInterp,yValsFirstInterp,'r*')
               hold off;
            % Make the legend
               legend({'User defined segmentation','Interpolated segmentation'})

        % Run the automated segmentation coe
        
            % Read in the image for this time point
                Image = double(imread(tifFilePathName,1));
            % Preallocate matrices to store the data
                % Local maximum of phase brightness
                    yPosFit = NaN(numTimePoints,size(Image,2)); 
                % Maxima smoothed across the leading edge
                    yPosSmooth = NaN(numTimePoints,size(Image,2));
            % Choose whether to plot the segmentation steps
                plotResults = 0;
            % Run the segmentation code
                [yPosFitImage yPosSmoothImage] = segmentBasedOnPreviousTimepoint(Image,...
                    yLimsBckgnd,xLimsBckgnd,yValsFirstInterp,plotResults);
            % Record the segmentation results in the pre-allocated matrices    
                yPosFit(1,:) = yPosFitImage;
                yPosSmooth(1,:) = yPosSmoothImage;
                yPosFit(1,:) = yPosFitImage;
                yPosSmooth(1,:) = yPosSmoothImage;
            % Bring up all values that aren't keeping up with the leading edge
                maxyPosSmooth = yPosSmooth(1,round(length(yPosSmooth(1,:))/2))+150;
                newyPosSmooth = yPosSmooth(1,:);
                newyPosSmooth(newyPosSmooth>maxyPosSmooth)=maxyPosSmooth;
                edgeIdx = floor(1/50)+1;
                yPosFit(1,1:leadingEdgeEndPoints(edgeIdx,1)) = ...
                    min(maxyPosSmooth,size(Image,1));
                yPosFit(1,leadingEdgeEndPoints(edgeIdx,2):end) = ...
                    min(maxyPosSmooth,size(Image,1)); 
                yPosSmooth(1,1:leadingEdgeEndPoints(edgeIdx,1)) = ...
                    min(maxyPosSmooth,size(Image,1));
                yPosSmooth(1,leadingEdgeEndPoints(edgeIdx,2):end) = ...
                    min(maxyPosSmooth,size(Image,1));
            % Plot the final fit
                figure(5)
                imshow(Image,[])
                hold on;
                plot(yPosFit(1,:),'b')
                hold off;
                drawnow

	%% Run the fitting software for all timepoints
        for numTimePoint = 2:numTimePoints 
           
            % Display the current timepoint
                numTimePoint

            % Read in the image
                Image = double(imread(tifFilePathName,numTimePoint));
                
            % Plot?
                plotResults = 0;

            % Run the segmentation
                [yPosFitImage yPosSmoothImage] = segmentBasedOnPreviousTimepoint(Image,yLimsBckgnd,xLimsBckgnd,yPosSmooth(numTimePoint-1,:),plotResults);
    
            % Record the segmentaion results
                yPosFit(numTimePoint,:) = yPosFitImage;
                yPosSmooth(numTimePoint,:) = yPosSmoothImage;
            % Bring up all values that aren't keeping up with the leading edge
                micronsBehind2Keep = 5;
                idxBehind2Keep = round(micronsBehind2Keep./(videoInfo.nanometersPerPixel/1000));
                maxyPosSmooth = yPosSmooth(numTimePoint,round(length(yPosSmooth(numTimePoint,:))/2))+idxBehind2Keep;
                newyPosSmooth = yPosSmooth(numTimePoint,:);
                newyPosSmooth(newyPosSmooth>maxyPosSmooth)=maxyPosSmooth;
                edgeIdx = floor((numTimePoint-1)/idxBetweenEdgeRecord)+1;
                yPosFit(numTimePoint,1:leadingEdgeEndPoints(edgeIdx,1)) = ...
                    min(maxyPosSmooth,size(Image,1));
                yPosFit(numTimePoint,leadingEdgeEndPoints(edgeIdx,2):end) = ...
                    min(maxyPosSmooth,size(Image,1)); 
                yPosSmooth(numTimePoint,1:leadingEdgeEndPoints(edgeIdx,1)) = ...
                    min(maxyPosSmooth,size(Image,1));
                yPosSmooth(numTimePoint,leadingEdgeEndPoints(edgeIdx,2):end) = ...
                    min(maxyPosSmooth,size(Image,1));
        % Plot the resulting leading edge segmentation
            figure(6)
            imshow(Image,[])
            hold on;
            plot(yPosFit(numTimePoint,:),'r')
            hold off;
               
    end


    % Plot a kymograph of the resulting segmentation over time
        figure(7)
        imagesc(bsxfun(@minus,yPosFit',...
            mean(yPosFit(:,max(leadingEdgeEndPoints(:,1)):...
            min(leadingEdgeEndPoints(:,2)))'))')
        title('Press Enter if the segmentation looks good. Otherwise, exit this figure.')
        ginput
    
    % Save the segmentation results
        LEFAnalysisData = v2struct();
        eval([videoInfo.saveAnalysisFileNames{numVideo,1} ' = LEFAnalysisData'])
        save([videoInfo.saveAnalysisFolderPath videoInfo.saveAnalysisFileNames{numVideo,1}],videoInfo.saveAnalysisFileNames{numVideo,1},'-v7.3');
        clearvars -except videoInfo alignmentAndCroppingData numVideo 


end

%% Analyze the newly segmented leading edges

clearvars -except videoInfo alignmentAndCroppingData numVideo 

for numVideo = 1:videoInfo.numVideos    

	% Load in the data
        load([videoInfo.saveAnalysisFolderPath videoInfo.saveAnalysisFileNames{numVideo} '.mat'] );
        eval( [ 'LEFAnalysisResults = ' videoInfo.saveAnalysisFileNames{numVideo} ])
        if isfield(LEFAnalysisResults,'alignmentAndCroppingData')
            LEFAnalysisResults = rmfield(LEFAnalysisResults,'alignmentAndCroppingData');
        end
        if isfield(LEFAnalysisResults,'videoInfo')
            LEFAnalysisResults = rmfield(LEFAnalysisResults,'videoInfo');
        end
        if isfield(LEFAnalysisResults,'numVideo')
            LEFAnalysisResults = rmfield(LEFAnalysisResults,'numVideo');
        end
	% Unpack the segmentation data
        v2struct(LEFAnalysisResults)
        clear LEFAnalysisResults
        eval(['clear ' videoInfo.saveAnalysisFileNames{numVideo}])
    % Reformat some information
        eval(['millisecondsPerFrame = alignmentAndCroppingData.' videoInfo.videoNames{numVideo} '.millisecondsPerFrame'])
        nanometersPerPixel = videoInfo.nanometersPerPixel;
    % Run the analysis code
        analyzeContourFluctuations
    % Repack the segmentation data
        LEFAnalysisResults = v2struct();
        LEFAnalysisResults = rmfield(LEFAnalysisResults,'alignmentAndCroppingData');
        LEFAnalysisResults = rmfield(LEFAnalysisResults,'videoInfo');
        LEFAnalysisResults = rmfield(LEFAnalysisResults,'numVideo');
    % Save the data
        eval([videoInfo.saveAnalysisFileNames{numVideo,1} ' = LEFAnalysisResults'])
        save([videoInfo.saveAnalysisFolderPath videoInfo.saveAnalysisFileNames{numVideo,1}],videoInfo.saveAnalysisFileNames{numVideo,1},'-v7.3');
        clearvars -except numVideo videoInfo alignmentAndCroppingData
end


