


% Choose which folder of videos to reformat
    superFolderPath = '/Users/Rikki/Documents/MembraneLeadingEdge/20190307_HighMagAG++_0.1%DMSOCtrl_Beamsplitter_Analyzed/CroppedAndAlignedVideos/FluorDecon/';
% Read in the directory info
    superFolderPathInfo = dir(superFolderPath);
    superFolderPathInfo = superFolderPathInfo([superFolderPathInfo.isdir]);
    superFolderPathInfo = superFolderPathInfo(~ismember({superFolderPathInfo.name},{'.','..','.DS_Store'}));
% Pull out the file names
    folderNameList = {superFolderPathInfo.name};
% Pull out the number of folders
    numFolders = length(folderNameList);
    
% Loop through each file and make a folder for it
for numFolder = 1:numFolders
    
    % Process the folder information
        % Read in the file name, minus the extention
            folderName = folderNameList{1,numFolder};
        % Determine the file path
            saveFilePath = [superFolderPath folderName '.tif'];
        % Pyll out the tiff info
            % Find the names of all the tif files
                folderInfo = dir([superFolderPath folderName '/*.tif']);
                tiffFileNameList = {folderInfo.name};
        % Determine the number of timepoints   
            numTimePoints = length(tiffFileNameList); 
        
     % Save the first timepoint
            % Load the file name
                fileName = tiffFileNameList{1,1};
            % Load the file path
                filePath = [superFolderPath folderName '/' fileName];
            % Load the image
                Image = mat2gray(double(imread(filePath)));
               % imshow(Image,[])
               % ginput
            % Save the image
                imwrite(Image,saveFilePath);
     % Loop through each timepoint
        for numTimePoint = 2:numTimePoints
            % Load the file name
                fileName = tiffFileNameList{1,numTimePoint};
            % Load the file path
                filePath = [superFolderPath folderName '/' fileName];
            % Load the image
                Image = mat2gray(double(imread(filePath)));
               % imshow(Image,[])
               % ginput
            % Save the image
                imwrite(Image,saveFilePath,'WriteMode','append');
        end
            
%     % Move the file into that folder
%         sourceFilePath = [superFolderPath fileName];
%         saveFilePath = [saveFolderPath fileName];
%         movefile(sourceFilePath,saveFilePath)
    
end