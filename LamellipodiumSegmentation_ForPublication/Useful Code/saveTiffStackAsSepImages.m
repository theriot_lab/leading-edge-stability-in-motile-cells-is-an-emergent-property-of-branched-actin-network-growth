


% Choose which folder of videos to reformat
    %superFolderPath = '/Users/Rikki/Documents/MembraneLeadingEdge/Deconvolution/20190430_HighMagAAVSGFP++_Beamsplitter_Analyzed/CroppedAndAlignedVideos/Fluor/';
    %superFolderPath = '/Users/Rikki/Documents/MembraneLeadingEdge/20190307_HighMagAG++_0.1%DMSOCtrl_Beamsplitter_Analyzed/CroppedAndAlignedVideos/Fluor/';
        %superFolderPath = '/Users/Rikki/Documents/MembraneLeadingEdge/20190305_HighMagAG++_1.0%Agar_Beamsplitter_Analyzed/CroppedAndAlignedVideos/Fluor/';
        superFolderPath = '/Users/Rikki/Documents/MembraneLeadingEdge/20190219_HighMagAG++_1.0%Agar_Beamsplitter_Analyzed/CroppedAndAlignedVideos/Fluor/';

% Read in the directory info
    superFolderPathInfo = dir(superFolderPath);
    superFolderPathInfo = superFolderPathInfo(~[superFolderPathInfo.isdir]);
    superFolderPathInfo = superFolderPathInfo(~ismember({superFolderPathInfo.name},{'.','..','.DS_Store'}));
% Pull out the file names
    fileNameList = {superFolderPathInfo.name};
% Pull out the number of folders
    numFiles = length(fileNameList);
    
% Loop through each file and make a folder for it
for numFile = 1:numFiles
    
    % Process the tif information
        % Read in the file name, minus the extention
            fileName = fileNameList{1,numFile};
            lastPtIdx = find(fileName=='.',1,'last');
            saveFolderName = fileName(1:(lastPtIdx-1));
        % Determine the file path
            filePath = [superFolderPath fileName];
        % Determine the number of timepoints   
            numTimePoints = length(imfinfo(filePath));
            
    % Make a folder with that file name
        saveFolderPath = [superFolderPath saveFolderName '/'];
        % Make a folder to save the results, if one does not exist yet
            if ~exist(saveFolderPath, 'dir')
                mkdir(saveFolderPath);
            end        
        
    % Create a file prefix
        lastPrefixIdx = find(fileName=='.',1,'last');
        filePrefix = fileName(1:(lastPrefixIdx-1));
        filePrefix = [filePrefix '_%0' num2str(ceil(log10(numTimePoints))) 'd.tif'];
        
     % Loop through each timepoint
        for numTimePoint = 1:numTimePoints
            % Load the image
                Image = imread(filePath,numTimePoint);
            % Load the name to save the image to
                saveFileName = sprintf(filePrefix,numTimePoint);
                saveFilePath = [saveFolderPath saveFileName];
            % Save the image
                imwrite(Image,saveFilePath);
        end
            
%     % Move the file into that folder
%         sourceFilePath = [superFolderPath fileName];
%         saveFilePath = [saveFolderPath fileName];
%         movefile(sourceFilePath,saveFilePath)
    
end