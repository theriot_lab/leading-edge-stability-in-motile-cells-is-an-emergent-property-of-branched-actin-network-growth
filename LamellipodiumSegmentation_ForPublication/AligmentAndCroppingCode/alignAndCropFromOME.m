
%% This script loops through a series of movies, and allows the user to 
% choose a set of time points where the cell is moving persistently,
% align the cells in the vertical direction, and then crop out the leading
% edge. You can target as many persistent segments within a single movie 
% as you would like for downstream analysis. 

% 1. Start by choosing the time points in between which the cell is moving
% persistently. You can do this by scrolling through the time points in
% small increments using the upper scroll bar, or by scroling through the
% time points in larger increments using the lower scroll bar. Once you
% have decided on the stop and end times, scroll to the first time point 
% and select the click button "Choose first time point'. Then scroll to the
% last time point and select the click button "Choose last time point'.
% Don't forget, you can always redo this step later on if you want to make
% adjustments

% 2. Next, align the cell so thats its direction of motion points upwards
% on the screen. To do this, click "Align Images", then click the center 
% of the cell, then click the center of the lamellipodium. (in doing so, 
% you define the vector of the direction of motion of the cell. Continue
% repeating this process until the cell migrates upwards throughout the
% entire set of time points that you selected earlier. You can update both
% the start and end time as well as the alignment as many times as you 
% would like.

% 3. Once you have chosen the time points and aligned the video, next you
% will want to crop out just the lamellipodium (the extreme phase constrast
% intensity variations in the cell body will make it difficult to do 
% segmentation down the line). To crop out everything but the
% lamellipodium, click "Crop Images". This will reveal a resizeable box that
% starts the size of the entire image. Resize the box by dragging the sides,
% click and drag inside the box to translate the box position. YOU CANNOT 
% ADJUST THE TIME OR ALIGNMENT WHILE YOU ARE ADJUSTING THE CROP. If you want
% to scroll through the time points or re-align after entering crop mode,
% simply double-click on of the size of the box to save its position. After
% you'r done adjust other features, you can click "Crop Images" again to
% adjust the cropping box.

% 4. Continue iteratively adjusting all of these features until you are
% happy with the result, then exit the GUI. A pop-up window will appear
% asking you whether you want to save the alignment results,offering you.
% three options: If you like the cropping result, click "Yes, save the data
% and close the figure". If you didn't like the results and want to move
% on, click "No delete the data and close the figure". If you didn't like
% the results and want to try again, click "I would like to keep working on this.

% 5. A pop up window will appear asking if you'd like to save another clip 
% from this movie. Decide whether there are other tracks within that video 
% that you would like to use. For example, sometimes a cell migrates 
% persistently for a while, then blebs/turns, and then resumes migrating
% persistently afterwards. Maybe you want to extract two separate
% clips for for each persistent period. In the pop window, make this decision.

% 6. At the end, the code will make a folder with an identical name to the
% input, with "_Analyzed" appended to the end. Check out
% "CroppedAndAlignedVideos" to see whether you like the result. Generally,
% if you run this code and it detects that you already have a cropped 
% clip associated with a video, it will skip segemntation of that video. If
% you ever want a redo, change "videoInfo.redoManualCropAndAlign" to be
% true on line 160 of this code. Similarly, you can adjust the for loops
% to only segment one video in a dataset at a time.

% This code was written by Rikki M. Garner as a companion to the manuscript
% titled "Leading edge stability in motile cells in an emergent property of
% branched actin network growth", and was last updated 2020/08/19.


%% Clear the system
    close all;
    clear all;
    
% Fill out the following information about where to find the images 
    
    % Tell the program where to find the image files      
        superFolderPath = ['/Users/Rikki/Documents/'...
            'LamellipodiumSegmentation_ForPublication/ExampleMovie/'...
            '180111_HighMagKW_1PercentDMSOCtrl/'];
        nanometersPerPixel = 64.3;  
        
    % Tell the program where to save the results
        % Place in the analyzed data folder
            saveFolderPath = superFolderPath(1:(find(superFolderPath=='/',1,'last')-1));
            saveFolderPath = [saveFolderPath '_Analyzed/'];
            % Make a folder to save the results, if one does not exist yet
                if ~exist(saveFolderPath, 'dir')
                    mkdir(saveFolderPath);
                end        
        % Name it the same as the folder name
            lastFolderIdx = find(superFolderPath=='/',2,'last');
            saveFileName = [saveFolderPath((lastFolderIdx(1)+1):(lastFolderIdx(2)-1))];
            % Remove the percent character
                percentCharIdx = strfind(saveFileName,'%');
                for numPercent = 1:length(percentCharIdx)
                    saveFileName = [saveFileName(1:(percentCharIdx-1)) 'Per' saveFileName((percentCharIdx+1):end)];
                end
            % Remove the point character
                percentCharIdx = strfind(saveFileName,'.');
                for numPercent = 1:length(percentCharIdx)
                    saveFileName = [saveFileName(1:(percentCharIdx-1)) 'Pt' saveFileName((percentCharIdx+1):end)];
                end
        % Make a place to save the video info and alignment info
            saveVideoInfoFileName = ['videoInfo_' saveFileName];
            saveVideoInfoFilePath = [saveFolderPath saveVideoInfoFileName];
            saveAlignmentAndCroppingFileName = ['alignmentAndCroppingData_' saveFileName];
            saveAlignmentAndCroppingFilePath = [saveFolderPath saveAlignmentAndCroppingFileName];

        % If this data set already exists, load it in and assign it to
        % a generic variable
            if exist([saveVideoInfoFilePath '.mat'],'file') == 2
                load([saveVideoInfoFilePath '.mat'])
                eval(['videoInfo =' saveVideoInfoFileName])                  
            else
               % Read in the directory info
                    superFolderPathInfo = dir(superFolderPath);
                    superFolderPathInfo = superFolderPathInfo([superFolderPathInfo.isdir]);
                    superFolderPathInfo = superFolderPathInfo(~ismember({superFolderPathInfo.name},{'.','..','.DS_Store'}));
                % Pull out the folder names
                    folderNameList = {superFolderPathInfo.name};
                % Pull out the number of folders
                    numFolders = length(folderNameList);
                % Pull out the folder prefix
                    folderNamePrefix = folderNameList(1);
                    folderNamePrefix = folderNamePrefix{1,1};
                    folderNamePrefix = folderNamePrefix(1:(find(folderNamePrefix=='_',1,'last')-1));
                % Pull out the individal folder numbers (by name)
                    folderNamePostfix = nan(numFolders,1);
                    for numFolder = 1:numFolders
                        folderName = folderNameList(numFolder);
                        folderName = folderName{1,1};
                        folderNamePostfix(numFolder,1) = str2num(folderName((find(folderName=='_',1,'last')+1):end));
                    end
                    folderNamePostfix;
                 % Resort the folder name list and postfix names in increasing order
                    [folderNamePostfix sortIdx] = sort(folderNamePostfix);
                    folderNameList = folderNameList(sortIdx);

                % Save all of this info into a structure, save it, and clear all the
                % variables    
                    videoInfo = v2struct();
                    eval([videoInfo.saveVideoInfoFileName '=videoInfo'])
                    save([videoInfo.saveFolderPath videoInfo.saveVideoInfoFileName],videoInfo.saveVideoInfoFileName,'-v7.3');
            end     

    % Load in the alignment/crop data, if one does not exist yet

        % If this data set already exists, load it in and assign it to
        % a generic variable
            if exist([videoInfo.saveAlignmentAndCroppingFilePath '.mat'],'file') == 2
                load(videoInfo.saveAlignmentAndCroppingFilePath)
                eval(['alignmentAndCroppingData =' videoInfo.saveAlignmentAndCroppingFileName])
            else
                alignmentAndCroppingData = [];
            end
            
    % Choose whether you want to redo manual alignment
            videoInfo.redoManualCropAndAlign = 1;
            videoInfo.reSaveFileInfo = 0;
                        
    clearvars -except videoInfo alignmentAndCroppingData    

%% Loop through each set of videos

    for numFolder = 1:videoInfo.numFolders
         
        % Load in the folder info 
            % Print the folder number
                sprintf('Processing folder # %i of %i',numFolder,videoInfo.numFolders)

            % Pull in the current folder name
                folderName = videoInfo.folderNameList(numFolder);
                folderName = folderName{1,1};

            % Infer the sample name from the folder name
                % Copy the folder name 
                    sampleName = ['Sample_' folderName];
                % Remove the percent characters
                    percentCharIdx = strfind(sampleName,'%');
                    for numPercent = 1:length(percentCharIdx)
                        sampleName = [sampleName(1:(percentCharIdx(numPercent)-1)) 'Pr' sampleName((percentCharIdx(numPercent)+1):end)];
                    end
                % Remove the percent character
                    decimalCharIdx = strfind(sampleName,'.');
                    for numDecimalCharIdx = 1:length(decimalCharIdx)
                        sampleName = [sampleName(1:(decimalCharIdx(numDecimalCharIdx)-1+(numDecimalCharIdx-1))) 'Pt' sampleName((decimalCharIdx(numDecimalCharIdx)+1+(numDecimalCharIdx-1)):end)];
                    end
                % Remove the 'migrating'
                    if any(strfind(sampleName,'_Migrating'))
                        migratingIdx = strfind(sampleName,'_Migrating');
                        sampleName = [sampleName(1:(migratingIdx-1)) sampleName((migratingIdx+10):end)];
                    end
                % Rename to correctly indicate 100X videos
                    if any(strfind(sampleName,'20X'))
                        magLoc = strfind(sampleName,'20X');
                        sampleName = [sampleName(1:(magLoc-1)) '100X' sampleName((magLoc+3):end)];
                    end
                    sampleName
                    if length(sampleName)>61
                        'Sample name is too long'
                        break
                    end
            % Pull out the names of the tif files in this set
                folderPathInfo = dir([videoInfo.superFolderPath folderName '/*.tif']);
                if isempty(folderPathInfo)
                    folderName = [folderName '/Pos0'];
                    folderPathInfo = dir([videoInfo.superFolderPath folderName '/*.tif']);
                end
                tifFileNameList = {folderPathInfo.name};

            % Pull out the file info for this set of tifs
                % Pull out the file name and file path
                    tifFileName = tifFileNameList(1);
                    tifFileName = tifFileName{1,1};
                    tifFilePathName = [videoInfo.superFolderPath folderName '/' tifFileName];
                    
        if videoInfo.reSaveFileInfo
            
            % Determine the number of videos
                videoInfo.videoNames = fieldnames(alignmentAndCroppingData);
                videoInfo.numVideos = length(videoInfo.videoNames);
                numVideos = length(cell2mat(strfind(videoInfo.videoNames,[sampleName,'_'])));
                for numVideo = 1:numVideos
                    % Load in the sample data
                        sampleData = eval(['alignmentAndCroppingData.',sampleName,'_',num2str(numVideo)]);
                    % Add the file path 
                        sampleData.tifFilePathName = tifFilePathName;
                    % Reassign the data to its field
                        eval(['alignmentAndCroppingData.',sampleName,'_',num2str(numVideo),'= sampleData'])
                    % Reassign the concatenated data file to its original name
                        eval([videoInfo.saveAlignmentAndCroppingFileName, ' = alignmentAndCroppingData']) 
                    % Save the data
                        save(videoInfo.saveAlignmentAndCroppingFilePath,videoInfo.saveAlignmentAndCroppingFileName,'-v7.3');
                end
            
        elseif ~isfield(alignmentAndCroppingData,[sampleName,'_',num2str(1)]) || videoInfo.redoManualCropAndAlign

            % Load in the data
            
                % Load the reader
                    reader = bfGetReader(tifFilePathName); 
                    omeMeta = reader.getMetadataStore();
                    omeXML = char(omeMeta.dumpXML());

                % Determine the time step
                    timePointsInMilliseconds = getDeltaTRikki(reader);
                    timePointsInMilliseconds = timePointsInMilliseconds-timePointsInMilliseconds(1);
                    timeStepInMilliseconds = diff(timePointsInMilliseconds,1,2);
                    timeStepInMilliseconds = round(timeStepInMilliseconds,1);
                    millisecondsPerFrame = round(nanmean(timeStepInMilliseconds(:)));
                    secondsPerFrame = millisecondsPerFrame/10^3;
                    maxTimeStepDifferenceFromMean = nanmax(abs(timeStepInMilliseconds(:)-millisecondsPerFrame));
                    sprintf('Time step varies up to %i Percent',round(((maxTimeStepDifferenceFromMean)/millisecondsPerFrame)*100))

                % Export the number of time points
                    idxNumTimepointsBeg = strfind(omeXML, 'SizeT')+7;
                    idxNumTimepointsEnd = strfind(omeXML, 'SizeX')-3;
                    numTimePoints = str2num(omeXML(idxNumTimepointsBeg:idxNumTimepointsEnd));

                % Pull out the downsampled time steps
                    timeIdxDownsampled = 1:floor(1/secondsPerFrame):numTimePoints;
                    timePointsInMillisecondsDownsampled = timePointsInMilliseconds(timeIdxDownsampled);
                    numTimePointsDownsampled = length(timeIdxDownsampled);
                    
        if numTimePoints>round(10000/55)
            % Run the aligner (multiple times if requested) and wait for the user to close the GUI
                varargout = GUI_Align_And_Crop_Images;
                waitfor(GUI_Align_And_Crop_Images)
                numSubMovies=1;
            % Save the results for the first video
                if  exist('angle')==1 && exist('position')==1 && exist('firstTPIdx')==1 && exist('lastTPIdx')==1  
                    % Clear the field for this sample name 
                        eval(['alignmentAndCroppingData.',sampleName,'_',num2str(numSubMovies),' = []'])          

                    % Add the analysis results to the concatenated data set
                    % Save the results to a structure
                        sampleData = [];
                        sampleData.angle = angle;
                        sampleData.position = position;
                        sampleData.firstTPIdx = firstTPIdx;  
                        sampleData.lastTPIdx = lastTPIdx;    
                        sampleData.millisecondsPerFrame = millisecondsPerFrame; 
                        sampleData.tifFilePathName = tifFilePathName;
                    % Save this structure to the concatenated data file 
                        eval(['alignmentAndCroppingData.',sampleName,'_',num2str(numSubMovies),'= sampleData'])
                    % Reassign the concatenated data file to its original name
                        eval([videoInfo.saveAlignmentAndCroppingFileName, ' = alignmentAndCroppingData']) 
                    % Save the data
                        save(videoInfo.saveAlignmentAndCroppingFilePath,videoInfo.saveAlignmentAndCroppingFileName,'-v7.3');
                    % Clear the variables
                        clear angle
                        clear position
                        clear firstTPIdx
                        clear lastTPIdx
                end

                % Ask if the user wants to run the aligner again on another time
                % segment (repeat until user says no)                    
                    answer = questdlg('Would you like to make another video from this movie?', ...
                        'User Input', ...
                        'Yes, start the alignment algorithm again','No, move on to the next movie','Yes');
                    % Handle response
                        switch answer
                            case 'Yes, start the alignment algorithm again'
                                varargout = GUI_Align_And_Crop_Images;
                                waitfor(GUI_Align_And_Crop_Images)
                                numSubMovies=numSubMovies + 1;
                                % Save the results for the first video
                                    if exist('angle')==1 && exist('position')==1 && exist('firstTPIdx')==1 && exist('lastTPIdx')==1  
                                        % Clear the field for this sample name 
                                            eval(['alignmentAndCroppingData.',sampleName,'_',num2str(numSubMovies),'= []'])          

                                        % Add the analysis results to the concatenated data set
                                        % Save the results to a structure
                                            sampleData = [];
                                            sampleData.angle = angle;
                                            sampleData.position = position;
                                            sampleData.firstTPIdx = firstTPIdx;  
                                            sampleData.lastTPIdx = lastTPIdx;    
                                            sampleData.millisecondsPerFrame = millisecondsPerFrame; 
                                            sampleData.tifFilePathName = tifFilePathName;       
                                        % Save this structure to the concatenated data file 
                                            eval(['alignmentAndCroppingData.',sampleName,'_',num2str(numSubMovies),'= sampleData'])
                                        % Reassign the concatenated data file to its original name
                                            eval([videoInfo.saveAlignmentAndCroppingFileName, ' = alignmentAndCroppingData']) 
                                        % Save the data
                                            save(videoInfo.saveAlignmentAndCroppingFilePath,videoInfo.saveAlignmentAndCroppingFileName,'-v7.3');
                                        % Clear the variables
                                            clear angle
                                            clear position
                                            clear firstTPIdx
                                            clear lastTPIdx
                                    end
                            case 'No, move on to the next movie'
                        end
                % Ask if the user wants to run the aligner again on another time
                % segment (repeat until user says no)    
                    while strcmp(answer,'Yes, start the alignment algorithm again')==1
                        answer = questdlg('Would you like to make another video from this movie?', ...
                            'User Input', ...
                            'Yes, start the alignment algorithm again','No, move on to the next movie','Yes');
                        % Handle response
                            switch answer
                                case 'Yes, start the alignment algorithm again'
                                    varargout = GUI_Align_And_Crop_Images;
                                    waitfor(GUI_Align_And_Crop_Images)
                                    numSubMovies = numSubMovies + 1;
                                    % Save the results for the first video
                                        if exist('angle')==1 && exist('position')==1 && exist('firstTPIdx')==1 && exist('lastTPIdx')==1  
                                            % Clear the field for this sample name 
                                                eval(['alignmentAndCroppingData.',sampleName,'_',num2str(numSubMovies),'= []'])          

                                            % Add the analysis results to the concatenated data set
                                            % Save the results to a structure
                                                sampleData = [];
                                                sampleData.angle = angle;
                                                sampleData.position = position;
                                                sampleData.firstTPIdx = firstTPIdx;  
                                                sampleData.lastTPIdx = lastTPIdx;    
                                                sampleData.millisecondsPerFrame = millisecondsPerFrame; 
                                                sampleData.tifFilePathName = tifFilePathName;       
                                            % Save this structure to the concatenated data file 
                                                eval(['alignmentAndCroppingData.',sampleName,'_',num2str(numSubMovies),'= sampleData'])
                                            % Reassign the concatenated data file to its original name
                                                eval([videoInfo.saveAlignmentAndCroppingFileName, ' = alignmentAndCroppingData']) 
                                            % Save the data
                                                save(videoInfo.saveAlignmentAndCroppingFilePath,videoInfo.saveAlignmentAndCroppingFileName,'-v7.3');
                                            % Clear the variables
                                                clear angle
                                                clear position
                                                clear firstTPIdx
                                                clear lastTPIdx
                                        end
                                case 'No, move on to the next movie'
                            end
                    end
            end
        end
        
        % Clear the system
            clearvars -except videoInfo numFolder alignmentAndCroppingData
         
    end

if exist('sampleName')==1
    'Sample name is too long'
    return
end        

%% Loop through each video and save the cropped and aligned segments

    % Determine the number of videos
        videoInfo.videoNames = fieldnames(alignmentAndCroppingData);
        videoInfo.numVideos = length(videoInfo.videoNames);
    % Determine the folder location to save the videos
        videoInfo.saveVideoFolderPath = [videoInfo.saveFolderPath 'CroppedAndAlignedVideos/'];
    % Make a folder to save the results, if one does not exist yet
        if ~exist(videoInfo.saveVideoFolderPath, 'dir')
            mkdir(videoInfo.saveVideoFolderPath);
        end      
        
for numVideo = 1:videoInfo.numVideos

    % Determine the save file path
        videoInfo.saveVideoFileNames{numVideo,1} = videoInfo.videoNames{numVideo,1}(8:end);
        videoInfo.saveVideoFilePaths{numVideo,1} = [videoInfo.saveVideoFolderPath videoInfo.saveVideoFileNames{numVideo,1} '.tif'];
    % Load in the alignment data
        videoAlignmentAndCroppingData = eval(['alignmentAndCroppingData.' videoInfo.videoNames{numVideo,1}]);
        firstTPIdx = videoAlignmentAndCroppingData.firstTPIdx;
        lastTPIdx = videoAlignmentAndCroppingData.lastTPIdx;
        angle = videoAlignmentAndCroppingData.angle; 
        position = videoAlignmentAndCroppingData.position; 
        tifFilePathName = videoAlignmentAndCroppingData.tifFilePathName; 
        
    if ~isempty(angle) && ~isempty(position) && ~isempty(firstTPIdx) && ~isempty(lastTPIdx)
        % Load the reader
            clear reader
            reader = bfGetReader(tifFilePathName); 
        
           for numTimePoint = firstTPIdx:lastTPIdx

                % Rotate the image
                    phaseIm = imrotate(mat2gray(double(bfGetPlane(reader,numTimePoint))),angle,'bicubic'); 
                % Crop the image
                    % Make sure edges aren't larger than the image size
                        position(3) = min(position(3),size(phaseIm,2)-position(1));
                        position(4) = min(position(4),size(phaseIm,1)-position(2));
                    phaseIm = phaseIm(position(2):(position(2)+position(4)),position(1):(position(1)+position(3)));
                %     % Display the image
                %         figure(1);
                %         imagesc(phaseIm)
                %         drawnow
                % Write the tif file   
                    if numTimePoint==firstTPIdx
                        imwrite(phaseIm,videoInfo.saveVideoFilePaths{numVideo,1})
                    else            
                        imwrite(phaseIm,videoInfo.saveVideoFilePaths{numVideo,1},'WriteMode','append')
                    end
           end
    end
end

% Save the updated video info file
    eval([videoInfo.saveVideoInfoFileName '=videoInfo'])
    save([videoInfo.saveFolderPath videoInfo.saveVideoInfoFileName],videoInfo.saveVideoInfoFileName,'-v7.3');
    clearvars -except videoInfo alignmentAndCroppingData      

    beep