function varargout = GUI_Align_And_Crop_Images(varargin)

%% This function  creates a GUI to align and crop images of migrating cells

% (This function is called by alignAndCropFromOME.m)

% This code was written by Rikki M. Garner as a companion to the manuscript
% titled "Leading edge stability in motile cells in an emergent property of
% branched actin network growth", and was last updated 2020/08/19.

%%
% GUI_SET_TIME_ENDPOINTS MATLAB code for GUI_Set_Time_Endpoints.fig
%      GUI_Align_And_Crop_Images, by itself, creates a new GUI_Align_And_Crop_Images or raises the existing
%      singleton*.
%
%      H = GUI_Align_And_Crop_Images returns the handle to a new GUI_Align_And_Crop_Images or the handle to
%      the existing singleton*.
%
%      GUI_Align_And_Crop_Images('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI_Align_And_Crop_Images.M with the given input arguments.
%
%      GUI_Align_And_Crop_Images('Property','Value',...) creates a new GUI_Align_And_Crop_Images or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before GUI_Align_And_Crop_Images_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GUI_Align_And_Crop_Images_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUI_Align_And_Crop_Images

% Last Modified by GUIDE v2.5 26-Nov-2018 14:09:47

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @GUI_Align_And_Crop_Images_OpeningFcn, ...
                   'gui_OutputFcn',  @GUI_Align_And_Crop_Images_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before GUI_Set_Time_Endpoints is made visible.
function GUI_Align_And_Crop_Images_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GUI_Align_And_Crop_Images (see VARARGIN)

% Choose default command line output for GUI_Set_Time_Endpoints
    handles.output = hObject;
    
% Read in data from the workspace
    reader = evalin('base','reader');
    timeIdxDownsampled = evalin('base','timeIdxDownsampled');
    timePointsInMillisecondsDownsampled = evalin('base','timePointsInMillisecondsDownsampled');

% Initialize the angle to zero and crop position to be the entire image
    angle = 0;
    position = [1 1 (size(bfGetPlane(reader,1),2)-1) (size(bfGetPlane(reader,1),1)-1)];
% Update the current timepoint
    timeIdx = get(handles.slider1,'Value');
    timeIdx = round(timeIdx);
    timeIdxOffset = get(handles.slider2,'Value');
    timeIdxOffset = round(timeIdxOffset);
% Prepare the rotated mage for this timepoint
    phaseIm = imrotate(mat2gray(double(bfGetPlane(reader,timeIdxDownsampled(timeIdx)+timeIdxOffset))),angle,'bicubic'); 
% Plot the image
    imagesc(phaseIm)
    
% Update the handles
    handles.reader=reader;
    handles.timeIdxDownsampled=timeIdxDownsampled;
    handles.timePointsInMillisecondsDownsampled=timePointsInMillisecondsDownsampled;
    handles.angle = angle;
    handles.position = position;

% Update guidata structure
    guidata(hObject, handles);

% This sets up the initial plot - only do when we are invisible
% so window can get raised using GUI_Set_Time_Endpoints.
% if strcmp(get(hObject,'Visible'),'off')
%     plot(rand(5));
% end

% UIWAIT makes GUI_Set_Time_Endpoints wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = GUI_Align_And_Crop_Images_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
axes(handles.axes1);
cla;

popup_sel_index = get(handles.popupmenu1, 'Value');
switch popup_sel_index
    case 1
        plot(rand(5));
    case 2
        plot(sin(1:0.01:25.99));
    case 3
        bar(1:.5:10);
    case 4
        plot(membrane);
    case 5
        surf(peaks);
end


% --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to FileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function OpenMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to OpenMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
file = uigetfile('*.fig');
if ~isequal(file, 0)
    open(file);
end

% --------------------------------------------------------------------
function PrintMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to PrintMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
printdlg(handles.figure1)

% --------------------------------------------------------------------
function CloseMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to CloseMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
selection = questdlg(['Close ' get(handles.figure1,'Name') '?'],...
                     ['Close ' get(handles.figure1,'Name') '...'],...
                     'Yes','No','Yes');
if strcmp(selection,'No')
    return;
end

delete(handles.figure1)


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
     set(hObject,'BackgroundColor','white');
end

set(hObject, 'String', {'plot(rand(5))', 'plot(sin(1:0.01:25))', 'bar(1:.5:10)', 'plot(membrane)', 'surf(peaks)'});


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    % Read in the timepoints/image data
        reader = handles.reader;
        timeIdxDownsampled = handles.timeIdxDownsampled;
        timePointsInMillisecondsDownsampled = handles.timePointsInMillisecondsDownsampled;
        angle = handles.angle;
        position = handles.position;
    % Update the current timepoint
        timeIdx = get(handles.slider1,'Value');
        timeIdx = round(timeIdx);
        timeIdxOffset = get(handles.slider2,'Value');
        timeIdxOffset = round(timeIdxOffset);
    % Prepare the rotated mage for this timepoint
        phaseIm = imrotate(mat2gray(double(bfGetPlane(reader,timeIdxDownsampled(timeIdx)+timeIdxOffset))),angle,'bicubic'); 
        %phaseIm = phaseIm(position(2):(position(2)+position(4)),position(1):(position(1)+position(3)));
    % Plot this timepoint
        imagesc(phaseIm)
    % Add the image of the rectangle
        rectHandle = imrect(gca,position);

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
reader = evalin('base','reader');
timeIdxDownsampled = evalin('base','timeIdxDownsampled');
timePointsInMillisecondsDownsampled = evalin('base','timePointsInMillisecondsDownsampled');
set(hObject, 'min', 1);
set(hObject, 'max', length(timePointsInMillisecondsDownsampled));
set(hObject, 'Value', 1);          
set(hObject,'SliderStep', [1/(length(timePointsInMillisecondsDownsampled)-1) 10/(length(timePointsInMillisecondsDownsampled)-1)]);

handles.slider1 = hObject;

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes on slider movement.
function slider2_Callback(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    % Load in the timpoints/image data
        reader = handles.reader;
        timeIdxDownsampled = handles.timeIdxDownsampled;
        timePointsInMillisecondsDownsampled = handles.timePointsInMillisecondsDownsampled;
        angle = handles.angle;
        position = handles.position;
    % Update the timestep
        timeIdx = get(handles.slider1,'Value');
        timeIdx = round(timeIdx);
        timeIdxOffset = get(handles.slider2,'Value');
        timeIdxOffset = round(timeIdxOffset);
        timeIdxDownsampled(timeIdx)+timeIdxOffset;
    % Load and rotate the image for this timestep
        phaseIm = imrotate(mat2gray(double(bfGetPlane(reader,timeIdxDownsampled(timeIdx)+timeIdxOffset))),angle,'bicubic'); 
        %phaseIm = phaseIm(position(2):(position(2)+position(4)),position(1):(position(1)+position(3)));
    % Display the image
        imagesc(phaseIm)
    % Add the crop area
        rectHandle = imrect(gca,position);
    % Update handles structure
        guidata(hObject, handles);

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

reader = evalin('base','reader');
timeIdxDownsampled = evalin('base','timeIdxDownsampled');
timePointsInMillisecondsDownsampled = evalin('base','timePointsInMillisecondsDownsampled');
set(hObject, 'min', 0);
set(hObject, 'max', 19);
set(hObject, 'Value', 1);          
set(hObject,'SliderStep', [1/(19) 10/18]);

handles.slider2 = hObject;
% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in pushbuttonChooseFirstTimePoint.
function pushbuttonChooseFirstTimePoint_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonChooseFirstTimePoint (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

reader = handles.reader;
timeIdxDownsampled = handles.timeIdxDownsampled;
timePointsInMillisecondsDownsampled = handles.timePointsInMillisecondsDownsampled;

timeIdx = get(handles.slider1,'Value');
timeIdx = round(timeIdx);
timeIdxOffset = get(handles.slider2,'Value');
timeIdxOffset = round(timeIdxOffset);
firstTPIdx = timeIdxDownsampled(timeIdx)+timeIdxOffset;
handles.firstTPIdx = firstTPIdx;
% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in pushbuttonChooseLastTimePoint.
function pushbuttonChooseLastTimePoint_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonChooseLastTimePoint (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
reader = handles.reader;
timeIdxDownsampled = handles.timeIdxDownsampled;
timePointsInMillisecondsDownsampled = handles.timePointsInMillisecondsDownsampled;

timeIdx = get(handles.slider1,'Value');
timeIdx = round(timeIdx);
timeIdxOffset = get(handles.slider2,'Value');
timeIdxOffset = round(timeIdxOffset);
lastTPIdx = timeIdxDownsampled(timeIdx)+timeIdxOffset;
assignin('base','lastTPIdx',lastTPIdx)
handles.lastTPIdx = lastTPIdx;
% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbuttonAlignImages.
function pushbuttonAlignImages_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonAlignImages (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)]

    % Load in the timepoints/image data
        reader = handles.reader;
        position = handles.position;
        timeIdxDownsampled = handles.timeIdxDownsampled;
        timePointsInMillisecondsDownsampled = handles.timePointsInMillisecondsDownsampled;
        angle  = handles.angle;
    % Load in the current timepoint
        timeIdx = get(handles.slider1,'Value');
        timeIdx = round(timeIdx);
        timeIdxOffset = get(handles.slider2,'Value');
        timeIdxOffset = round(timeIdxOffset);
    % Rotate the image
        phaseIm = imrotate(mat2gray(double(bfGetPlane(reader,timeIdxDownsampled(timeIdx)))),angle,'bicubic','bicubic'); 
    %Plot the rotated image
        imagesc(phaseIm)
    % Collect old fig handle functions overwritten by ginput
        fcnNames = {'WindowButtonDownFcn','WindowButtonMotionFcn'};
        fcnVals = get(gcf,fcnNames);
        oldFcnNameValPairs = cat(1, fcnNames, fcnVals);
    % Collect the user input line
        [x y] = ginput(2);
    % reset figure handle to replenish cursor to screen
        set(gcf, oldFcnNameValPairs{:}) % Restore the original figure functions
    % Record the angle        
        angle = angle + 90 - (atan2(-(y(2)-y(1)),(x(2)-x(1)))*180/pi);
    % Rotate the image
        phaseIm = imrotate(mat2gray(double(bfGetPlane(reader,timeIdxDownsampled(timeIdx)+timeIdxOffset))),angle,'bicubic'); 
        %phaseIm = phaseIm(position(2):(position(2)+position(4)),position(1):(position(1)+position(3)));
    %Plot the rotated image
        imagesc(phaseIm)
    % Update the crop position vector to account for the bigger image
        if position(1) == 1
           position = [1 1 (size(phaseIm,2)-1) (size(phaseIm,1)-1)];
        end
    % Update the handles
        handles.angle = angle;
        handles.position = position;
    % Add the handles to the guidata stucture
        guidata(hObject, handles);

% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbuttonCropImages.
function pushbuttonCropImages_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonCropImages (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%[colIdx rowIdx] = ginput(2);

    % Load in the relevant handles data
        reader = handles.reader;
        timeIdxDownsampled = handles.timeIdxDownsampled;
        timePointsInMillisecondsDownsampled = handles.timePointsInMillisecondsDownsampled;
        position = handles.position;
        angle  = handles.angle;
    % Load in the time points
        timeIdx = get(handles.slider1,'Value');
        timeIdx = round(timeIdx);
        timeIdxOffset = get(handles.slider2,'Value');
        timeIdxOffset = round(timeIdxOffset);
    % Load in and rotate the image
        phaseIm = imrotate(mat2gray(double(bfGetPlane(reader,timeIdxDownsampled(timeIdx)+timeIdxOffset))),angle,'bicubic'); 
    % Plot the image
        imagesc(phaseIm)
    % Collect old fig handle functions overwritten by ginput
        fcnNames = {'WindowButtonDownFcn','WindowButtonMotionFcn'};
        fcnVals = get(gcf,fcnNames);
        oldFcnNameValPairs = cat(1, fcnNames, fcnVals);
    % Choose the cropping rectangle
        rectHandle = imrect(gca,position);
    % Add the position
        position = round(wait(rectHandle));
    % reset figure handle to replenish cursor to screen
        set(gcf, oldFcnNameValPairs{:}) % Restore the original figure functions
    % Make sure edges aren't larger than the image size
        position(3) = min(position(3),size(phaseIm,2)-position(1));
        position(4) = min(position(4),size(phaseIm,1)-position(2));
        
    % Update the handles and save them into guidata
        handles.position = position;
        guidata(hObject, handles);

% --- Executes on button press in pushbuttonChooseCrop.
function pushbuttonChooseCrop_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonChooseCrop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% If all data looks good, give sign out, otherwise make sure all of teh
% data isn't there
    if isfield(handles,'position') && isfield(handles,'angle') && isfield(handles,'firstTPIdx') && isfield(handles,'lastTPIdx')
        
        % Load in the variables
            position = handles.position;
            angle  = handles.angle;
            firstTPIdx = handles.firstTPIdx;
            lastTPIdx = handles.firstTPIdx;

        if ~isempty(angle) && ~isempty(position) && ~isempty(firstTPIdx) && ~isempty(lastTPIdx)  

             selection = questdlg('Save alignment results?',...
                                 'Close Request Function',...
                                 'Yes, save the data and close the figure','No, please delete the data and close the figure','I would like to keep working on this','Yes');
             switch selection,
               case 'Yes, save the data and close the figure',
                   % Load in the variables
                        position = handles.position;
                        angle  = handles.angle;
                        firstTPIdx = handles.firstTPIdx;
                        lastTPIdx = handles.lastTPIdx;
                    % Assign the variables to the workspace
                        assignin('base','angle',angle)
                        assignin('base','position',position)
                        assignin('base','firstTPIdx',firstTPIdx)
                        assignin('base','lastTPIdx',lastTPIdx)   
                    % Close the figure
                        delete(hObject);
               case 'No, please delete the data and close the figure'     
                    % Close the figure
                        delete(hObject);
               case 'I would like to keep working on this'     
                    return
             end
        end
    else
         selection = questdlg('One or more variables have not been selected. Are you sure you want to close the figure?',...
                             'Close Request Function',...
                             'Yes','No, I would like to keep working','Yes');
         switch selection,
           case 'Yes',
                % Close the figure
                    delete(hObject);
           case 'No, I would like to keep working'        
                return
         end
    end
 
