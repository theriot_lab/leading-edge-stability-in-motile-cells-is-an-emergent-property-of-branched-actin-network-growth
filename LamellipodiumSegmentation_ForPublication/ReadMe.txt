This MATLAB code was written by Rikki M. Garner as a companion to the manuscript titled "Leading edge stability in motile cells in an emergent property of branched actin network growth", and was last updated 2020/08/19.

1. To run the code, start by opening MATLAB ands adding the entire folder, LamellipodiumSegmentation_ForPublication to your MATLAB path. You also need to make sure you have the Image Analysis and Statistics and Machine Learning toolboxes installed.

2. Unzip the example movie file 180111_HighMagKW_1PercentDMSOCtrl.zip into the Example Movie folder.

3. Update the following lines to your personal MATLAB path: alignAndCropFromOME line 76; runLeadingEdgeSegmentation.m line 33; Run_baselineSubtr_spatialFFTModeTimeAutocorrelationAnalysis.m lines 10 and 18;
runPlottingOfSpatialFFTModeTimeAutocorrSummaryStats_Expmts.m line 18

4. Run the alignment and cropping step using alignAndCropFromOME.m

5. Run the segmentation and velocity/curvature analysis code: runLeadingEdgeSegmentationAndAnalysis.m

6. Run the spatial Fourier mode time-autocorrelation analysis using Run_baselineSubtr_spatialFFTModeTimeAutocorrelationAnalysis.m

7. To visualize the results, runPlottingOfSpatialFFTModeTimeAutocorrSummaryStats_Expmts.m to plot the autocorrelation analysis.
