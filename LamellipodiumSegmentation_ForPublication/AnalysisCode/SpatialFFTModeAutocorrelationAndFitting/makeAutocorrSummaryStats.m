function [autocorrDataConcatenated] = makeAutocorrSummaryStats(autocorrDataConcatenated)

%% This function loops through all the autcorrelation fit data and creates 
% summary statistics as a function of condition
% (Run as part of plotSpatialFFTModeTimeAutocorrSummaryStats_Sims.m)

% This code was written by Rikki M. Garner as a companion to the manuscript
% titled "Leading edge stability in motile cells in an emergent property of
% branched actin network growth", and was last updated 2020/08/19.

%%


    % Unpack and delete the structure
        v2struct(autocorrDataConcatenated)
        clear autocorrDataConcatenated

        
    % Pre-allocate space to store the data 
        % Frequency values
            spatialfrequencyInInverseMicronsAverages = [];
            groupIdentityAverages = [];
        % Medians        
            decayConstMedians = [];
            ampConstMedians = [];
        % STD        
            decayConstSTD = [];
            ampConstSTD = [];
        % SE        
            decayConstSE = [];
            ampConstSE = [];
        % Means        
            decayConstMeans = [];
            ampConstMeans = [];   
        
    % Loop through each bin and calculate the average fourier amplitude for
    % each condition                 
    for numCondition = 1:numConditions
        
        % Determine the bin size for the spatial frequency
            if strcmp(binFlag,'binByMedian')
                spatialFrequencyBinSizeInInverseNanometers = nanmedian(diff_fVals);
            elseif strcmp(binFlag,'binSeparatelyByCondition')
                % Pull out the fVal step sizes for this condition
                    diff_fVals_2Analyze = diff_fVals(conditionIdentity==numCondition);
                % Find the most common bin size 
                    spatialFrequencyBinSizeInInverseNanometers = ...
                        mode(diff_fVals_2Analyze(~isnan(diff_fVals_2Analyze)));
            elseif strcmp(binFlag,'binManually')
                spatialFrequencyBinSizeInInverseNanometers = manualspatialFrequencyBinSizeInInverseNanometers;           
            end
        
        % Bin the frequencies (ceil so always an integer >0)
%             binnedFVals = ceil(spatialFrequencyInInverseNanometers(...
%                 conditionIdentity==numCondition)./...
%                 spatialFrequencyBinSizeInInverseNanometers);
            binnedFVals = ceil((spatialFrequencyInInverseNanometers(...
                conditionIdentity==numCondition) - ...
                (spatialFrequencyBinSizeInInverseNanometers/2))./...
                spatialFrequencyBinSizeInInverseNanometers);
        % Calculate the number of bins that exist in the data
            maxNumFreqBins = nanmax(binnedFVals(:));
            if isempty(maxNumFreqBins)
                maxNumFreqBins = 0;
            end
        % Concatenate these binned frequencies
            spatialfrequencyInInverseMicronsAverages = ...
                [spatialfrequencyInInverseMicronsAverages ...
                spatialFrequencyBinSizeInInverseNanometers*10^3*(1:maxNumFreqBins)];
        % Concatenate the group identity
            groupIdentityAverages = [groupIdentityAverages numCondition*ones([1 maxNumFreqBins])];
  
        for numFreqBin = 1:maxNumFreqBins

            % Pull out the indices for this frequency and group
                inFreqAndGroupLogicalIdx = (conditionIdentity==numCondition);
                inFreqAndGroupLogicalIdx(inFreqAndGroupLogicalIdx) = (binnedFVals==numFreqBin);

            % Pull out outliers for each measurement
                % Amplitude of exponential decay part
                    % Determine the points that aren't outliers
                        inFreqAndGroupLogicalIdxNoOutlierAmpConst = inFreqAndGroupLogicalIdx;
                        inlierAmpConst = ~isoutlier(ampConst(inFreqAndGroupLogicalIdxNoOutlierAmpConst));
                        inFreqAndGroupLogicalIdxNoOutlierAmpConst(inFreqAndGroupLogicalIdxNoOutlierAmpConst) = inlierAmpConst;
                    % Pull out the points that aren't outliers   
                        ampConst2Avg = ampConst(inFreqAndGroupLogicalIdxNoOutlierAmpConst); 
                % Decay constant
                    % Determine the points that aren't outliers
                        inFreqAndGroupLogicalIdxNoOutlierDecayConst = inFreqAndGroupLogicalIdx;
                        inlierDecayConst = ~isoutlier(decayConst(inFreqAndGroupLogicalIdxNoOutlierDecayConst));
                        inFreqAndGroupLogicalIdxNoOutlierDecayConst(inFreqAndGroupLogicalIdxNoOutlierDecayConst) = inlierDecayConst;
                    % Pull out the points that aren't outliers    
                        decayConst2Avg = decayConst(inFreqAndGroupLogicalIdxNoOutlierDecayConst);                   
                % If there are supposed to be replicates, don't bother 
                % plotting the values with fewer than 2 non-nan results   
                    % Calculate the number of replicates
                        numReplicates = numReplicatesByCondition(numCondition);
                    if numReplicates > 1
                        if sum(~isnan(ampConst2Avg))<2
                            ampConst2Avg = nan;
                        end
                        if sum(~isnan(decayConst2Avg))<2
                            decayConst2Avg = nan;
                        end
                    end
             % Calculate the summary statistics
                ampConstMedians = [ampConstMedians nanmedian(ampConst2Avg)];
                decayConstMedians = [decayConstMedians nanmedian(decayConst2Avg)];

                ampConstSTD = [ampConstSTD nanstd(ampConst2Avg)];
                decayConstSTD = [decayConstSTD nanstd(decayConst2Avg)];

                ampConstSE = [ampConstSE nanstd(ampConst2Avg)/...
                    sqrt(sum(~isnan(ampConst2Avg)))];
                decayConstSE = [decayConstSE nanstd(decayConst2Avg)/...
                    sqrt(sum(~isnan(decayConst2Avg)))];

                ampConstMeans = [ampConstMeans nanmean(ampConst2Avg)];
                decayConstMeans = [decayConstMeans nanmean(decayConst2Avg)];
                    
        end
    end
    
    autocorrDataConcatenated = v2struct();
    
end