function [autocorrDataConcatenated] = prepSpatialFFTModeAutocorrSummaryStatsByCondition_Experiments(autocorrDataConcatenated)
     
% Unpack the structure
    v2struct(autocorrDataConcatenated)

% Preallocate space to store the results
    % Calculate the number of unique conditions
        numConditions = length(unique(conditionByFolder));
    % Number of cells in each group
        numReplicatesByCondition = zeros(1,numConditions);
    % Spatial frequency
        spatialFrequencyInInverseNanometers = [];
    % Mean step between spatial frequency values
        diff_fVals = [];
    % Fitted exponential decay amplitudes    
        ampConst = [];
    % Fitted exponential decay rates    
        decayConst = [];
    % Condition number    
        conditionIdentity = [];
    % Replicate number (cell number within condition)    
        replicateIdentity = [];
    % Span used in background subtraction       
        spanInNanometers = [];

% Create the plot elements
    % Create an empty legend text cell
        legendText = conditionNames;
    % Choose the colormap 
    if numConditions == 1 
        CMByCondition = [0 0 0];
    elseif numConditions == 2
        CMByCondition = [0.9 1 0; 0 0 1];
    elseif numConditions == 3
        CMByCondition = [0 1 0; 0 0 1; 1 0 1];
    elseif numConditions == 4
        CMByCondition = [0 1 0; 0 1 1; 0 0 1; 1 0 1];
    elseif numConditions == 5
        CMByCondition = [0.9 1 0; 0 1 0; 0 1 1; 0 0 1; 1 0 1];
    elseif numConditions == 6        
        CMByCondition = [0.9 1 0; 1 0.5 0; 0 1 0; 0 1 1; 0 0 1; 1 0 1];
    elseif numConditions == 7       
        CMByCondition = [0.9 1 0; 1 0.5 0; 0 1 0; 0 1 1; 0 0 1; 1 0 1; 0 0 0];
    elseif numConditions > 7       
        CMByCondition = jet(numConditions);
    end            
            
% Save this information into a structure
    autocorrDataConcatenated = v2struct();
    clearvars -except autocorrDataConcatenated 

% Loop through each dataset and pull out the data
for numFolder = 1:length(autocorrDataConcatenated.folderPaths)
    
    % Load the folder path
        folderPath = autocorrDataConcatenated.folderPaths{numFolder};
        folderPathInfo =  dir(folderPath);
        
    % Load in the video data
        videoInfoIdx = strfind({folderPathInfo.name},'videoInfo');
        videoInfoIdx = find(~cellfun('isempty', videoInfoIdx));
        eval(['load(' '''' folderPathInfo(videoInfoIdx,1).folder '/' folderPathInfo(videoInfoIdx,1).name '''' ')'])
        eval(['videoInfoMajor =' folderPathInfo(videoInfoIdx,1).name(1:(end-4)) ';'])
        eval(['clear ' folderPathInfo(videoInfoIdx,1).name(1:(end-4))])
        clear videoInfoIdx
        
    % Pull out the condition number for this folder
        conditionNum = autocorrDataConcatenated.conditionByFolder(numFolder);
        
    % Loop through each dataset and save the data    
    for numVideoMajor = 1:videoInfoMajor.numVideos 
        
        % Load in the data
            load([videoInfoMajor.saveAnalysisFolderPath videoInfoMajor.saveAnalysisFileNames{numVideoMajor} '.mat'] );
            eval( [ 'LEFAnalysisResults = ' videoInfoMajor.saveAnalysisFileNames{numVideoMajor} ';'])
        % Unpack the analysis data
            v2struct(LEFAnalysisResults)
            clear LEFAnalysisResults
            eval(['clear ' videoInfoMajor.saveAnalysisFileNames{numVideoMajor}])
            
       % Save the results    
            % Update the cell count
                autocorrDataConcatenated.numReplicatesByCondition(conditionNum) = ...
                    autocorrDataConcatenated.numReplicatesByCondition(conditionNum)+1;
            % Pull out the cell number
                replicateNumber = autocorrDataConcatenated.numReplicatesByCondition(conditionNum);            
            % Pull out the number of wavemodes in this dataset
                numWavemodes = length(spatialFFTModeTimeAutocorrFits.spatialFrequencyInInverseNanometers);
            % Save the data   
                % Spatial frequency
                    autocorrDataConcatenated.spatialFrequencyInInverseNanometers = ...
                        [autocorrDataConcatenated.spatialFrequencyInInverseNanometers ...
                        spatialFFTModeTimeAutocorrFits.spatialFrequencyInInverseNanometers];
                % Mean step between spatial frequency values
                    meanDiffspatialFrequencyInInverseNanometers = mean(diff(spatialFFTModeTimeAutocorrFits.spatialFrequencyInInverseNanometers));
                    autocorrDataConcatenated.diff_fVals = [autocorrDataConcatenated.diff_fVals ...
                        meanDiffspatialFrequencyInInverseNanometers.*ones([1 numWavemodes])];
                % Fitted exponential decay amplitudes    
                    autocorrDataConcatenated.ampConst = [autocorrDataConcatenated.ampConst ...
                        spatialFFTModeTimeAutocorrFits.expAmpConst];
                % Fitted exponential decay rates    
                    autocorrDataConcatenated.decayConst = [autocorrDataConcatenated.decayConst ...
                        spatialFFTModeTimeAutocorrFits.expDecayConst];
                % Condition number    
                    autocorrDataConcatenated.conditionIdentity = [autocorrDataConcatenated.conditionIdentity ...
                        conditionNum.*ones([1 numWavemodes])];
                % Replicate number (cell number within condition)    
                    autocorrDataConcatenated.replicateIdentity = [autocorrDataConcatenated.replicateIdentity ...
                        replicateNumber.*ones([1 numWavemodes])];
                % Span used in background subtraction       
                    autocorrDataConcatenated.spanInNanometers = ...
                        spatialFFTModeTimeAutocorrData.spanInNanometers;


        clearvars -except autocorrDataConcatenated numFolder folderPath folderPathInfo videoInfoMajor conditionNum numVideoMajor alignmentAndCroppingDataMajor 

    end
    
    clearvars -except autocorrDataConcatenated numFolder
 
end


end