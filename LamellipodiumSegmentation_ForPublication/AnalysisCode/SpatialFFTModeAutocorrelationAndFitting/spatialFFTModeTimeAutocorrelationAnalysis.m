%% This script performs the spatial Fourier mode autocorrelation analysis on
% leading edge shape fluctuation data (experimentally measured or
% simulated). First, it performs a spatial Fourier transform on the 
% leading edge fluctuation data, then performs time-autocorrelation analysis
% separtely on each spatial Fourier mode. It then performs a best fit of 
% the autocorrelation decay curves to an exponential decay.
% (Run as part of Run_spatialFFTModeTimeAutocorrelationAnalysis.m or
% simulateLamellipodiumAndRunXCorrAnalysis.m or
% Run_baselineSubtr_spatialFFTModeTimeAutocorrelationAnalysis.m)

% This code was written by Rikki M. Garner as a companion to the manuscript
% titled "Leading edge stability in motile cells in an emergent property of
% branched actin network growth", and was last updated 2020/08/19.
%%

% Perform the spatial Fourier transform

    % Pull out the timsteps to analyze
        timeIdx2AnalyzeAutocorr = ...
            timePoint2StartAutocorr:numTimePoints2SkipAutocorr:numTimePoints;
    
    % Calculate the the spatial frequency information
        % Choose the number of modes to analyze (including the zeroth
        % order)
            numModes2Autocorr = round(numMemSegments/3);        
        % Calculate a vector of wavenumbers in # wavelengths per leading edge size
            wavNums = 0:(numModes2Autocorr-1);    
        % Convert the wavenumber to a spatial frequency in 1/nm
            spatialFrequencyInInverseNanometers = wavNums/leadingEdgeLengthInNanometers;
        % Also convert to angular frequency    
            qVals = 2*pi*spatialFrequencyInInverseNanometers;
     
	% Perform the Fourier transform
        % Perform the Fourier transform
            FFTYPosMemInNanometers = fft(yPosMemInNanometers(timeIdx2AnalyzeAutocorr,:),[],2);      
        % Take only the first half (and normalize the Fourier transform to
        % retain units of nm and to preserve the variance)
            FFTYPosMemInNanometers = FFTYPosMemInNanometers(:,wavNums+1).*...
                nanometersPerXValInc./sqrt(leadingEdgeLengthInNanometers);  
        
% Perform temporal autocorrelation of the FFT mode amplitudes        
        
    % Perform the autocorrelation 
        [rVals] = calculateAutocorrelationNonOverlapping(FFTYPosMemInNanometers);

    % Calculate the time lags 
        lagPosInMilliseconds = ...
            (0:(size(rVals,2)-1))*millisecondsPerFrame*numTimePoints2SkipAutocorr;               
        
% Fit the autocorrelation decay curves to an exponential decay out to
% r(tau) = r(millisecondsPerFrame)/(e/2), or at least 5 time lags

    % Pre-allocate space to store the fit parameters
        expDecayConst = nan([1 numModes2Autocorr]);
        expAmpConst = nan([1 numModes2Autocorr]);
        numTimeLags2Fit = nan([1 numModes2Autocorr]);
        MEAutocorrFit = cell([1 numModes2Autocorr]); 
          
   % Loop through all of the wavemodes and fit the autocorrelation decay                    
    for n = 1:(numModes2Autocorr-1)

        % Pull out the autcorrelation and time data vectors, ignoring the first noisy
        % tau=0 data point
            xVals = lagPosInMilliseconds(2:end);
            yVals = abs(rVals(n,2:end));

        % Perform the fit
            [fitCoefficients, numTimeLags2Fit(n), MEAutocorrFit{n}] = ...
                fitExponentialDecay(xVals,yVals);                     

        % Record the results
            expAmpConst(n) = fitCoefficients(1);
            expDecayConst(n) = fitCoefficients(2);
            
    end


% Optionally loop through all of the wavemodes and plot the exponential decay
if doPlot        

	% Choose the modes to plot
        modes2Plot = 5:1:12;
        nModes2Plot = length(modes2Plot);
    % Create a colormap
        CM2 = jet(nModes2Plot);      
        
    for nMode = 1:nModes2Plot
        
        n = modes2Plot(nMode);
        
        % Pull out the autcorrelation and time data vectors, ignoring the first noisy
        % t=0 data point
            xVals = lagPosInMilliseconds(2:end);
            yVals = abs(rVals(n,2:end));
                            
        % Calculate the best fit line
            bestFitLine = expAmpConst(n).*exp(-expDecayConst(n).*xVals);

        figure(4002)
        plot(xVals/1000,yVals,'-o','color',CM2(nMode,:))
        hold on;
        plot(xVals/1000,bestFitLine,'-','color',CM2(nMode,:),'Linewidth',2)
        if nMode == nModes2Plot
            xlabel('Time offset (s)')
            ylabel('Autocorrelation amplitude (nm^2)')
            title({'Example of fit','ae^{-bt} + c'})
            set(gca,'FontName','Arial','FontSize',18,'FontWeight','Bold');
            set(gca,'YScale','log')
            legend({'Raw Data','Best fit of ae^{-bt}sin(dt)+f'})
            ylim([10^0 10^8])
            hold off;                                    
        end
    end
end
        
% Optionally plot the results
if doPlot==1

    % Plot the amplitude as a function of wavemode
        figure(2)
        subplot(1,2,1)
        loglog(spatialFrequencyInInverseNanometers,expAmpConst,'k-o')
        ylabel({'Fitted amplitude (nm^2)','of exponential decay part'})
        xlabel('Spatial frequency (nm^{-1})')
        title({'Exponential decay component of','mode autocorrelation by condition'})  
        set(gca,'FontName','Arial','FontSize',20,'FontWeight','Bold');
    % Plot the decay constant as a function of wavemode
        figure(2);
        subplot(1,2,2)
        loglog(spatialFrequencyInInverseNanometers,expDecayConst,'k-o')
        title('Amplitude decay constant vs wavemode')
        ylabel({'Fitted decay constant (nm^2/ms)','of exponential decay part'})
        xlabel('Spatial frequency (nm^{-1})')
        title({'Exponential decay component of','mode autocorrelation by condition'}) 
        set(gca,'FontName','Arial','FontSize',20,'FontWeight','Bold');

end

