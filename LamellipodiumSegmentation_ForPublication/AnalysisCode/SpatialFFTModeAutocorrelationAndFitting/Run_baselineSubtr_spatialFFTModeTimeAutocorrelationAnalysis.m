%% The following code takes the membrane position data over time and performs the following tasks...
% 1. Take the spatial Fourier transform
% 2. Takes the autocorrelation of each mode with itself over time
% 3. Fits the autocorrelation over time to an exponential decay
% 4. Fits the exponential decay as a function of mode number to a power law

%% Prepare the  dataatialFFTModeAutocorrelationAndFitting/']);

    % Clear everything
        close all;
        clear all;
        
    % Choose the data to analyze
        superFolderPaths = {['/Users/Rikki/Documents/'...
            'LamellipodiumSegmentation_ForPublication/ExampleMovie/'...
            '180111_HighMagKW_1PercentDMSOCtrl_Analyzed/']};
         
     % Choose the span for the baseline subtraction
        spanInNanometers = 7000;
     % Choose whether to plot the fits as you go
        doPlot=1;

     % Load this information into a structure and clear everything else 
        plottingInfo = v2struct();
        clearvars -except plottingInfo
   
    for numCoverslip = 1:length(plottingInfo.superFolderPaths)
        
        % Tell the program where to find the image files
            superFolderPath = plottingInfo.superFolderPaths{numCoverslip};
            superFolderPathInfo =  dir(superFolderPath);
        % Load in the video data
            videoInfoIdx = strfind({superFolderPathInfo.name},'videoInfo');
            videoInfoIdx = find(~cellfun('isempty', videoInfoIdx));
            eval(['load(' '''' superFolderPathInfo(videoInfoIdx,1).folder '/' superFolderPathInfo(videoInfoIdx,1).name '''' ')']);
            eval(['videoInfoMajor =' superFolderPathInfo(videoInfoIdx,1).name(1:(end-4))]);
            eval(['clear ' superFolderPathInfo(videoInfoIdx,1).name(1:(end-4))]);
            clear videoInfoIdx
            
            
        for numVideoMajor = 1:videoInfoMajor.numVideos    

            % Load in the data
                load([videoInfoMajor.saveAnalysisFolderPath videoInfoMajor.saveAnalysisFileNames{numVideoMajor} '.mat'] );
                eval( [ 'LEFAnalysisResults = ' videoInfoMajor.saveAnalysisFileNames{numVideoMajor} ]);
            % Unpack the analysis data
                v2struct(LEFAnalysisResults);
                eval(['clear ' videoInfoMajor.saveAnalysisFileNames{numVideoMajor}]);

            % Do baseline subtraction timepoint-by-timepoint  
            
                % Convert the data to nanometers and seconds
                    yPosValsInNanometers = (size(Image,1) - yPosFit)*micronsPerPixel*1000;
                    yPosValsInNanometersCropped = yPosValsInNanometers(:,xMinPix:xMaxPix);
                % Convert the data to nanometers and seconds
                    xPosValsInNanometers = xPosValsInMicronsTotal*1000;
                    xPosValsInNanometersCropped = xPosValsInNanometers(xMinPix:xMaxPix);                    
          
                % Do baseline subtraction timepoint-by-timepoint
                    % Choose the span in #timepoints
                        spanInNanometers = plottingInfo.spanInNanometers;
                    % Choose the span in #timepoints
                        spanInIdx = spanInNanometers/(micronsPerPixel*1000);
                    % Pre-allocate space to store teh new positions
                        baselineYPosValsInNanometers = nan(size(yPosSmooth));
                    % Loop through each timepoint
                        for t = 1:numTimePoints
                            
                            % Pull out the edges and the indices to keep
                                edgeIdx = floor((t-1)/idxBetweenEdgeRecord)+1;
                                idx2Smooth = (leadingEdgeEndPoints(edgeIdx,1)+1):(leadingEdgeEndPoints(edgeIdx,2)-1);

                            % Perform the smoothing
                                % Determine the span in terms of a fraction of the total vector
                                    spanInFracOfTotal = spanInIdx/length(idx2Smooth);
                                % Stop the fitting if the leading edge is
                                % too small, otherwise run the smoothing
                                    if spanInFracOfTotal>=1
                                        'Error: Smoothing window is larger than the leading edge segment'
                                        break
                                    else
                                        % Run the smooth
                                            baselineYPosValsInNanometers(t,idx2Smooth) = ...
                                                smooth(yPosValsInNanometers(t,idx2Smooth),spanInFracOfTotal,'lowess');
                                    end
                                 
%                             % Optionally plot the results
%                                 figure(60)
%                                 plot(xPosValsInMicronsTotal(idx2Smooth)*1000,yPosValsInNanometers(t,idx2Smooth) ...
%                                 - baselineYPosValsInNanometers(t,idx2Smooth) + nanmean(yPosValsInNanometers(t,idx2Smooth)),'g')
%                                 hold on;
%                                 plot(xPosValsInMicronsTotal(idx2Smooth)*1000,yPosValsInNanometers(t,idx2Smooth),'r')
%                                 plot(xPosValsInMicronsTotal(idx2Smooth)*1000,baselineYPosValsInNanometers(t,idx2Smooth),'b')
%                                 hold off;
%                                 xlabel('X-position (nm)')
%                                 ylabel('Y-position (nm)')
%                                % ylim([min(min(yPosValsInNanometersCropped)) max(max(yPosValsInNanometersCropped))])
%                                 axis equal
%                                 legend('Baseline-corrected leading edge shape','Raw leading edge shape',sprintf('Loess-smoothed shape, \\lambda = %d nm',plottingInfo.spanInNanometers))
%                                 title('Example of position smoothing')
%                                 set(gca,'FontName','Arial','FontSize',18,'FontWeight','Bold');
%                                ginput
                           
                        end

                    % Continue to stop the function of the smoothing
                    % window is larger than the cell
                        if spanInFracOfTotal>=1
                            'Error: Smoothing window is larger than the leading edge segment'
                            break
                        end
                        
                    % Crop baseline vector and do baseline subtraction   
                        baselineYPosValsInNanometersCropped = baselineYPosValsInNanometers(:,xMinPix:xMaxPix);
                        yPosDiffValsInNanometersCropped = yPosValsInNanometersCropped - baselineYPosValsInNanometersCropped;
                    % Cut off NANs at sides
                        nanIdx = [0 find(any(isnan(yPosDiffValsInNanometersCropped))) size(yPosDiffValsInNanometersCropped,2)];
                        if length(nanIdx) == 2
                        else
                            nonNanDiff = diff(nanIdx);
                            firstNonNanIdx2Keep = nanIdx(find(nonNanDiff == max(nonNanDiff)))+1;
                            lastNonNanIdx2Keep = nanIdx(find(nonNanDiff == max(nonNanDiff))+1)-1;
                            yPosValsInNanometersCropped = yPosValsInNanometersCropped(:,firstNonNanIdx2Keep:lastNonNanIdx2Keep);
                            curveVal2plot = curveVal2plot(:,firstNonNanIdx2Keep:lastNonNanIdx2Keep);
                            yPosDiffValsInNanometersCropped = yPosDiffValsInNanometersCropped(:,firstNonNanIdx2Keep:lastNonNanIdx2Keep);
                            baselineYPosValsInNanometersCropped = baselineYPosValsInNanometersCropped(:,firstNonNanIdx2Keep:lastNonNanIdx2Keep);
                            xPosValsInNanometersCropped = xPosValsInNanometersCropped(firstNonNanIdx2Keep:lastNonNanIdx2Keep);
                        end
                    % Make sure each x-position has zero mean positional
                    % variability over all time
                        yPosDiffValsInNanometersCropped = bsxfun(@minus,yPosDiffValsInNanometersCropped,mean(yPosDiffValsInNanometersCropped)); 
                    
                   % Save the baseline subtracted positisions      
                        spatialFFTModeTimeAutocorrData.spanInNanometers = spanInNanometers;
                        spatialFFTModeTimeAutocorrData.yPosDiffValsInNanometersCropped = yPosDiffValsInNanometersCropped;
                        spatialFFTModeTimeAutocorrData.xPosValsInNanometersCropped = xPosValsInNanometersCropped;
                        spatialFFTModeTimeAutocorrData.timeValsInSeconds = timeValsInSeconds;
                        spatialFFTModeTimeAutocorrData.curveVal2plot = curveVal2plot;
                    
                        
            % Perform the spatial FFT, wavemode autocorrelation, and
            % fit of the autocorrelation decay curves

                % Prepare the data
                    % Give the relevant variables generic names
                        numMemSegments = length(spatialFFTModeTimeAutocorrData.xPosValsInNanometersCropped);         
                        nanometersPerXValInc = nanometersPerPixel;
                        leadingEdgeLengthInNanometers = nanometersPerXValInc.*numMemSegments;
                        yPosMemInNanometers = spatialFFTModeTimeAutocorrData.yPosDiffValsInNanometersCropped;
                    % Choose whether to plot in the function
                        doPlot=0;
                    % Pull out the number of timesteps to skip (none
                    % for experimental data)
                        numTimePoints2SkipAutocorr = 1;
                    % Choose the timepoint to start the analysis (the first
                    % for experimental data)   
                        timePoint2StartAutocorr = 1;

                % Run the spatial FFT, wavemode autocorrelation, and
                % fit of the autocorrelation decay curves        
                    spatialFFTModeTimeAutocorrelationAnalysis                            
                    
                % Save the results in the structure   
                    spatialFFTModeData.spatialFrequencyInInverseNanometers = spatialFrequencyInInverseNanometers;
                    spatialFFTModeData.numTimePoints2SkipAutocorr = numTimePoints2SkipAutocorr;
                    spatialFFTModeData.FFTYPosMemInNanometers = FFTYPosMemInNanometers;
                % Save the autocorrelations to a structure    
                    spatialFFTModeTimeAutocorrData.rVals = rVals;
                    spatialFFTModeTimeAutocorrData.lagPosInMilliseconds = lagPosInMilliseconds;
                    spatialFFTModeTimeAutocorrData.numTimePoints2SkipAutocorr = numTimePoints2SkipAutocorr;
                % Save the fit results to a structure     
                    spatialFFTModeTimeAutocorrFits.spatialFrequencyInInverseNanometers = spatialFrequencyInInverseNanometers;
                    spatialFFTModeTimeAutocorrFits.expDecayConst = expDecayConst;
                    spatialFFTModeTimeAutocorrFits.expAmpConst = expAmpConst;   
                    spatialFFTModeTimeAutocorrFits.numTimeLags2Fit = numTimeLags2Fit;    
                    spatialFFTModeTimeAutocorrFits.MEAutocorrFit = MEAutocorrFit; 
                           
            % Save the data
                LEFAnalysisResults.spatialFFTModeData = spatialFFTModeData;
                LEFAnalysisResults.spatialFFTModeTimeAutocorrData = spatialFFTModeTimeAutocorrData;
                LEFAnalysisResults.spatialFFTModeTimeAutocorrFits = spatialFFTModeTimeAutocorrFits;
                eval([videoInfoMajor.saveAnalysisFileNames{numVideoMajor,1} ' = LEFAnalysisResults']);
                save([videoInfoMajor.saveAnalysisFolderPath videoInfoMajor.saveAnalysisFileNames{numVideoMajor,1}],videoInfoMajor.saveAnalysisFileNames{numVideoMajor,1},'-v7.3');
                    
            clearvars -except plottingInfo numCoverslip numVideoMajor videoInfoMajor alignmentAndCroppingDataMajor dataNum

        end
        
        if exist('spanInFracOfTotal')
            if spanInFracOfTotal>=1
                'Error: Smoothing window is larger than the leading edge segment'
                break
            end
        end
       
        clearvars -except plottingInfo numCoverslip dataNum
    end

if exist('spanInFracOfTotal')
   if spanInFracOfTotal>=1
    'Error: Smoothing window is larger than the leading edge segment'
    return
   end
end
                                      

