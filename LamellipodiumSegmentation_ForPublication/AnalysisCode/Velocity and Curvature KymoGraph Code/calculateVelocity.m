function [myvelocities] = calculateVelocity(ypos)

%% This function takes in a time-lapse series of a line 
% object moving in the vertical direction. It calculates the velocity of 
% each point along the line in the vertical direction.

% Input:
    % ypos = An NxM array (where N is the number of frames in the video
        % and M is the horizontal size of the frame) containing the value 
        % of the vertical position for each point along the leading edge.
        % Coordinates such that (0,0) is in the top left corner.
        
% (This function is called by analyzeContourFluctuations.m)
        
% This code was written by Rikki M. Garner as a companion to the manuscript
% titled "Leading edge stability in motile cells in an emergent property of
% branched actin network growth", and was last updated 2020/08/19.

%% Calculate the velocity along the column-direction, and color the boundary image with boundary velocity 
    myvelocities=[];
    numTimePoints = size(ypos,1);
    numLeadingEdgePoints = size(ypos,2);
    
    for k = 1:20 % 20 with 50ms intervals is 1 sec
        tskip = k+1;
        tMax = floor(numTimePoints/tskip)*tskip;
        numSegments = tMax/tskip;
        myvelocities{k} = (ypos(tskip:tskip:tMax,:)-ypos(1:tskip:(tMax-k),:))/k;
    end

    
    
    
    
    
    
    