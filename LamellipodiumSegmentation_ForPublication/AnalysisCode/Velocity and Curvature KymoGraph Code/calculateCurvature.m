function [curveVal] = calculateCurvature(outline,seg)

%% This function takes in a clockwise ordered list of pixel coordinates for 
    % object boundary and calculates the local curvature at each point 
    % across the boundary. The curvature is calculated by fitting a circle 
    % to the curve. The curvature is defined by the inverse of the radius 
    % of that fitted circle

% Inputs
    % outline = A 1xN cell array (where N is the number of frames in the
        % video) containing a clockwise list of boundary pixel coordinates 
        % of the object of interest for each frame. The coordinates 
        % are defined by (0,0) being in the top left corner.
    % seg = the number of frames skipped to calculate the velocity
        
% Outputs
    % curveI = A 1xN cell array (where N is the number of frames in the
        % video) containing the boundary of the cell colored by its
        % calculated curvature value
    % curveVal = An NxM array (where N is the number of frames in the video
        % and M is the horizontal size of the frame) containing the value 
        % of the curvature for each point along the leading edge.
        
% (This function is called by analyzeContourFluctuations.m)

% This code was written by Rikki M. Garner as a companion to the manuscript
% titled "Leading edge stability in motile cells in an emergent property of
% branched actin network growth", and was last updated 2020/08/19.

%% Calculate the radius of curvature from the ordered list of boundary
% coordinates 
    myOut=[];
    nPoints=[];
    x=[];
    y=[];
    c=[];
    r=[];
%     seg = 15; % Size of segment to detect curvature
    for n = 1:size(outline,2) % For all frames in the video
        myOut = outline{1,n}; % Import ordered list of edge points
        nPoints = size(myOut,1); % Number of points comprising edge
        for m = 1:nPoints % For each point on the boundary
            % Pull out the vector of coordinates to fit (from m-seg to
            % m+seg)
            if m-seg <= 0 % If you need to loop backwards to the end of the list
                y = [myOut(nPoints+(m-seg),1) myOut(m,1) myOut(m+seg,1)]; % Pull out the row positions  
                x = [myOut(nPoints+(m-seg),2) myOut(m,2) myOut(m+seg,2)]; % Pull out the column positions
            elseif m+seg > nPoints % If you need to loop forwards to the beginning of the list
                y = [myOut(m-seg,1) myOut(m,1) myOut((m+seg)-nPoints,1)];
                x = [myOut(m-seg,2) myOut(m,2) myOut((m+seg)-nPoints,2)];
            else 
                y = [myOut(m-seg,1) myOut(m,1) myOut(m+seg,1)];
                x = [myOut(m-seg,2) myOut(m,2) myOut(m+seg,2)];
            end
            [center,rad] = circlefit3d([x(1) y(1) 0],[x(2) y(2) 0], [x(3) y(3) 0]);
            if ~isempty(center) 
                c(n,m,:) = center;
                r(n,m) = rad;
            else 
                c(n,m,:) = 0;
                r(n,m) = 0;
            end
            if c(n,m,2) > myOut(m,1)
                    r(n,m) = -r(n,m); % Make the radius negative if the center lies below the curve
            end
        end
    end
nanVals = find(isnan(r))'; % Get rid of all NaN values (set them equal to infinity)
r(nanVals) = Inf;
curveVal = 1./r; % At that position, make the value equal to the curvature (the inverse of the radius)  

% % Make an image where the intensity of the boundary pixel is equal to its curvature
%     myOut=[];
%     nPoints=[];
%     xVal=[];
%     yVal=[];
%     curveVal=[];
% %     curveI=[];
%     for n = 1:size(outline,2) % For each time point
%         myOut = outline{1,n}; % Pull out the ordered list of outline points
%         nPoints = size(myOut,1); % Pull out the number of points on the list
%         newI = zeros(maxZPos2Plot,nBeads);
%         for m = 1:nPoints % For each point in the ordered list
%             yVal = myOut(m,1); % Pull out the row number
%             xVal = myOut(m,2); % Pull out the column number
%             if c(n,m,2) < yVal
%                     r(n,m) = -r(n,m); % Make the radius negative if the center lies below the curve
%             end
%             newI(yVal,xVal) = 1/r(n,m); % At that position, make the value equal to the curvature (the inverse of the radius)
%         end
% %         curveI{n} = newI; % Save the curvature image for each time point
% %         size(curveI)
%     end

% % Eliminate the row variable (leaving a time point x column number heat map
% % of curvature intensity)
%     myI=[];
%     curveVal=[];
%     for n = 1:size(curveI,2) % For each time point
%         myI = curveI{1,n}; % Pull out the curve value image
%         for m = 1:size(myI,2) % For each column of the image
%             [y x] = find(myI(:,m),1); % Find the coordinates first non-zero pixel in each column (x will always be one) 
%             if isempty(y)
%                 curveVal(n,m) = 0;
%             else
%                 curveVal(n,m) = myI(y,m); % Make the value equal to the curvature (the inverse of the radius)
%                     % where n is the time point and m is the column number
%             end
%         end
%     end

nanVals = find(isnan(curveVal))'; % Get rid of all NaN values
curveVal(nanVals) = 0;
infVals = find(isinf(curveVal))'; % Get rid of +-Inf values by resetting them to largest nonzero value
for n = 1:length(infVals)
    myk = infVals(n);
    if curveVal(myk) > 0
        curveVal(myk) = max(max(curveVal<Inf));
    elseif curveVal(myk) < 0
        curveVal(myk) = min(min(curveVal>Inf));
    end
end
