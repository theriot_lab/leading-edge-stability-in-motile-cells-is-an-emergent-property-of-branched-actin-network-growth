%% This script calclates the local velocity and curvature of each point 
% along the leading edge, and then generates a kymograph of each to 
% visualize the results.

% (This function is called by runLeadingEdgeSegmentation.m)

% This code was written by Rikki M. Garner as a companion to the manuscript
% titled "Leading edge stability in motile cells in an emergent property of
% branched actin network growth", and was last updated 2020/08/19.

%% Input conversions, and prepare the temporal and spatial separation vectors

    % Invert the LE position data such that the y-position increases as the cell
    % moves forward
        ypos = size(Image,1) - yPosSmooth; 
        
    % Prepare the conversion factors    
        % Period of measurements in seconds
            secondsPerFrame = millisecondsPerFrame/1000;
        % Period of measurements in microns
            micronsPerPixel = nanometersPerPixel/1000; 

    % Creates vectors for time points in frames and in seconds
        % Vector of time points in frames
            timeValsInFrames = 1:size(ypos,1);
        % Vector of time points in seconds
            timeValsInSeconds = secondsPerFrame*timeValsInFrames;

    % Creates vectors for spatial dimensions in pixels and in microns
        % Vector of spatial dimension in pixels
            xPosValsInPixTotal = 1:size(ypos,2);
        % Vector of spatial dimension in microns
            xPosValsInMicronsTotal = micronsPerPixel*xPosValsInPixTotal;

    % Crops the image to contain only the part of the leading edge that is
    % well-fit throughout the whole image
        xMinPix = max(leadingEdgeEndPoints(:,1)); % x value of leading edge to be calculated (in pixels)
        xMaxPix = min(leadingEdgeEndPoints(:,2)); % x value of leading edge to be calculated (in pixels)

    % Saves cropped versions of the position vectors as well
        xPosValsInPixCropped = xPosValsInPixTotal(1:((xMaxPix-xMinPix)+1));
        xPosValsInMicronsCropped = xPosValsInMicronsTotal(1:((xMaxPix-xMinPix)+1));
   
%% Calculate and display velocity of leading edge using outline

    % Calculate velocity with different coase-graining
        [myvelocities] = calculateVelocity(ypos); % produces pixel velocities

    % Determine proper coarse-graining to calculate velocity

        maxvel = [];
        minvel = [];
        meanvel = [];
        for k = 1:size(myvelocities,2)
            yvel2plot = myvelocities{1,k};
            maxvel(k) = max(max((yvel2plot(:,xMinPix:xMaxPix)))).*(micronsPerPixel/secondsPerFrame);
            minvel(k) = min(min((yvel2plot(:,xMinPix:xMaxPix)))).*(micronsPerPixel/secondsPerFrame);
            meanvel(k) = mean2(yvel2plot(:,xMinPix:xMaxPix)).*(micronsPerPixel/secondsPerFrame);
        end

        figure(101); %Plot calculated velocities vs coarse-graining of positions
        coarseTime = secondsPerFrame*(1:size(meanvel,2));
        a1 = plot(coarseTime,minvel,'b','LineWidth',5);
        hold on;
        a2 = plot(coarseTime,maxvel,'r','LineWidth',5);
        a3 = plot(coarseTime,meanvel,'g','LineWidth',5);
        hold off; 
        legend([a1, a2, a3],'Minimum velocity','Maximum velocity', 'Average velocity')
        xlabel('Amount of time used to calculate average velocity (s)')
        ylabel('Velocity (in nm/ms)')
        set(gca,'FontName','Arial','FontSize',28,'FontWeight','Bold');

        velDiscFact=5; %frame separation for velocity calculation

    % Display velocity as a kymograph
        yvel2plot = myvelocities{1,velDiscFact}.*(micronsPerPixel/secondsPerFrame); % Velocity in nm/ms
        yvel2plot = yvel2plot(:,xMinPix:xMaxPix);

        figure(102);
        imagesc(xPosValsInMicronsCropped,timeValsInSeconds,yvel2plot)
        set(gca, 'CLim', [-1, 1]);
        colorbar;
        colormap(jet);
        title('Velocity across the leading edge over time (nm/ms)')
        ylabel({'Time (seconds)'})
        xlabel('Distance across the leading edge (um)')
        set(gca,'FontName','Arial','FontSize',28,'FontWeight','Bold');

    % Display difference of velocity from average 

        yvel2avg = transpose(yvel2plot);
        meanVelVal = mean(yvel2avg);
        yvel2plotdiff = bsxfun(@minus,yvel2avg,meanVelVal);
        yvel2plotdiff = transpose(yvel2plotdiff);

        figure(103);
        imagesc(xPosValsInMicronsCropped,timeValsInSeconds,yvel2plotdiff)
        set(gca, 'CLim', [-1, 1]);
        colorbar;
        colormap(bluewhitered);
        title({'Velocity across the leading edge over time (nm/ms)','(Difference from the average)'})
        ylabel({'Time (seconds)'})
        xlabel('Distance across the leading edge (\mum)')
        set(gca,'FontName','Arial','FontSize',28,'FontWeight','Bold');

        beep

%% Calculate and display curvature along outline

    %Create outline values from ypos
        for n = 1:size(ypos,1)
            outline{1,n} = [ypos(n,:)' (1:size(ypos,2))'];
        end
    % Choose 1/2 segment size to calculate curvature
        halfSegForCurveFitting = 15;
    % Calculate curvature
        [curveVal] = calculateCurvature(outline,halfSegForCurveFitting);

    %Display curvature as kymograph
        figure(104);
        curveVal2plot = curveVal./micronsPerPixel;
        curveVal2plot = curveVal2plot(:,xMinPix:xMaxPix);
        imagesc(xPosValsInMicronsCropped,timeValsInSeconds,curveVal2plot)
        set(gca, 'CLim', [-1.0, 1.0]);
        colorbar;
        colormap(jet);
        title('Curvature across the leading edge over time (1/\mum)')
        ylabel({'Time (seconds)'})
        xlabel('Distance across the leading edge (\mum)')
        set(gca,'FontName','Arial','FontSize',28,'FontWeight','Bold');

