function conditionNames = extractConditionNamesForSims(datasetInfo)

%% This pulls out the condition names based on the file name
% (Run as part of plotSpatialFFTModeTimeAutocorrSummaryStats_Sims.m)

% This code was written by Rikki M. Garner as a companion to the manuscript
% titled "Leading edge stability in motile cells in an emergent property of
% branched actin network growth", and was last updated 2020/08/19.

%%

% Preallocate space to store the results
    % Calculate the number of unique conditions
        datasetInfo.numConditions = ...
            length(unique(datasetInfo.conditions2Plot));
    % Create the cell to store the condition names    
        conditionNames = cell([1 datasetInfo.numConditions]);

% Loop through each folder and pull out the mat file info
for conditionNum = datasetInfo.conditionNums
    
    % Find the first mat file in this condition
    
        % Find the index of the first dataset in this condition
            idx2Analyze = find(datasetInfo.conditionNums==conditionNum,1,'first');
        % Load the corresponding folder and parameter
            numFolder = datasetInfo.folders2Analyze(idx2Analyze);
            numParameter = datasetInfo.parameterVals2Analyze(idx2Analyze);

        % Pull out the folder path
            folderPath = datasetInfo.folderPaths{numFolder};    

        % Pull out the .mat file info for this folder
            matFileInfo = dir([folderPath '*.mat']); 

         % Load the global info variable of the first file to pull out the file prefix
            % Determine the file path
                matFilePath = [folderPath matFileInfo(1).name];
            % Load the relevant data
                load(matFilePath,'globalInfo')  
            % Determine the file path for the first replicate of the desired parameter
                matFilePath = [folderPath sprintf(globalInfo.fileNameForSprintf,globalInfo.dateNum,numParameter,1) '.mat'];

    % Load this file's parameter name and value

        % Load the relevant data
            load(matFilePath,'globalInfo')                

        % Take the file name and remove all unsavory characters
            lastUnderscoreIdx = find(globalInfo.fileNameForSprintf=='_',2,'last');
            lastUnderscoreIdx = lastUnderscoreIdx(1) + 1;
            lastPercentVal = find(globalInfo.fileNameForSprintf=='%',2,'last');
            lastPercentVal = lastPercentVal(1) - 1;
            dataNameForPlotting = globalInfo.fileNameForSprintf(lastUnderscoreIdx:lastPercentVal);
            dataNameForPlotting = sprintf([dataNameForPlotting ' = ' datasetInfo.num2sprintfConversion],...
                globalInfo.parameterVals(numParameter));

        % Record this name for plotting later
            conditionNames{conditionNum} = dataNameForPlotting;

    
    clearvars -except datasetInfo conditionNames conditionNum
 
end


                    
end