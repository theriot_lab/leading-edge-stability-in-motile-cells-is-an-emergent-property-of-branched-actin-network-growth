function [autocorrDataConcatenated] = prepSpatialFFTModeAutocorrResultsByCondition_Sims(autocorrDataConcatenated)

%% This function loops through all folders and files and concatenate the
% relevant data into a single structure
% (Run as part of plotSpatialFFTModeTimeAutocorrSummaryStats_Sims.m)

% This code was written by Rikki M. Garner as a companion to the manuscript
% titled "Leading edge stability in motile cells in an emergent property of
% branched actin network growth", and was last updated 2020/08/19.

%%

% Unpack the structure
    v2struct(autocorrDataConcatenated)

% Preallocate space to store the results
    % Calculate the number of unique conditions
        numConditions = length(unique(conditionNums));
    % Number of cells in each group
        numReplicatesByCondition = zeros(1,numConditions);
    % Spatial frequency
        spatialFrequencyInInverseNanometers = [];
    % Mean step between spatial frequency values
        diff_fVals = [];
    % Fitted exponential decay amplitudes    
        ampConst = [];
    % Fitted exponential decay rates    
        decayConst = [];
    % Condition number    
        conditionIdentity = [];
    % Replicate number     
        replicateIdentity = [];
    % The parameter value
        parameterVals = nan([1 numConditions]);

% Create the plot elements
    % Create an empty legend text cell
        legendText = conditionNames;
    % Choose the colormap 
    if numConditions == 1 
        CMByCondition = [0 0 0];
    elseif numConditions == 2
        CMByCondition = [0.9 0 0; 0 0 1];
    elseif numConditions == 3
        CMByCondition = [0 1 0; 0 0 1; 1 0 1];
    elseif numConditions == 4
        CMByCondition = [0 1 0; 0 1 1; 0 0 1; 1 0 1];
    elseif numConditions == 5
        CMByCondition = [0.9 0 0; 0 1 0; 0 1 1; 0 0 1; 1 0 1];
    elseif numConditions == 6        
        CMByCondition = [0.9 0 0; 1 0.5 0; 0 1 0; 0 1 1; 0 0 1; 1 0 1];
    elseif numConditions == 7       
        CMByCondition = [0.9 0 0; 1 0.5 0; 0 1 0; 0 1 1; 0 0 1; 1 0 1; 0 0 0];
    elseif numConditions > 7       
        CMByCondition = jet(numConditions);
    end            
            

    
% Save this information into a structure
    autocorrDataConcatenated = v2struct();
    clearvars -except autocorrDataConcatenated 

% Loop through each folder and pull out the mat file info
for numFolder = 1:length(autocorrDataConcatenated.folderPaths)
    
    % Pull out the folder path
        folderPath = autocorrDataConcatenated.folderPaths{numFolder};    
    
    % Pull out the .mat file info for this folder
        matFileInfo = dir([folderPath '*.mat']); 

    % Calculate the number of .mat files
        numMatFiles = length(matFileInfo);  
        
    % Loop through each matfile and save the data    
    for numMatFile = 1:numMatFiles
        
        try
        
        % Determine the file path
            matFilePath = [folderPath matFileInfo(numMatFile).name];
        
        % Determine whether this mat file should be processed
            % Load the parameter number
                load(matFilePath,'parameterValsNum')
            % Determine whether any folder/param combos match this combo    
                inFolderAndCorrectParam = ...
                    (numFolder == autocorrDataConcatenated.folders2Analyze)&...
                    (parameterValsNum == autocorrDataConcatenated.parameterVals2Analyze);
                
            if any(inFolderAndCorrectParam)
                
                % Load the relevant data
                    % Load the autocorr fit information
                        load(matFilePath,'spatialFFTModeTimeAutocorrFits')
                    % Load the parameter value
                        load(matFilePath,'globalInfo')
                        parameterVal = globalInfo.parameterVals(parameterValsNum);
                
                % Determine which condition this folder/parameter combo is
                    conditionNum = autocorrDataConcatenated.conditionNums(inFolderAndCorrectParam);

                % Save the results    
                    % Update the cell count
                        autocorrDataConcatenated.numReplicatesByCondition(conditionNum) = ...
                            autocorrDataConcatenated.numReplicatesByCondition(conditionNum)+1;
                    % Pull out the cell number
                        replicateNumber = autocorrDataConcatenated.numReplicatesByCondition(conditionNum);            
                    % Pull out the number of wavemodes in this dataset
                        numWavemodes = length(spatialFFTModeTimeAutocorrFits.spatialFrequencyInInverseNanometers);
                    % Save the data   
                        % Spatial frequency
                            autocorrDataConcatenated.spatialFrequencyInInverseNanometers = ...
                                [autocorrDataConcatenated.spatialFrequencyInInverseNanometers ...
                                spatialFFTModeTimeAutocorrFits.spatialFrequencyInInverseNanometers];
                        % Mean step between spatial frequency values
                            meanDiffspatialFrequencyInInverseNanometers = mean(diff(spatialFFTModeTimeAutocorrFits.spatialFrequencyInInverseNanometers));
                            autocorrDataConcatenated.diff_fVals = [autocorrDataConcatenated.diff_fVals ...
                                meanDiffspatialFrequencyInInverseNanometers.*ones([1 numWavemodes])];
                        % Fitted exponential decay amplitudes    
                            autocorrDataConcatenated.ampConst = [autocorrDataConcatenated.ampConst ...
                                spatialFFTModeTimeAutocorrFits.expAmpConst];
                        % Fitted exponential decay rates    
                            autocorrDataConcatenated.decayConst = [autocorrDataConcatenated.decayConst ...
                                spatialFFTModeTimeAutocorrFits.expDecayConst];
                        % Condition number    
                            autocorrDataConcatenated.conditionIdentity = [autocorrDataConcatenated.conditionIdentity ...
                                conditionNum.*ones([1 numWavemodes])];
                        % Replicate number (cell number within condition)    
                            autocorrDataConcatenated.replicateIdentity = [autocorrDataConcatenated.replicateIdentity ...
                                replicateNumber.*ones([1 numWavemodes])];
                        % Record the value of the parameter    
                            autocorrDataConcatenated.parameterVals(conditionNum) = parameterVal;
                    
            else
            end


        clearvars -except autocorrDataConcatenated numFolder folderPath matFileInfo numMatFiles
        catch ME1
            'No autocorr fit data available'
            matFilePath
           % ginput();
        end
        
    end
    
    clearvars -except autocorrDataConcatenated numFolder
 
end


end