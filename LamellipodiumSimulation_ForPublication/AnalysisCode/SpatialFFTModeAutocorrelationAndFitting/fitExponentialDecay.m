function [fitCoefficients, numTimePoints2Fit, MEAutocorrFit] = fitExponentialDecay(xVals,yVals)

%% This function takes in two vectors (xVals and yVals) and fits them to the 
% function yVals = A*exp(-r*xVals). The region fitted corresponds to the
% first set of points where yVals > yVals(1)/(e/2). For these set of points,
% a line is fit to (xVals,log(yVals)).
% (Run as part of spatialFFTModeTimeAutocorrelationAnalysis.m)

% Input: xVals, yVals = Vectors of equal length containing real-valued
% entries 
% Output: fitCoefficients = A [1 x 2] vector = [A r] containing the best fit
% values for A and r
% numTimePoints2Fit = the number of time points that were fit
% MEAutocorrFit = an error message if the fitting failed for some reason
%
% This code was written by Rikki M. Garner as a companion to the manuscript
% titled "Leading edge stability in motile cells in an emergent property of
% branched actin network growth", and was last updated 2020/08/19.

%%

% Preallocate an empty error message
    MEAutocorrFit=[];

% Fit an initial exponential decay to the first few data points
try

    % Determine the number of timepoints to fit the exponential
        % Find the first time index falling off below a factor of (e/2)
        % (and use all of the timepoints if it never falls that far)
            firstTimeIdxBelowE = min(length(yVals),find(yVals<(yVals(1)/(exp(1)/2)),1,'first'));
        % If this number exists, set it to the timescale to fit, otherwise 
        % choose the first point below the background noise window      
            if ~isempty(firstTimeIdxBelowE)
                numTimePoints2Fit = firstTimeIdxBelowE;
            else
                % Estimate the median and std of the background noise 
                % (the second half of time points)
                    backgndPredict = nanmedian(yVals(round(length(yVals)/2):end));
                    backgndStd = nanstd(yVals(round(length(yVals)/2):end));
                % Find the first time index where the value
                % drops below the noisy background window (or becomes
                % negative)
                    numTimePoints2Fit = find(yVals<max(0,backgndPredict+backgndStd),1,'first');
            end
            if isempty(numTimePoints2Fit)
                numTimePoints2Fit=5;
            end
        % Make sure you're fitting at least 5 points
            numTimePoints2Fit = max(5,numTimePoints2Fit);

    % Fit an initial exponential decay in this region
        % Fit a line to x vs log(y)
            p = polyfit(xVals(1:numTimePoints2Fit),log(yVals(1:numTimePoints2Fit)),1);
        % Convert the linear fit values to the decay constant and amplitude    
            expDecayConst = -p(1);
            expAmpConst = exp(p(2));
        % Save the fit coefficients     
            if expDecayConst>=0
                fitCoefficients = [expAmpConst expDecayConst];
            else
                MEAutocorrFit = 'Decay constant was less than zero, fitting failed';
                fitCoefficients = nan([1 2]);
            end            
 
catch MEAutocorrFit
	fitCoefficients = nan(1,2);    
end

end