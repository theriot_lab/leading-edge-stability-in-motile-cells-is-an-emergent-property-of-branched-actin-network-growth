%% This script performs autocorrelation analysis on all files in all folders specified. 

% This code was written by Rikki M. Garner as a companion to the manuscript
% titled "Leading edge stability in motile cells in an emergent property of
% branched actin network growth", and was last updated 2020/08/19.


% Clear the system
    clear all;
    close all;
    
% Choose the datasets to analyze  
%     % Example for a defined set of datasets
%         % Membrane driven by thermal fluctuations
%             folderPaths = {['/Volumes/homes/Rikki/Simulations/BRActinGrowth_Spread_ScanParameters/',...
%                 'Final/SimulationResults/Results20200411_1/']};
%         % Branching angle with variability
%             folderPaths = {['/Volumes/homes/Rikki/Simulations/BRActinGrowth_Spread_ScanParameters/'...
%                   'Final/SimulationResults/Results20200403_1/']}; 
%             folderPaths = {['/Volumes/homes/Rikki/Simulations/BRActinGrowth_Spread_ScanParameters/'...
%                   'Final/SimulationResults/Results20200408_1/']};
%         % Branching angle no variability
%             folderPaths = {['/Volumes/homes/Rikki/Simulations/BRActinGrowth_Spread_ScanParameters/'...
%                   'Final/SimulationResults/Results20200331/']}; 
%             folderPaths = {['/Volumes/homes/Rikki/Simulations/BRActinGrowth_Spread_ScanParameters/'...
%                   'Final/SimulationResults/Results20200409_1/']};
%         % Leading edge simulation with hydrodynamics that stopped early        
%             folderPaths = ...
%                 {['/Users/Rikki/Documents/Theory/FromAndy/RikkisPackaging/MolecularMechanisms'...
%                 '/BRActinGrowth/BRActinGrowth_Spread_ScanParameters/SimulationResults/'...
%                 'Results20200608_7/']};  
        % Number of membrane beads
            folderPaths = {['/Volumes/homes/Rikki/Simulations/BRActinGrowth_Spread_ScanParameters/'...
                  'Final/SimulationResults/Results20200313/']};
            folderPaths = {['/Users/Rikki/Documents/Theory/FromAndy/RikkisPackaging/'...
                  'MolecularMechanisms/BRActinGrowth/BRActinGrowth_Spread_ScanParameters/'...
                  'SimulationResults/Results20200702/']}; 
              
        % Calculate the number of folders in this set
            numFolders  = length(folderPaths);
            
    % Example for a folder of directories
        % Choose the parent folder containing all of the datasets to analyze
            superFolderPath = ['/Volumes/homes/Rikki/Simulations/'...
                'BRActinGrowth_Spread_ScanParameters/Final/SimulationResults/'];
        % Create a list of all of the folders to loop through
            % Pull out information about the directory's contents
                superFolderPathInfo = dir(superFolderPath);
            % Only the subdirectory info
                superFolderPathInfo = superFolderPathInfo([superFolderPathInfo.isdir]);
            % Remove the boiler plate stuff    
                superFolderPathInfo = superFolderPathInfo(~ismember({superFolderPathInfo.name},{'.','..','.DS_Store'}));
            % Pull out the number of subdirectories
                numFolders = length(superFolderPathInfo);
            % Create a cell to store the folder paths   
                folderPaths = cell([numFolders 1]);
            % Loop through each folder and add its path to the list
                for folderNum = 1:numFolders
                    folderPaths{folderNum} = [superFolderPath superFolderPathInfo(folderNum).name '/'];
                end
                
 % Loop through each folder and run the autocorrelation analysis pipeline
 for folderNum = 24:numFolders
     
     % Load the folderPath for this folder
        folderPath = folderPaths{folderNum};
     
	% Load the information about the mat files contained in the folder
        matFileInfo = dir([folderPath '*.mat']);
    
    % Calculate the number of files
        numMatFiles = length(matFileInfo);
        
     for matFileNum = 1:numMatFiles  
         
         try
         
        % Print which dataset we're on
            sprintf('Analyzing file %i of %i in folder %i of %i',matFileNum,numMatFiles,folderNum,numFolders)
         
        % Pull out the file name
            matFilePath = [folderPath matFileInfo(matFileNum).name];

        % Load the error message
            load(matFilePath,'ME')
            
        % If the ME variable exists, then the simulation is complete
        % If ME exists but is not empty, then the simulation failed for
        % some reason
        if exist('ME')
            if ~isempty(ME)
                'Simulation failed'   
                
                ginput();
            else

                % Prepare the data for autocotorrelation analysis   
                    % Load the variables
                        load(matFilePath,'numTimeStepsMem2Record')
                        load(matFilePath,'numTimeStepsMem2Skip')
                        load(matFilePath,'dtRelaxMemInMilliseconds')
                        load(matFilePath,'numMemSegments')
                        load(matFilePath,'xSepMemSegmentsInNanometers')
                        load(matFilePath,'leadingEdgeLengthInNanometers')
                        load(matFilePath,'yPosMemInNanometersRecord')
                    % Rename the variables to be more generic 
                        numTimePoints = numTimeStepsMem2Record;
                        yPosMemInNanometers = yPosMemInNanometersRecord;
                        nanometersPerXValInc = xSepMemSegmentsInNanometers;
                    % Calculate the number of milliseconds in between membrane position
                    % measurements
                        millisecondsPerFrame = numTimeStepsMem2Skip.*dtRelaxMemInMilliseconds; 
                    % Choose the number of timesteps to skip (none)
                        numTimePoints2SkipAutocorr = 1;
                    % Choose the timepoint to start the analysis (after reaching steady state)   
                        timePoint2StartAutocorr = round(10000/millisecondsPerFrame);
                    % Choose whether to plot the result
                        doPlot=0;

                % Run the autocorrelation analysis        
                    spatialFFTModeTimeAutocorrelationAnalysis   

                % Save the input data to a structure   
                    spatialFFTModeData = struct();
                    spatialFFTModeData.spatialFrequencyInInverseNanometers = spatialFrequencyInInverseNanometers;
                    spatialFFTModeData.numTimePoints2SkipAutocorr = numTimePoints2SkipAutocorr;
                    spatialFFTModeData.timePoint2StartAutocorr = timePoint2StartAutocorr;
                    spatialFFTModeData.FFTYPosMemInNanometers = FFTYPosMemInNanometers;
                % Save the autocorrelations to a structure    
                    spatialFFTModeTimeAutocorrData = struct();
                    spatialFFTModeTimeAutocorrData.rVals = rVals;
                    spatialFFTModeTimeAutocorrData.lagPosInMilliseconds = lagPosInMilliseconds;
                    spatialFFTModeTimeAutocorrData.numTimePoints2SkipAutocorr = numTimePoints2SkipAutocorr;
                    spatialFFTModeTimeAutocorrData.timePoint2StartAutocorr = timePoint2StartAutocorr;
                % Save the fit results to a structure     
                    spatialFFTModeTimeAutocorrFits = struct();
                    spatialFFTModeTimeAutocorrFits.spatialFrequencyInInverseNanometers = spatialFrequencyInInverseNanometers;
                    spatialFFTModeTimeAutocorrFits.expDecayConst = expDecayConst;
                    spatialFFTModeTimeAutocorrFits.expAmpConst = expAmpConst;   
                    spatialFFTModeTimeAutocorrFits.numTimeLags2Fit = numTimeLags2Fit;    
                    spatialFFTModeTimeAutocorrFits.MEAutocorrFit = MEAutocorrFit;  
                
                % Pack the data into a structure
                    outputVars = struct;
                    outputVars.spatialFFTModeData = spatialFFTModeData;
                    outputVars.spatialFFTModeTimeAutocorrData = spatialFFTModeTimeAutocorrData;
                    outputVars.spatialFFTModeTimeAutocorrFits = spatialFFTModeTimeAutocorrFits;
                    
                % Save the data
                    save(matFilePath,'-append','-struct','outputVars');
                    
                % Clear the system for this loop
                    clearvars -except folderPaths numFolders folderNum folderPath matFileInfo matFileNum numMatFiles
            end
        else
            'Simulation not complete'    
            ginput();
        end  
        
        catch errorMessage
            errorMessage
            'Autocorrelation analysis failed'    
            ginput();
        end
         
     end
     
    
 end
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 