function [autocorrelation] = calculateAutocorrelationNonOverlapping(data)

%% This function performs autocorrelation analysis over the first dimension
% of the input matrix.
% (Run as part of spatialFFTModeTimeAutocorrelationAnalysis.m)

% Input: data = A [numTimePoint x numObjects] matrix
% containing the objects' property values (can be real or complex) over time
% Output: autocorrelation = A [numLags x numObjects] matrix
% containing the time-averaged autocorrelation value for each object and
% time lag

% This code was written by Rikki M. Garner as a companion to the manuscript
% titled "Leading edge stability in motile cells in an emergent property of
% branched actin network growth", and was last updated 2020/08/19.

%%

% Extract the dimensions of the matrix
    % Pull out the number of timepoints
        numTimePoints = size(data,1);
    % Pull out the number of tracks
        numObjects = size(data,2);

% Choose how many time offsets for which to calculate the autocorrelation,
% and create a matrix to store them in
    % Choose the number of time offsets to try 
    % (all that have >=4 non-overlapping measurements)
        numTimeOffsets = round(numTimePoints/4);        
    % Create a matrix to store the autocorrelation values
        autocorrelation=nan([numObjects, numTimeOffsets]);
    
% Loop through each time offset and calculate the autocorrelation
for timeOffsetNum = 0:(numTimeOffsets-1)
    % Pull out the timepoints to use
        timePoints1 = 1:max(1,timeOffsetNum):(numTimePoints-timeOffsetNum);
        timePoints2 = (timeOffsetNum+1):max(1,timeOffsetNum):numTimePoints;
    % Calculate the time-averaged autocorrelation value for each object
        autocorrelation(:,timeOffsetNum+1) = ...
            nanmean(data(timePoints2,:).*conj(data(timePoints1,:)),1);
end
