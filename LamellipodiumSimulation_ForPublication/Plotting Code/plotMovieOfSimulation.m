%% This script makes a movie of a simulation

% This code was written by Rikki M. Garner as a companion to the manuscript
% titled "Leading edge stability in motile cells in an emergent property of
% branched actin network growth", and was last updated 2020/08/19.

%% Clear the system and load in the relevant data

    % Clear the system
        clear all;
        close all;    

    % Choose the folder contining the simulation results
        folderName = ...
            ['/Users/Rikki/Documents/LamellipodiumSimulation_ForPublication/'...
            'SimulationResults/Results20200820_1/'];
    % Choose which mat file to plot    
        numMatFile2Plot = 1;     

    % Pull out the .mat file info
        matFileInfo = dir([folderName '*.mat']); 

    % Load in the data
        load([folderName matFileInfo(numMatFile2Plot).name])
    
%% Choose how to save the video

    % Choose the location to save the video
        destinationFolderName = matFileInfo(numMatFile2Plot).name;
        destinationFolderName = [destinationFolderName(1:(find(destinationFolderName=='.',1,'last')-1))];
        destinationFolderPath = ['/Users/Rikki/Documents/'...
            'LamellipodiumSimulation_ForPublication/SimulationResults/Movies/' ...
            destinationFolderName '/'];
    % Delete the folder if it already exists
        if exist(destinationFolderPath,'dir')
            rmdir(destinationFolderPath, 's')
        end
    % Make the folder 
        mkdir(destinationFolderPath);
    % Choose the tif file prefix
        filePathPrefix  = [destinationFolderPath destinationFolderName '_0000%i' '.tif'];
    
%% Initialize the figure

    % Make the figure
        f1 = figure(10);
        f1.Units = 'normalized';
        f1.OuterPosition = [0 0 1 1];        
    
    % Choose the xlims
        % Choose the number of beads to plot
            numBeads2Plot = 20;
        % Choose the colormap
            CM = jet(numBeads2Plot);
        % Pick out the x-psoition of the rightmost bead
            xMax = xPosMemInNanometers(numBeads2Plot);            
            
    % Calculate the time each membrane recording occured        
        times2RecordInMillisecondsMem = (idx2RecordMem-1)*dtRelaxMemInMilliseconds;
        
    % Choose the time points to display       
        timePoints2PlotFil = 1:round(size(filamentBarbedEndXValsRecord,1)/50):....
            size(filamentBarbedEndXValsRecord,1);
        
%% Loop through the time points, plot and record the frame

for numTimePointRecordFil = timePoints2PlotFil

    % Pull out the nearest saved membrane timepoint
        % (The filament properties were saved less often because they 
        % are more data intensive than the membrane properties)
        numTimePointRecordMem = round((numTimePointRecordFil-1)*...
            numTimeStepsFilament2Skip/numTimeStepsMem2Skip)+1;

    % Plot the filaments
        % Pull out the filament properties
            % Pull out the filament barbed end x-vals
                filamentBarbedEndXVals = filamentBarbedEndXValsRecord(numTimePointRecordFil,:);
            % Determine the number of filaments within the x-limits we chose to plot    
                numFilaments = sum(filamentBarbedEndXVals<=xMax);
            % Pull out the properties of these filaments    
                isCapped4Calc = filamentIsCappedRecord(numTimePointRecordFil,1:numFilaments);
                filamentBarbedEndYVals = filamentBarbedEndYValsRecord(numTimePointRecordFil,1:numFilaments);
                filamentBarbedEndXVals = filamentBarbedEndXValsRecord(numTimePointRecordFil,1:numFilaments);
                thetaVals4Calc = thetaValsRecord(numTimePointRecordFil,1:numFilaments);
                lf4Calc = lfRecord(numTimePointRecordFil,1:numFilaments);
            % Calculate the associated pointed end positions of the filaments   
                filamentPointedEndYVals = filamentBarbedEndYVals - lf4Calc.*cos(thetaVals4Calc);
                filamentPointedEndXVals = filamentBarbedEndXVals - lf4Calc.*sin(thetaVals4Calc); 
            % Concatenate the barbed and pointed end positions for each dimension   
                filamentYVals = [filamentPointedEndYVals(:) , filamentBarbedEndYVals(:)]';
                filamentXVals = [filamentPointedEndXVals(:) , filamentBarbedEndXVals(:)]'; 
            % Calculate which membrane segments each filament is associated
            % with
                beadIdx4Calc = ceil(filamentBarbedEndXVals/xSepMemSegmentsInNanometers);
        % Plot the filament barbed end positions as gray circles    
            plot(filamentBarbedEndXVals*10^(-3),filamentBarbedEndYVals*10^(-3),....
                'o','MarkerFaceColor',[0.5 0.5 0.5],'MarkerEdgeColor','none','MarkerSize',5)
            hold on;
        % Plot the capped filament barbed ends as black circles  
            if any(isCapped4Calc)        
                filamentBarbedEndYValsCapped = filamentBarbedEndYVals(isCapped4Calc);
                filamentBarbedEndXValsCapped = filamentBarbedEndXVals(isCapped4Calc);
                plot(filamentBarbedEndXValsCapped*10^(-3),filamentBarbedEndYValsCapped*10^(-3),...
                    'o','MarkerFaceColor',[0 0 0],'MarkerEdgeColor','none','MarkerSize',5)
            end
        % Plot the equillibrium filament positio,n, colored by which membrane 
        % segment the filaent ilies uner   
            for numFil = 1:length(filamentPointedEndYVals)
                % Plot the filament
                    plot(filamentXVals(:,numFil)*10^(-3), filamentYVals(:,numFil)*10^(-3),...
                        '-','color',CM(beadIdx4Calc(numFil),:))
                % If the filament barbed end lies beyond the membrane,
                % overlay a light gray dot
                    if any(filamentYVals(:,numFil)>yPosMemInNanometersRecord(numTimePointRecordMem,beadIdx4Calc(numFil)))
                        plot(filamentBarbedEndXVals(numFil)*10^(-3),...
                            filamentBarbedEndYVals(numFil)*10^(-3),...
                            'o','MarkerFaceColor',[0.9 0.9 0.9],'MarkerEdgeColor','none','MarkerSize',5)
                    end
            end

    % Plot the membrane
        % Plot the membrane position
            plot(([(xPosMemInNanometers-(xSepMemSegmentsInNanometers/2)); ...
                (xPosMemInNanometers+(xSepMemSegmentsInNanometers/2))])*10^(-3),...
                [yPosMemInNanometersRecord(numTimePointRecordMem,:); ...
                yPosMemInNanometersRecord(numTimePointRecordMem,:)]*10^(-3),...
                'k-','LineWidth',5)
        % Plot the edges between the membrane segments
            plot(([(xPosMemInNanometers(1:(end-1))+(xSepMemSegmentsInNanometers/2)); ...
                (xPosMemInNanometers(1:(end-1))+(xSepMemSegmentsInNanometers/2))])*10^(-3),...
                [yPosMemInNanometersRecord(numTimePointRecordMem,2:end);...
                yPosMemInNanometersRecord(numTimePointRecordMem,1:(end-1))]*10^(-3),...
                'k-','LineWidth',5)


    % Clean up and annotate the figure
        % Set the axes lanegth to be propeortional to the true dimensions 
            axis equal
        % Update the x-limits
            xLimMax = xMax*10^(-3);
            xlim([0 xLimMax])
        % Update the y-limits
            yLimMin = mean(yPosMemInNanometersRecord(numTimePointRecordMem,...
                1:numBeads2Plot)*10^(-3))-(xMax*10^(-3)/2);
            yLimMax = mean(yPosMemInNanometersRecord(numTimePointRecordMem,...
                1:numBeads2Plot)*10^(-3))+(xMax*10^(-3)/2);
            yLimMean = mean(yPosMemInNanometersRecord(numTimePointRecordMem,...
                1:numBeads2Plot)*10^(-3));
            ylim([yLimMin yLimMax])
        % Label the x- and y-axis    
            xlabel('X-position (�m)')
            ylabel('Y-position (�m)')
        % Add the timestamp
            text(xLimMax*0.8,yLimMean + ((yLimMax-yLimMean)*0.9),...
                sprintf('t = %2.3f s',times2RecordInMillisecondsMem(numTimePointRecordMem)*10^(-3)) ...
                ,'FontName','Helvetica','FontSize',30)
        % Set the figure font characteristics    
            set(gca,'FontName','Helvetica','FontSize',30);
            hold off;
            drawnow

    % Write the image
       % Record the figure as an image
            actinFig = getframe(gcf);
            [actinIm Map] = frame2im(actinFig);
        % Update the filename
            destinationTrack = sprintf(filePathPrefix,numTimePointRecordFil);
        % Write the image file
            imwrite(actinIm, destinationTrack)

end
    

          