%% This script plots the time course of key measurable parameters as the 
% simulation reaches steady state

% This code was written by Rikki M. Garner as a companion to the manuscript
% titled "Leading edge stability in motile cells in an emergent property of
% branched actin network growth", and was last updated 2020/08/19.

%%

% Clear the system
    clear all;
    close all;    


% paramCounter = 0;
% 
% while ~isempty(paramCounter)
%     
%     % Update the param counter
%         paramCounter = paramCounter + 1;
        
for paramCounter=1
    
    if paramCounter==1      
        % Choose the folder containing the files to plot
            folderName = ...
                ['/Users/Rikki/Documents/LamellipodiumSimulation_ForPublication/'...
                'SimulationResults/Results20200820_1/'];
        % Choose which files to plot (generally one of each parameter value)
            matFiles2Plot = [1,2,3]; 
    end
        
% Chose whether to plot the movie
    plotMovie=0;
    
% Pull out the .mat file info
    matFileInfo = dir([folderName '*.mat']);
    
    numMatFiles = length(matFileInfo);
    
    

    numMatFiles2Plot = length(matFiles2Plot);
    if numMatFiles2Plot==1 
        CMLinePlots = [0 0 0];
    elseif numMatFiles2Plot == 2
        CMLinePlots = [1 0 0; 0 0 1];
    elseif numMatFiles2Plot == 3
        CMLinePlots = [0 1 0; 0 0 1; 1 0 1];
    elseif numMatFiles2Plot == 4
        CMLinePlots = [0 1 0; 0 1 1; 0 0 1; 1 0 1];
    elseif numMatFiles2Plot == 5
        CMLinePlots = [1 0 0; 0 1 0; 0 1 1; 0 0 1; 1 0 1];
    elseif numMatFiles2Plot == 6        
        CMLinePlots = [1 0 0; 1 0.5 0; 0 1 0; 0 1 1; 0 0 1; 1 0 1];
    elseif numMatFiles2Plot == 7       
        CMLinePlots = [1 0 0; 1 0.5 0; 0 1 0; 0 1 1; 0 0 1; 1 0 1; 0 0 0];
    elseif numMatFiles2Plot > 7       
        CMLinePlots = jet(numMatFiles2Plot);
    end
    
    legendText = {};
    legendTextHist = {};
    

% Loop through each file and plt the data
 for numMatFile = 1:numMatFiles2Plot
        
	% Determine which mat file to open
        numMatFile2Plot = matFiles2Plot(numMatFile);
     
    % Update the legend text for thie file
        % Load in the simulation info (parameter#, replicate#)
            load([folderName matFileInfo(numMatFile2Plot).name],'globalInfo')
            load([folderName matFileInfo(numMatFile2Plot).name],'parameterValsNum')
            load([folderName matFileInfo(numMatFile2Plot).name],'replicateNum')
        % Take the file name and remove all unsavory characters
            lastUnderscoreIdx = find(globalInfo.fileNameForSprintf=='_',2,'last');
            lastUnderscoreIdx = lastUnderscoreIdx(1) + 1;
            lastPercentVal = find(globalInfo.fileNameForSprintf=='%',2,'last');
            lastPercentVal = lastPercentVal(1) - 1;
            dataNameForPlotting = globalInfo.fileNameForSprintf(lastUnderscoreIdx:lastPercentVal);
            dataNameForPlotting  = strrep(dataNameForPlotting ,'_',' ');
            dataNameForPlotting  = strrep(dataNameForPlotting ,'Migrating ','');
            dataNameForPlotting  = strrep(dataNameForPlotting ,'Pr','%');
            dataNameForPlottingPrefix  = strrep(dataNameForPlotting ,'Pt','.');
            dataNameForPlotting = sprintf([dataNameForPlottingPrefix ' = %2.2d'],globalInfo.parameterVals(parameterValsNum));
%         % Record this name for plotting later
%             globalInfo2.legendText{parameterValsNum} = dataNameForPlotting;
        % Record the legend text
            legendText = [legendText dataNameForPlotting];
            legendText = [legendText {''}];
            legendTextHist = [legendTextHist dataNameForPlotting];
     


    % Load in the data
        % Create the matfile to load the large datasets 
            % Pull out the file name
                filePathOutOnLocal = [folderName matFileInfo(numMatFile2Plot).name];
            % Create the mat file object    
                matFileObj = matfile(filePathOutOnLocal,'Writable',true);

        % Print the current file
            load([folderName matFileInfo(numMatFile2Plot).name],'ME')
            
        % If the ME variable exists, then the simulation is complete
        % If ME exists but is not empty, then the simulation failed for
        % some reason
            if exist('ME')
                if ~isempty(ME)
                    'Simulation failed'
                    % Load the variables
                        % Choose which timepoints to plot
                            % Load in the timepoints
                                load([folderName matFileInfo(numMatFile2Plot).name],'dtRelaxMemInMilliseconds')
                                load([folderName matFileInfo(numMatFile2Plot).name],'fracOfTotalTime2Start')
                                load([folderName matFileInfo(numMatFile2Plot).name],'numTimeStepsMem2Skip')
                                load([folderName matFileInfo(numMatFile2Plot).name],'numTimeStepsMem2Record')
                                load([folderName matFileInfo(numMatFile2Plot).name],'times2RecordInMillisecondsMem')
                                load([folderName matFileInfo(numMatFile2Plot).name],'numTimeStepsFilament2Record')
                                load([folderName matFileInfo(numMatFile2Plot).name],'times2RecordInMillisecondsFilament')
                                load([folderName matFileInfo(numMatFile2Plot).name],'numTimeStepsFilament2Skip')
                                load([folderName matFileInfo(numMatFile2Plot).name],'leadingEdgeLengthInNanometers')
                                load([folderName matFileInfo(numMatFile2Plot).name],'branchAngle')
                                load([folderName matFileInfo(numMatFile2Plot).name],'branchAngleDeviation')
                                load([folderName matFileInfo(numMatFile2Plot).name],'maxNumFilaments2Sim') 
                                timeIdx2Analyze = floor(length(times2RecordInMillisecondsMem).*fracOfTotalTime2Start):length(times2RecordInMillisecondsMem);
                                timeValsInMilliseconds = times2RecordInMillisecondsMem(1:length(timeIdx2Analyze));
                            % Choose the time points to plot
                                % Membrane time points
                                    fracOfTotalTime2StartPlotMem = 1/numTimeStepsMem2Record;
                                    timeIdx2AnalyzeMem = floor(numTimeStepsMem2Record.*fracOfTotalTime2StartPlotMem):numTimeStepsMem2Record;
                                    timeValsInMillisecondsMem = times2RecordInMillisecondsMem(1:length(timeIdx2AnalyzeMem));
                                    numTimePoint2Skip2PlotMem = floor(50/(numTimeStepsMem2Skip*dtRelaxMemInMilliseconds));
                                    timeIdx2DisplayMem = 1:numTimePoint2Skip2PlotMem:length(timeValsInMillisecondsMem);
                                    timeIdx2PlotMem = timeIdx2AnalyzeMem(timeIdx2DisplayMem);
                                % Filament time points
                                    fracOfTotalTime2StartPlotFil = 1/numTimeStepsFilament2Record;
                                    timeIdx2AnalyzeFil = round(numTimeStepsFilament2Record.*fracOfTotalTime2StartPlotFil):numTimeStepsFilament2Record;
                                    timeValsInMillisecondsFil = times2RecordInMillisecondsFilament(1:length(timeIdx2AnalyzeFil));
                                    numTimePoint2Skip2PlotFil = max(1,floor(50/(numTimeStepsFilament2Skip*dtRelaxMemInMilliseconds)));
                                    timeIdx2DisplayFil = 1:numTimePoint2Skip2PlotFil:length(timeValsInMillisecondsFil);
                                    timeIdx2PlotFil = timeIdx2AnalyzeFil(timeIdx2DisplayFil);
                            % Load in the membrane x-position data
                                load([folderName matFileInfo(numMatFile2Plot).name],'xPosMemInNanometers')
                                xPosMemInNanometers = xPosMemInNanometers;
                                load([folderName matFileInfo(numMatFile2Plot).name],'xSepMemSegmentsInNanometers')
                            % Load the y-position data
                                yPosMemInNanometers2Plot = matFileObj.yPosMemInNanometersRecord(timeIdx2PlotMem,:);
                                numFilaments2Plot = matFileObj.numFilamentsRecord(timeIdx2PlotMem,:);
                                numCappedFilaments2Plot = matFileObj.numCappedFilamentsRecord(timeIdx2PlotMem,:);
                                lf2Plot = matFileObj.lfRecord(timeIdx2PlotFil,:);
                                isCapped2Plot = matFileObj.filamentIsCappedRecord(timeIdx2PlotFil,:);
                                thetaVals2Plot = matFileObj.thetaValsRecord(timeIdx2PlotFil,:);  
                            % Create the ypos diff matrix
                                yPosMemInNanometers2Plot = transpose(yPosMemInNanometers2Plot);
                                meanYPos = mean(yPosMemInNanometers2Plot);
                                yPosMemInNanometersDiff = bsxfun(@minus,yPosMemInNanometers2Plot,meanYPos);
                                yPosValsInNanometersCroppedDiff = transpose(yPosMemInNanometersDiff);
                                yPosMemInNanometers2Plot = transpose(yPosMemInNanometers2Plot);

                            % Rename variables
                                xPosValsInMicronsCropped = xPosMemInNanometers/10^3;
                                timeValsInSeconds2PlotMem = timeValsInMillisecondsMem(timeIdx2DisplayMem)*10^(-3);
                                timeValsInSeconds2PlotFilament = timeValsInMillisecondsFil(timeIdx2DisplayFil)*10^(-3);
                                yPosDiffValsInNanometersCropped = yPosValsInNanometersCroppedDiff;
                                nanometersPerPixel = xSepMemSegmentsInNanometers;     
                else
                    % Load the variables
                        % Choose which timepoints to plot
                            % Load in the timepoints
                                load([folderName matFileInfo(numMatFile2Plot).name],'dtRelaxMemInMilliseconds')
                                load([folderName matFileInfo(numMatFile2Plot).name],'fracOfTotalTime2Start')
                                load([folderName matFileInfo(numMatFile2Plot).name],'numTimeStepsMem2Skip')
                                load([folderName matFileInfo(numMatFile2Plot).name],'numTimeStepsMem2Record')
                                load([folderName matFileInfo(numMatFile2Plot).name],'times2RecordInMillisecondsMem')
                                load([folderName matFileInfo(numMatFile2Plot).name],'numTimeStepsFilament2Record')
                                load([folderName matFileInfo(numMatFile2Plot).name],'times2RecordInMillisecondsFilament')
                                load([folderName matFileInfo(numMatFile2Plot).name],'numTimeStepsFilament2Skip')
                                load([folderName matFileInfo(numMatFile2Plot).name],'leadingEdgeLengthInNanometers')
                                load([folderName matFileInfo(numMatFile2Plot).name],'branchAngle')
                                load([folderName matFileInfo(numMatFile2Plot).name],'branchAngleDeviation')
                                load([folderName matFileInfo(numMatFile2Plot).name],'maxNumFilaments2Sim') 
                                load([folderName matFileInfo(numMatFile2Plot).name],'spatialFFTModeTimeAutocorrFits')       
                                
                                timeIdx2Analyze = floor(length(times2RecordInMillisecondsMem).*fracOfTotalTime2Start):length(times2RecordInMillisecondsMem);
                                timeValsInMilliseconds = times2RecordInMillisecondsMem(1:length(timeIdx2Analyze));
                            % Choose the time points to plot
                                % Membrane time points
                                    fracOfTotalTime2StartPlotMem = 1/(numTimeStepsMem2Record-1);
                                    timeIdx2AnalyzeMem = floor(numTimeStepsMem2Record.*fracOfTotalTime2StartPlotMem):numTimeStepsMem2Record;
                                    timeValsInMillisecondsMem = times2RecordInMillisecondsMem(1:length(timeIdx2AnalyzeMem));
                                    numTimePoint2Skip2PlotMem = floor(50/(numTimeStepsMem2Skip*dtRelaxMemInMilliseconds));
                                    timeIdx2DisplayMem = 1:numTimePoint2Skip2PlotMem:length(timeValsInMillisecondsMem);
                                    timeIdx2PlotMem = timeIdx2AnalyzeMem(timeIdx2DisplayMem);
                                % Filament time points
                                    fracOfTotalTime2StartPlotFil = 1/numTimeStepsFilament2Record;
                                    timeIdx2AnalyzeFil = round(numTimeStepsFilament2Record.*fracOfTotalTime2StartPlotFil):numTimeStepsFilament2Record;
                                    timeValsInMillisecondsFil = times2RecordInMillisecondsFilament(1:length(timeIdx2AnalyzeFil));
                                    numTimePoint2Skip2PlotFil = max(1,floor(50/(numTimeStepsFilament2Skip*dtRelaxMemInMilliseconds)));
                                    timeIdx2DisplayFil = 1:numTimePoint2Skip2PlotFil:length(timeValsInMillisecondsFil);
                                    timeIdx2PlotFil = timeIdx2AnalyzeFil(timeIdx2DisplayFil);
                            % Load in the membrane x-position data
                                load([folderName matFileInfo(numMatFile2Plot).name],'xPosMemInNanometers')
                                load([folderName matFileInfo(numMatFile2Plot).name],'xSepMemSegmentsInNanometers')
                            % Load the y-position data
                                yPosMemInNanometers2Plot = matFileObj.yPosMemInNanometersRecord(timeIdx2PlotMem,:);
                                numFilaments2Plot = matFileObj.numFilamentsRecord(timeIdx2PlotMem,:);
                                numCappedFilaments2Plot = matFileObj.numCappedFilamentsRecord(timeIdx2PlotMem,:);
                                lf2Plot = matFileObj.lfRecord(timeIdx2PlotFil,:);
                                isCapped2Plot = matFileObj.filamentIsCappedRecord(timeIdx2PlotFil,:);
                                thetaVals2Plot = matFileObj.thetaValsRecord(timeIdx2PlotFil,:);

                    % Create the ypos diff matrix
                        yPosMemInNanometers2Plot = transpose(yPosMemInNanometers2Plot);
                        meanYPos = mean(yPosMemInNanometers2Plot);
                        yPosMemInNanometersDiff = bsxfun(@minus,yPosMemInNanometers2Plot,meanYPos);
                        yPosValsInNanometersCroppedDiff = transpose(yPosMemInNanometersDiff);
                        yPosMemInNanometers2Plot = transpose(yPosMemInNanometers2Plot);

                    % Rename variables
                        xPosValsInMicronsCropped = xPosMemInNanometers/10^3;
                        timeValsInSeconds2PlotMem = timeValsInMillisecondsMem(timeIdx2DisplayMem)*10^(-3);
                        timeValsInSeconds2PlotFilament = timeValsInMillisecondsFil(timeIdx2DisplayFil)*10^(-3);
                        yPosDiffValsInNanometersCropped = yPosValsInNanometersCroppedDiff;
                        nanometersPerPixel = xSepMemSegmentsInNanometers;
                end
            else
                'Simulation not complete'    
                ginput();
            end
         

%% Plot the number, avg length, and avg angle of the filaments over time

% Choose the timepoint where the simulation reached steady state, and
% create histograms using data points from all timepoints after
    numTimePointMemSteadyState = round(size(yPosDiffValsInNanometersCropped,1)/2);
    numTimePointFilamentSteadyState = round(numTimeStepsFilament2Record/2);

% Plot the filament density
    % Plot the average over time
        figure(2)
        subplot(2,5,1)
        % Calculate the mean and standard deviation for each timepoint
%             numFilamentsMean = nanmean(numFilaments2Plot'/xSepMemSegmentsInNanometers);
%             numFilamentsSD = nanstd(numFilaments2Plot'/xSepMemSegmentsInNanometers);
            numFilamentsMean = nanmean((numFilaments2Plot-numCappedFilaments2Plot)'/xSepMemSegmentsInNanometers);
            numFilamentsSD = nanstd((numFilaments2Plot-numCappedFilaments2Plot)'/xSepMemSegmentsInNanometers);
        % Pull out the indices that don't contain nans
            nonanidx = ~(isnan(numFilamentsMean)|isnan(numFilamentsSD));
            numFilamentsMean = numFilamentsMean(nonanidx);
            numFilamentsSD = numFilamentsSD(nonanidx);
            timeValsInSeconds2PlotMemNoNan = timeValsInSeconds2PlotMem(nonanidx);
        % Plot the mean as a function of time    
            plot(timeValsInSeconds2PlotMemNoNan,numFilamentsMean,'color',CMLinePlots(numMatFile,:))
            hold on;
        % Plot the standard deviation as shading    
            fill([timeValsInSeconds2PlotMemNoNan,fliplr(timeValsInSeconds2PlotMemNoNan)],...
                [numFilamentsMean+numFilamentsSD, fliplr(numFilamentsMean-numFilamentsSD)],...
                CMLinePlots(numMatFile,:),'FaceAlpha',0.3,'EdgeAlpha',0)
        % If it's the last matfile 2 plot, close off the graph 
        if  numMatFile == numMatFiles2Plot 
            hold off;
            title('Linear filament density')
            xlabel('Time (sec)')
            ylim([0 1])
            ylabel('Linear filament density (filaments/nm)')
            %legend(legendText,'Location','NorthEast')
            set(gca,'FontName','Helvetica','FontSize',16);
        end
    
    % Plot the histogram
        figure(2)
        subplot(2,5,6)
        % Pull out all of the steady state measurements, and remove the
        % nans
            filDensityAll = (numFilaments2Plot-numCappedFilaments2Plot)'/xSepMemSegmentsInNanometers;
            filDensityAll = filDensityAll(:);
            filDensityAll(isnan(filDensityAll)) = [];
        % Choose the histogram bin edges
            xLimMemPos = maxNumFilaments2Sim/xSepMemSegmentsInNanometers;
            xLimTick = 1/xSepMemSegmentsInNanometers;
            memPosBinEdges = (xLimTick/2):xLimTick:xLimMemPos;
        % Plot a histogram of these measurements
            histogram(filDensityAll,memPosBinEdges,...
                'Normalization','pdf','FaceColor',CMLinePlots(numMatFile,:),...
                'FaceAlpha',0.3,'EdgeAlpha',0)
            hold on;
        % If it's the last matfile 2 plot, close off the graph 
        if  numMatFile == numMatFiles2Plot 
            hold off;
            title('Linear filament density')
            xlim([0 1])
            xlabel('Linear filament density (filaments/nm)')
            ylabel('Probability density (1/(filaments/nm))')
            %legend(legendTextHist,'Location','Northeast')
            set(gca,'FontName','Helvetica','FontSize',16);  
        end        

% Plot the filament length
    % Plot the average over time
        figure(2)
        subplot(2,5,2)
        % Calculate the mean and standard deviation for each timepoint
            lfRecordNoCap = lf2Plot;
            lfRecordNoCap(isCapped2Plot) = nan;
            lfMean = nanmean(lfRecordNoCap');
            lfSD = nanstd(lfRecordNoCap'); 
        % Pull out the indices that don't contain nans
            nonanidx = ~(isnan(lfMean)|isnan(lfSD));
            lfMean = lfMean(nonanidx);
            lfSD = lfSD(nonanidx);
            timeValsInSeconds2PlotFilamentNoNan = timeValsInSeconds2PlotFilament(nonanidx);
        % Plot the mean as a function of time    
            plot(timeValsInSeconds2PlotFilamentNoNan,lfMean,'color',CMLinePlots(numMatFile,:))
            hold on;
        % Plot the standard deviation as shading    
            fill([timeValsInSeconds2PlotFilamentNoNan,fliplr(timeValsInSeconds2PlotFilamentNoNan)],...
                [lfMean+lfSD, fliplr(lfMean-lfSD)],...
                CMLinePlots(numMatFile,:),'FaceAlpha',0.3,'EdgeAlpha',0)
        % If it's the last matfile 2 plot, close off the graph 
        if  numMatFile == numMatFiles2Plot 
            hold off;
            title('Filament length')
            xlabel('Time (sec)')
            ylim([0 350])
            ylabel('Filament length (nm)')
            %legend(legendText,'Location','NorthEast')
            set(gca,'FontName','Helvetica','FontSize',16);
        end
    
    % Plot the histogram
        figure(2)
        subplot(2,5,7)
        % Pull out all of the steady state measurements, and remove the
        % nans
            lfAll = lfRecordNoCap(:);
            lfAll(isnan(lfAll)) = [];
        % Choose the histogram bin edges
            xLimMemPos = 1000;
            xLimTick = 2.7;
            memPosBinEdges = (xLimTick/2):xLimTick:xLimMemPos;
        % Plot a histogram of these measurements
            histogram(lfAll,memPosBinEdges,...
                'Normalization','pdf','FaceColor',CMLinePlots(numMatFile,:),...
                'FaceAlpha',0.3,'EdgeAlpha',0)
            hold on;
        % If it's the last matfile 2 plot, close off the graph 
        if  numMatFile == numMatFiles2Plot 
            hold off;
            title('Filament length')
            xlim([0 500])
            xlabel('Filament length (nm)')
            ylabel('Probability density (1/nm)')
            %legend(legendTextHist,'Location','Northeast')
            set(gca,'FontName','Helvetica','FontSize',16); 
            hold off;
        end    
        
% Plot the filament angle
    % Plot the average over time
        figure(2)
        subplot(2,5,3)
        % Calculate the mean and standard deviation for each timepoint                 
            % Loop through each recorded timepoint and ft the mean and standard
            % deviation
                % Pre-allocate space to store the mean and the standard
                % deviation
                    thetaValsMean = nan([1, size(thetaVals2Plot,1)]);
                    thetaValsSD = nan([1, size(thetaVals2Plot,1)]);              
                % Determine the branch angle in degrees
                    branchAngleInDegrees = branchAngle*180/pi;
                % And 1/2 the branching andles
                    branchAngleHalfInDegrees = branchAngleInDegrees/2; 
                % Run the for loop    
                    for numTimePoint2Fit = 1:size(thetaVals2Plot,1)  
                        numTPPads=0;
                        numFilsThisTP=0;
                        while (numFilsThisTP<(90*100))&&(numTPPads<5)
                        % Pull out the timepoints used to estimate the mode
                        % filament angle
                            tp2pull = max(1,numTimePoint2Fit-numTPPads):min(size(thetaVals2Plot,1),numTimePoint2Fit+numTPPads);
                        % Pull out the theta values for this timepoint, take
                        % the absolute value, and convert to degrees
                            thetaVals2Hist2Fit = abs(thetaVals2Plot(tp2pull,:))*180/pi; 
                        % Determine how many filaments there are
                            numFilsThisTP = sum(~isnan(thetaVals2Hist2Fit(:)));
                        % Update the pad variable
                            numTPPads = numTPPads + 1;
                        end
                        % Remove the sympathetic peaks at larger branching angles
                            % Find the approximate average angle
                                % Choose the bin size 
                                    binSize = 1;
                                % Bin the filament angles to the nearest degree
                                    thetaVals2HistBinned = floor(thetaVals2Hist2Fit(:)/binSize)'*binSize;
                                % Find the mode of these values    
                                    estimatedMeanTheta1 = mode(thetaVals2HistBinned(thetaVals2HistBinned<90));
                            % Remove filaments outside of this window                        
                                thetaVals2Hist2Fit(thetaVals2Hist2Fit>(estimatedMeanTheta1+branchAngleHalfInDegrees)) = [];
                                thetaVals2Hist2Fit(thetaVals2Hist2Fit<(estimatedMeanTheta1-branchAngleHalfInDegrees)) = [];
                                thetaVals2Hist2Fit(isnan(thetaVals2Hist2Fit))=[];
                        % Record the median and the standard deviation  
                            thetaValsMean(numTimePoint2Fit) = median(thetaVals2Hist2Fit);
                            thetaValsSD(numTimePoint2Fit) = std(thetaVals2Hist2Fit);
                    end
        % Pull out the indices that don't contain nans
            nonanidx = ~(isnan(thetaValsMean)|isnan(thetaValsSD));
            thetaValsMean = thetaValsMean(nonanidx);
            thetaValsSD = thetaValsSD(nonanidx);
            timeValsInSeconds2PlotFilamentNoNan = timeValsInSeconds2PlotFilament(nonanidx);
        % Plot the mean as a function of time    
            plot(timeValsInSeconds2PlotFilamentNoNan,thetaValsMean,'color',CMLinePlots(numMatFile,:))
            hold on;
        % Plot the standard deviation as shading    
            fill([timeValsInSeconds2PlotFilamentNoNan,fliplr(timeValsInSeconds2PlotFilamentNoNan)],...
                [thetaValsMean+thetaValsSD, fliplr(thetaValsMean-thetaValsSD)],...
                CMLinePlots(numMatFile,:),'FaceAlpha',0.3,'EdgeAlpha',0)
        % If it's the last matfile 2 plot, close off the graph 
        if  numMatFile == numMatFiles2Plot 
            hold off;
            title('Filament angle')
            xlabel('Time (sec)')
            ylim([0 90])
            ylabel('Filament angle (�)')
            legend(legendText,'Location','NorthEast')
            set(gca,'FontName','Helvetica','FontSize',16);
        end
    
    % Plot the histogram
        figure(2)
        subplot(2,5,8)
        % Pull out all of the steady state measurements, and remove the
        % nans, take the absolute value, and convert to degrees
            thetaValsAll = abs(thetaVals2Plot(round(size(thetaVals2Plot,1)/2):size(thetaVals2Plot,1),:))*180/pi; 
            thetaValsAllthetaValsAll = thetaValsAll(:);
            thetaValsAll(thetaValsAll>180) = nan;
            thetaValsAll(isnan(thetaValsAll)) = [];
        % Choose the histogram bin edges
            xLimMemPos = 90;
            xLimTick = 1.75;
            memPosBinEdges = 0:xLimTick:xLimMemPos;
        % Plot a histogram of these measurements
            histogram(thetaValsAll,memPosBinEdges,...
                'Normalization','pdf','FaceColor',CMLinePlots(numMatFile,:),...
                'FaceAlpha',0.3,'EdgeAlpha',0)
            hold on;
        % If it's the last matfile 2 plot, close off the graph 
        if  numMatFile == numMatFiles2Plot 
            hold off;
            title('Filament angle')
            xlim([0 90])
            xlabel('Filament angle (�)')
            ylabel('Probability density (1/�)')
            legend(legendTextHist,'Location','Northeast')
            set(gca,'FontName','Helvetica','FontSize',16); 
        end           
        
   
% Plot the membrane speed
    % Plot the average over time
        figure(2)
        subplot(2,5,4)
        % Load in the data
            speed2Plot = matFileObj.vYPosMemInNanometersRecord(timeIdx2PlotMem,:);
        % Calculate the mean and standard deviation for each timepoint
            memSpeedMean = nanmean(speed2Plot');
            memSpeedSD = nanstd(speed2Plot'); 
        % Pull out the indices that don't contain nans
            nonanidx = ~(isnan(memSpeedMean)|isnan(memSpeedSD));
            memSpeedMean = memSpeedMean(nonanidx);
            memSpeedSD = memSpeedSD(nonanidx);
            timeValsInSeconds2PlotMemNoNan = timeValsInSeconds2PlotMem(nonanidx);
        % Plot the mean as a function of time    
            plot(timeValsInSeconds2PlotMemNoNan,memSpeedMean,'color',CMLinePlots(numMatFile,:))
            hold on;
        % Plot the standard deviation as shading    
            fill([timeValsInSeconds2PlotMemNoNan,fliplr(timeValsInSeconds2PlotMemNoNan)],...
                [memSpeedMean+memSpeedSD, fliplr(memSpeedMean-memSpeedSD)],...
                CMLinePlots(numMatFile,:),'FaceAlpha',0.3,'EdgeAlpha',0)
        % If it's the last matfile 2 plot, close off the graph 
        if  numMatFile == numMatFiles2Plot 
            hold off;
            title('Membrane velocity')
            xlabel('Time (sec)')
            ylim([0 1])
            ylabel('Membrane segment velocity (nm/ms)')
            legend(legendText,'Location','NorthEast')
            set(gca,'FontName','Helvetica','FontSize',16);
        end
    
    % Plot the histogram
        figure(2)
        subplot(2,5,9)
        % Pull out all of the steady state measurements, and remove the
        % nans
            memSpeedAll = speed2Plot(numTimePointMemSteadyState:end,:);
            memSpeedAll = memSpeedAll(:);
            memSpeedAll(isnan(memSpeedAll)) = [];
        % Choose the histogram bin edges
            xLimMemPos = 1;
            xLimTick = 0.01;
            memPosBinEdges = -xLimMemPos:xLimTick:xLimMemPos;
        % Plot a histogram of these measurements
            histogram(memSpeedAll,memPosBinEdges,...
                'Normalization','pdf','FaceColor',CMLinePlots(numMatFile,:),...
                'FaceAlpha',0.3,'EdgeAlpha',0)
            hold on;
        % If it's the last matfile 2 plot, close off the graph 
        if  numMatFile == numMatFiles2Plot 
            hold off;
            title('Membrane velocity')
            xlim([-xLimMemPos/10 xLimMemPos])
            xlabel('Membrane segment velocity (nm/ms)')
            ylabel('Probability density (1/(nm/ms))')
            legend(legendTextHist,'Location','Northeast')
            set(gca,'FontName','Helvetica','FontSize',16);           
        end        

        
% Plot the membrane position variability
    % Plot the average over time
        figure(2)
        subplot(2,5,5)
        % Calculate the mean and standard deviation for each timepoint
            memPosMean = nanmean(yPosDiffValsInNanometersCropped');
            memPosSD = nanstd(yPosDiffValsInNanometersCropped'); 
        % Pull out the indices that don't contain nans
            nonanidx = ~(isnan(memPosMean)|isnan(memPosMean));
            memPosMean = memPosMean(nonanidx);
            memPosSD = memPosSD(nonanidx);
            timeValsInSeconds2PlotMemNoNan = timeValsInSeconds2PlotMem(nonanidx);
        % Plot the mean as a function of time    
            plot(timeValsInSeconds2PlotMemNoNan,memPosMean,'color',CMLinePlots(numMatFile,:))
            hold on;
        % Plot the standard deviation as shading    
            fill([timeValsInSeconds2PlotMemNoNan,fliplr(timeValsInSeconds2PlotMemNoNan)],...
                [memPosMean+memPosSD, fliplr(memPosMean-memPosSD)],...
                CMLinePlots(numMatFile,:),'FaceAlpha',0.3,'EdgeAlpha',0)
        % If it's the last matfile 2 plot, close off the graph 
        if  numMatFile == numMatFiles2Plot 
            hold off;
            title('Membrane position variability')
            xlabel('Time (sec)')
            ylim([-100 100])
            ylabel({'Membrane position (nm)','(difference from average)'})
            legend(legendText,'Location','Northeast')
            set(gca,'FontName','Helvetica','FontSize',16); 
        end
    
    % Plot the histogram
        figure(2)
        subplot(2,5,10)
        % Pull out all of the steady state measurements, and remove the
        % nans
            memPosAll = yPosDiffValsInNanometersCropped(numTimePointMemSteadyState:end,:);
            memPosAll = memPosAll(:);
            memPosAll(isnan(memPosAll)) = [];
        % Choose the histogram bin edges
            xLimMemPos = 500;
            xLimTick = 10;
            memPosBinEdges = -xLimMemPos:xLimTick:xLimMemPos;
        % Plot a histogram of these measurements
            histogram(memPosAll,memPosBinEdges,...
                'Normalization','pdf','FaceColor',CMLinePlots(numMatFile,:),...
                'FaceAlpha',0.3,'EdgeAlpha',0)
            hold on;
        % If it's the last matfile 2 plot, close off the graph 
        if  numMatFile == numMatFiles2Plot 
            hold off;
            title('Membrane position variability')
            xlim([-xLimMemPos xLimMemPos])
            xlabel({'Membrane position (nm)','(difference from average)'})
            ylabel('Probability density (nm(-1))')
            legend(legendTextHist,'Location','Northeast')
            set(gca,'FontName','Helvetica','FontSize',16);             
        end
        

  drawnow

    


 end
 
    % Maximize the image
        figHandle = figure(2);
        figHandle.Position =  [-237 422 2797 905];
%      %% Save the figure
%         % Choose the filename and path for the figure
%             destinationTrack = sprintf(['/Users/Rikki/Documents/Leading Edge Fluctuations',...
%                 '/Manuscript/Figures/SupplementaryFigureX/SuppFigX_%s.eps'],...
%                 dataNameForPlottingPrefix);
%         % Save the file
%             print(gcf,destinationTrack,'-depsc','-painters')
            
end