%% This script plots the autocorrelation analysis fit results
% (Run as part of plotSpatialFFTModeTimeAutocorrSummaryStats_Sims.m)

% This code was written by Rikki M. Garner as a companion to the manuscript
% titled "Leading edge stability in motile cells in an emergent property of
% branched actin network growth", and was last updated 2020/08/19.

%%


    % Create an empty legend text cell
    if autocorrDataConcatenated.numConditions > 1
        % Don't plot the SD
            legendTextScatterAmp = cell([1 ((autocorrDataConcatenated.numConditions))]);
            legendTextScatterAmp(:) = {''};
            legendTextAmp = cell([1 ((autocorrDataConcatenated.numConditions*2))]);
            legendTextAmp(:) = {''};
            legendTextScatterDecayRate = cell([1 ((autocorrDataConcatenated.numConditions))]);
            legendTextScatterDecayRate(:) = {''};
            legendTextDecayRate = cell([1 ((autocorrDataConcatenated.numConditions*2))]);
            legendTextDecayRate(:) = {''};
    else
        % Plot the SD
            legendTextScatterAmp = cell([1 ((autocorrDataConcatenated.numConditions))]);
            legendTextScatterAmp(:) = {''};
            legendTextAmp = cell([1 ((autocorrDataConcatenated.numConditions*3))]);
            legendTextAmp(:) = {''};
            legendTextScatterDecayRate = cell([1 ((autocorrDataConcatenated.numConditions))]);
            legendTextScatterDecayRate(:) = {''};
            legendTextDecayRate = cell([1 ((autocorrDataConcatenated.numConditions*3))]);
            legendTextDecayRate(:) = {''};
    end
    
    % Choose the axes limits
        % Spatial frequency
            xLimsSpatialFreq = [0.025 7];
            xtickSpatialFreq = [0.05 0.1 0.2 0.4 0.8 1.6 3.2 6.4]; 
        % Amplitudes
            exponentValuesAmp = 0:8;
            yLimsAmp = [10^(exponentValuesAmp(1)) 10^(exponentValuesAmp(end))];
            yTicksAmp = 10.^(exponentValuesAmp);
            yTickLabelsAmp = cellstr(strcat(repmat('10^{',length(exponentValuesAmp),1),...
                string(exponentValuesAmp)',repmat('}',length(exponentValuesAmp),1))); 
        % Decay rates
            exponentValuesDecayRate = -6:0;
            yLimsDecayRate = [10^(exponentValuesDecayRate(1)) 10^(exponentValuesDecayRate(end))];
            yTicksDecayRate = 10.^(exponentValuesDecayRate);
            yTickLabelsDecayRate = cellstr(strcat(repmat('10^{',length(exponentValuesDecayRate),1),...
                string(exponentValuesDecayRate)',repmat('}',length(exponentValuesDecayRate),1)));

	% Plot the fitted amplitude of the exponential decay
        % Make the scatter plot
            % Pull up the plot
                figure(1);
                subplot(numRows,numCols,numSubplotScatterAmp)
            % Loop through each condition and plot it
            for numCondition = 1:length(autocorrDataConcatenated.conditionOrder2Plot)

                % Pull out the condition to plot
                    numCondition2Plot = conditionOrder2Plot(numCondition);

                % Pull out the amplitudes
                    yVals = autocorrDataConcatenated.ampConst(...
                       autocorrDataConcatenated.conditionIdentity==numCondition2Plot);
                    nonNanIdx = ~isnan(yVals);
                    yVals = yVals(nonNanIdx);         
                if ~isempty(yVals)
                % Pull out the frequency bins
                    xVals = autocorrDataConcatenated.spatialFrequencyInInverseNanometers(...
                       autocorrDataConcatenated.conditionIdentity==numCondition2Plot);
                    xVals = xVals(nonNanIdx);
                % Plot the scatter plot   
                    p = scatter(xVals*1000,yVals,[],'MarkerFaceColor',...
                        autocorrDataConcatenated.CMByCondition(numCondition2Plot,:),...
                        'MarkerEdgeColor','none');
                    p.MarkerFaceAlpha = alphaVal;
                    clear p
                    hold on;
                % Update the legend text 
                    if autocorrDataConcatenated.numConditions>1
                        legendTextScatterAmp{numCondition} = ['Fit of single cell - ' autocorrDataConcatenated.conditionNames{numCondition2Plot}];
                    else
                        legendTextScatterAmp{numCondition} = ['Fit of single cell'];
                    end
                end
            end
            % Plot sample power laws
                % Count the maximum number of spatial frequency bins
                    [numFreqBinsByCondition] = histc(autocorrDataConcatenated.groupIdentityAverages,...
                        1:autocorrDataConcatenated.numConditions);
                    firstConditionWithMaxFreqBins = find(numFreqBinsByCondition == ...
                        max(numFreqBinsByCondition),1,'first');
                % Pull out the frequency values to plot
                    spatialFrequencyInInverseMicrons2Plot = ...
                        autocorrDataConcatenated.spatialfrequencyInInverseMicronsAverages(...
                        autocorrDataConcatenated.groupIdentityAverages==firstConditionWithMaxFreqBins);
                % Plot the power law of -4    
                    loglog(spatialFrequencyInInverseMicrons2Plot,...
                        0.7*10^3.*spatialFrequencyInInverseMicrons2Plot.^(-4),'k-','LineWidth',2)
                % Plot the power law of -2    
                    loglog(spatialFrequencyInInverseMicrons2Plot,...
                        2*10^6.*spatialFrequencyInInverseMicrons2Plot.^(-2),'k--','LineWidth',1)
            % Annotate the plots
                hold off;    
                box on;   
                set(gca,'YScale','log')
                set(gca,'XScale','log')
                ylabel({'Fitted amplitude (nm^2)'})
                xlabel('Spatial frequency (�m^{-1})')
                title({[autocorrDataConcatenated.datasetName 's'],...
                    'Fitted amplitude','of exponential decay component'})
                % Delete emtpy legend elements
                    legendTextScatterAmp(...
                        find(cellfun(@isempty,legendTextScatterAmp))) = [];
                legend([legendTextScatterAmp,'Power law = -4','Power law = -2'],'Location','SouthWest')             
                set(gca,'FontName','Arial','FontSize',12,'FontWeight','Bold');  


            % Plot the binned and averaged values
            % Pull up the plot
                figure(1);
                subplot(numRows,numCols,numSubplotSummaryStatsAmp)
            % Loop through each condition and plot it
            for numCondition = 1:length(autocorrDataConcatenated.conditionOrder2Plot)

                % Pull out the condition to plot
                    numCondition2Plot = conditionOrder2Plot(numCondition);

                % Pull out the amplitudes
                    yVals = autocorrDataConcatenated.ampConstMedians(...
                        autocorrDataConcatenated.groupIdentityAverages==numCondition2Plot);
                    nonNanIdx = ~isnan(yVals);
                    yVals = yVals(nonNanIdx);
                if ~isempty(yVals)
                % Pull out the frequency bins
                    xVals = autocorrDataConcatenated.spatialfrequencyInInverseMicronsAverages(...
                        autocorrDataConcatenated.groupIdentityAverages==numCondition2Plot);
                    xVals = xVals(nonNanIdx);
                % Pull out the SE to plot
                    SEVals = autocorrDataConcatenated.ampConstSE(...
                        autocorrDataConcatenated.groupIdentityAverages==numCondition2Plot);
                    SEVals = SEVals(nonNanIdx);
                % Plot the medians    
                    loglog(xVals,yVals,'o-','color',autocorrDataConcatenated.CMByCondition(numCondition,:),...
                        'MarkerFaceColor',autocorrDataConcatenated.CMByCondition(numCondition,:),...
                        'MarkerEdgeColor','none','MarkerSize',10,'LineWidth',1)
                    hold on;
                % Plot the standard error as error bars    
                    errorbar(xVals,yVals,SEVals,'r','LineStyle', 'none')
                % Update the legend text  
                    if autocorrDataConcatenated.numConditions>1
                        legendTextAmp{2*(numCondition-1)+1} = ['Median - ' autocorrDataConcatenated.conditionNames{numCondition2Plot}];
                        legendTextAmp{2*(numCondition-1)+2} = '+- 1 SE';
                    else
                        % Plot the standard deviation
                            % Pull out the STD to plot
                                SDVals = autocorrDataConcatenated.ampConstSTD(...
                                    autocorrDataConcatenated.groupIdentityAverages==numCondition2Plot);
                                SDVals = SDVals(nonNanIdx);
                            % Plot the standard deviation as a filled area
                                lowerFillVal = yVals-SDVals;
                                lowerFillVal(lowerFillVal<yLimsAmp(1)) = yLimsAmp(1);
                                fill([xVals,fliplr(xVals)],[lowerFillVal, fliplr(yVals+SDVals)],...
                                    'k','FaceAlpha',0.3,'EdgeAlpha',0)
                        legendTextAmp{3*(numCondition-1)+1} = ['Median'];
                        legendTextAmp{3*(numCondition-1)+2} = '+- 1 SE';
                        legendTextAmp{3*(numCondition-1)+3} = '+- 1 SD';
                    end
                end
            end
            % Plot sample power laws
                % Plot the power law of -4    
                    loglog(spatialFrequencyInInverseMicrons2Plot,...
                        0.7*10^3.*spatialFrequencyInInverseMicrons2Plot.^(-4),'k-','LineWidth',2)
                % Plot the power law of -2    
                    loglog(spatialFrequencyInInverseMicrons2Plot,...
                        2*10^6.*spatialFrequencyInInverseMicrons2Plot.^(-2),'k--','LineWidth',1)
            % Annotate the plots
                hold off;    
                box on;   
                set(gca,'YScale','log')
                set(gca,'XScale','log')
                ylabel({'Fitted amplitude (nm^2)'})
                xlabel('Spatial frequency (�m^{-1})')
                title({[autocorrDataConcatenated.datasetName 's'],...
                    'Fitted amplitude','of exponential decay component'})
                % Delete empty legend elements
                    legendTextAmp(...
                        find(cellfun(@isempty,legendTextAmp))) = ...
                        [];
                legend([legendTextAmp,'Power law = -4','Power law = -2'],'Location','SouthWest')             
                set(gca,'FontName','Arial','FontSize',12,'FontWeight','Bold');

            
	% Plot the exponential decay constant  
        % Make the scatter plot
            % Pull up the plot
                figure(1);
                subplot(numRows,numCols,numSubplotScatterDecay)
            % Loop through each condition and plot it
            for numCondition = 1:length(autocorrDataConcatenated.conditionOrder2Plot)

                % Pull out the condition to plot
                    numCondition2Plot = conditionOrder2Plot(numCondition);

                % Pull out the decay rates
                    yVals = autocorrDataConcatenated.decayConst(...
                       autocorrDataConcatenated.conditionIdentity==numCondition2Plot);
                    nonNanIdx = ~isnan(yVals);
                    yVals = yVals(nonNanIdx);      
                if ~isempty(yVals)  
                % Pull out the frequency bins
                    xVals = autocorrDataConcatenated.spatialFrequencyInInverseNanometers(...
                       autocorrDataConcatenated.conditionIdentity==numCondition2Plot);
                    xVals = xVals(nonNanIdx);
                % Plot the scatter plot   
                    p = scatter(xVals*1000,yVals,[],'MarkerFaceColor',...
                        autocorrDataConcatenated.CMByCondition(numCondition2Plot,:),...
                        'MarkerEdgeColor','none');
                    p.MarkerFaceAlpha = alphaVal;
                    clear p
                    hold on;
                % Update the legend text  
                    if autocorrDataConcatenated.numConditions>1
                        legendTextScatterDecayRate{numCondition} = ['Fit of single cell - ' autocorrDataConcatenated.conditionNames{numCondition2Plot}];
                    else
                        legendTextScatterDecayRate{numCondition} = ['Fit of single cell'];
                    end
                end
            end
            % Plot sample power laws
                % Plot the power law of 2    
                    loglog(spatialFrequencyInInverseMicrons2Plot,...
                        2*10^(-2).*spatialFrequencyInInverseMicrons2Plot.^(2),'k--','LineWidth',1)
                % Plot the power law of 1    
                    loglog(spatialFrequencyInInverseMicrons2Plot,...
                        5*10^(-5).*spatialFrequencyInInverseMicrons2Plot.^(1),'k-','LineWidth',1)
            % Annotate the plots
                hold off;    
                box on;   
                set(gca,'YScale','log')
                set(gca,'XScale','log')
                ylabel({'Fitted decay constant (ms^{-1})'})
                xlabel('Spatial frequency (�m^{-1})')
                title({[autocorrDataConcatenated.datasetName 's'],'Decay rate','of exponential decay component'}) 
                % Delete emtpy legend elements
                    legendTextScatterDecayRate(...
                        find(cellfun(@isempty,legendTextScatterDecayRate))) = [];
                legend([legendTextScatterDecayRate,'Power law = 2','Power law = 1'],...
                    'Location','NorthWest')                  
                set(gca,'FontName','Arial','FontSize',12,'FontWeight','Bold');   

        % Plot the binned values
            % Pull up the plot
                figure(1);
                subplot(numRows,numCols,numSubplotSummaryStatsDecay)
            % Loop through each condition and plot it
            for numCondition = 1:length(autocorrDataConcatenated.conditionOrder2Plot)

                % Pull out the condition to plot
                    numCondition2Plot = conditionOrder2Plot(numCondition);

                % Pull out the decay rates
                    yVals = autocorrDataConcatenated.decayConstMedians(...
                       autocorrDataConcatenated. groupIdentityAverages==numCondition2Plot);
                    nonNanIdx = ~isnan(yVals);
                    yVals = yVals(nonNanIdx);
                if ~isempty(yVals)
                % Pull out the frequency bins
                    xVals = autocorrDataConcatenated.spatialfrequencyInInverseMicronsAverages(...
                        autocorrDataConcatenated.groupIdentityAverages==numCondition2Plot);
                    xVals = xVals(nonNanIdx);
                % Pull out the SE to plot
                    SEVals = autocorrDataConcatenated.decayConstSE(...
                        autocorrDataConcatenated.groupIdentityAverages==numCondition2Plot);
                    SEVals = SEVals(nonNanIdx);
                % Plot the medians    
                    loglog(xVals,yVals,'o-','color',autocorrDataConcatenated.CMByCondition(numCondition,:),...
                            'MarkerFaceColor',autocorrDataConcatenated.CMByCondition(numCondition,:),...
                            'MarkerEdgeColor','none','MarkerSize',10,'LineWidth',1)
                    hold on;
                % Plot the standard error as error bars    
                    errorbar(xVals,yVals,SEVals,'r','LineStyle', 'none')
                % Update the legend text       
                    if autocorrDataConcatenated.numConditions>1
                        legendTextDecayRate{2*(numCondition-1)+1} = ['Median - ' autocorrDataConcatenated.conditionNames{numCondition2Plot}];
                        legendTextDecayRate{2*(numCondition-1)+2} = '+- 1 SE';
                    else
                        % Plot the SD 
                            % Pull out the SD to plot
                                SDVals = autocorrDataConcatenated.decayConstSTD(...
                                    autocorrDataConcatenated.groupIdentityAverages==numCondition2Plot);
                                SDVals = SDVals(nonNanIdx);
                            % Plot the standard deviation as a filled area
                                lowerFillVal = yVals-SDVals;
                                lowerFillVal(lowerFillVal<yLimsDecayRate(1)) = yLimsDecayRate(1);
                                fill([xVals,fliplr(xVals)],[lowerFillVal, fliplr(yVals+SDVals)],...
                                    'k','FaceAlpha',0.3,'EdgeAlpha',0)
                        legendTextDecayRate{3*(numCondition-1)+1} = ['Median'];
                        legendTextDecayRate{3*(numCondition-1)+2} = '+- 1 SE';
                        legendTextDecayRate{3*(numCondition-1)+3} = '+- 1 SD';
                    end
                end
            end
            % Plot sample power laws
                % Plot the power law of 2    
                    loglog(spatialFrequencyInInverseMicrons2Plot,...
                        2*10^(-2).*spatialFrequencyInInverseMicrons2Plot.^(2),'k--','LineWidth',1)
                % Plot the power law of 1    
                    loglog(spatialFrequencyInInverseMicrons2Plot,...
                        5*10^(-5).*spatialFrequencyInInverseMicrons2Plot.^(1),'k-','LineWidth',1)
            % Annotate the plots
                hold off;      
                box on;      
                set(gca,'YScale','log')
                set(gca,'XScale','log')
                ylabel({'Fitted decay constant (ms^{-1})'})
                xlabel('Spatial frequency (�m^{-1})')
                title({[autocorrDataConcatenated.datasetName 's'],...
                    'Decay rate','of exponential decay component'})   
                % Delete emtpy legend elements
                    legendTextDecayRate(...
                        find(cellfun(@isempty,legendTextDecayRate))) = [];
                legend([legendTextDecayRate,'Power law = 2','Power law = 1'],...
                    'Location','NorthWest')                  
                set(gca,'FontName','Arial','FontSize',12,'FontWeight','Bold');    


