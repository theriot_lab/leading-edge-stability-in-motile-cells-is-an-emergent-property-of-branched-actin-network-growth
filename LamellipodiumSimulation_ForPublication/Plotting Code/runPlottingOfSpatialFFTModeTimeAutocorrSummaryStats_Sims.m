%% This script plots the spatial Fourier mode autocorrelation analysis fit 
% parameters, binned and averaged over all replicates

% This code was written by Rikki M. Garner as a companion to the manuscript
% titled "Leading edge stability in motile cells in an emergent property of
% branched actin network growth", and was last updated 2020/08/19.

%%


% Clear the system
    close all;
    clear all;
    
% Choose the datasets to analyze  
    % Example for a defined set of datasets 
        % Choose the folders containing the data
            allFolderPaths = {['/Users/Rikki/Documents/'...
                'LamellipodiumSimulation_ForPublication/SimulationResults/'...
                'Results20200820/']}; 
        % Calculate the number of folders in this set
            numFolders  = length(allFolderPaths);
            
%     % Example for a folder of directories
%         % Choose the parent folder containing all of the datasets to analyze
%             superFolderPath = ['/Users/Rikki/Documents/'...
%                'LamellipodiumSimulation_ForPublication/SimulationResults/'];
%         % Create a list of all of the folders to loop through
%             % Pull out information about the directory's contents
%                 superFolderPathInfo = dir(superFolderPath);
%             % Only the subdirectory info
%                 superFolderPathInfo = superFolderPathInfo([superFolderPathInfo.isdir]);
%             % Remove the boiler plate stuff    
%                 superFolderPathInfo = superFolderPathInfo(~ismember({superFolderPathInfo.name},{'.','..','.DS_Store'}));
%             % Pull out the number of subdirectories
%                 numFolders = length(superFolderPathInfo);
%             % Create a cell to store the folder paths   
%                 allFolderPaths = cell([numFolders 1]);
%             % Loop through each folder and add its path to the list
%                 for folderNum = 1:numFolders
%                     allFolderPaths{folderNum} = [superFolderPath superFolderPathInfo(folderNum).name '/'];
%                 end
                
% Loop through each folder and run the autocorrelation analysis pipeline
 for folderNum = 1:numFolders
     
    % Print which dataset we're on
        sprintf('Plotting folder %i of %i',folderNum,numFolders)
        
    try
     
	% Choose the dataset to plot
        % Choose the dataset name
            datasetName = 'Simulated leading edge';
        % List the paths to the folders containing the data    
            folderPaths = allFolderPaths(folderNum); 
        % Load the first file and record the relevant information     
            % Load the first file info
                % Determine the file path
                    % Pull out the folder
                        folderPath = folderPaths{1};
                    % Pull out the .mat file info
                        matFileInfo = dir([folderPath '*.mat']);   
                    % Determine the file path
                        matFilePath = [folderPath matFileInfo(1).name];
                % Load the simulation parameter information
                    load(matFilePath,'globalInfo') 
                % Determine the number of parameter vals
                    numParameterVals = length(globalInfo.parameterVals);
            % Choose the folders and respective parameter values to analyze
                folders2Analyze = ones([1 numParameterVals]);
                parameterVals2Analyze = 1:numParameterVals;
            % Choose the condition numbers corresponding to the folder/parameter combos    
                conditionNums = parameterVals2Analyze;
            % Choose the conditions to plot
                conditions2Plot = parameterVals2Analyze;
            % choose the order to plot the conditions    
                conditionOrder2Plot = parameterVals2Analyze;
            % Choose the condition names
                % conditionNames = {'Simulated leading edge'};
                num2sprintfConversion = '%3g';
                datasetInfo = v2struct();
                conditionNames = extractConditionNamesForSims(datasetInfo);
                clear datasetInfo
            % Pull out the associated variable name    
                dataNameForPlottingPrefix = conditionNames{1};
                firstEqualsIdx = find(dataNameForPlottingPrefix=='=',1,'first');
                dataNameForPlottingPrefix = dataNameForPlottingPrefix(1:(firstEqualsIdx-2));
                
	% Plot the summary statistics for the fitted parameters      
        % Aggregate the data by condition and calculate summary statistics
            % Initialize the parameters to run the code
                % Create a struct to store the summary starts
                    autocorrDataConcatenated = struct();
                % Load data into the autocorr structure
                    % Dataset name
                        autocorrDataConcatenated.datasetName = datasetName;
                    % Folders containing the data
                        autocorrDataConcatenated.folderPaths = folderPaths;
                    % List of folder/parameter/condition combos to analyze
                        autocorrDataConcatenated.folders2Analyze = folders2Analyze;
                        autocorrDataConcatenated.parameterVals2Analyze = parameterVals2Analyze;
                        autocorrDataConcatenated.conditionNums = conditionNums;
                    % Which conditions 2 plot
                        autocorrDataConcatenated.conditions2Plot = conditions2Plot;
                    % Which order to plot the parameters in
                        autocorrDataConcatenated.conditionOrder2Plot = conditionOrder2Plot;
                    % Condition names
                        autocorrDataConcatenated.conditionNames = conditionNames;
            % Aggregate the data by condition        
                autocorrDataConcatenated = ...
                    prepSpatialFFTModeAutocorrResultsByCondition_Sims(autocorrDataConcatenated);
            % Determine summary statistics for autocorrelation analysis 
            % by binning and averaging across conditions   
                % Choose how to bin the data
%                     % Example for binning by the median diff_fVal
%                         autocorrDataConcatenated.binFlag='binByMedian';
%                     % Example for manual binning
%                         autocorrDataConcatenated.binFlag='binManually';
%                         autocorrDataConcatenated.manualspatialFrequencyBinSizeInInverseNanometers = 0.2*10^(-3);
                    % Example for binning by the mode of each condition
                    % (appropriate when there is only one step size, but it
                    % might vary by condition, e.g. changing the size of
                    % the leading edge)
                        autocorrDataConcatenated.binFlag='binSeparatelyByCondition';
                % Run the code to compute summary stats
                    autocorrDataConcatenated = makeAutocorrSummaryStats(autocorrDataConcatenated);
                
       %% Plot the results
            % Prepare the plotting parameters
                % Choose the numbner of rows
                    numRows = 1;
                % Choose the number of columns
                    numCols = 4;
                % Choose the subplots for this dataset
                    numSubplotScatterAmp = 1;
                    numSubplotScatterDecay = 2;
                    numSubplotSummaryStatsAmp = 3;
                    numSubplotSummaryStatsDecay = 4;
                % Choose the alpha value
                    alphaVal = min(1,0.03*40/autocorrDataConcatenated.numReplicatesByCondition(1));
            % Run the plotting code
                plotSpatialFFTModeAutocorrSummaryStats
                drawnow
                

    % Link all of the axes
  
        % Define the parameter display axes
        
            % Fitted Amplitudes
                rows = 1;
                cols = [1 3];
                numPlots=0;
                for  rowNum = rows
                    for colNum = cols
                        numPlots=numPlots+1;
                        subplotNum = colNum + (numCols*(rowNum-1));
                        hAxAmpAll(numPlots) = subplot(numRows,numCols,subplotNum);
                    end
                    
                end
                
            % Spatial frequency
                rows = 1;
                cols = [1:4];
                numPlots=0;
                for  rowNum = rows
                    for colNum = cols
                        numPlots=numPlots+1;
                        subplotNum = colNum + (numCols*(rowNum-1));
                        hAxSpatialFrequency(numPlots) = subplot(numRows,numCols,subplotNum);
                    end
                    
                end
                
            % Exponential decay rates
                rows = 1;
                cols = [2 4];
                numPlots=0;
                for  rowNum = rows
                    for colNum = cols
                        numPlots=numPlots+1;
                        subplotNum = colNum + (numCols*(rowNum-1));
                        hAxDecay(numPlots) = subplot(numRows,numCols,subplotNum);
                    end
                    
                end
            
        % Link the axes
            linkAmpAll = linkprop(hAxAmpAll,{'YLim','YTick','YTickLabels'});
            linkSpatialFrequency = linkprop(hAxSpatialFrequency,{'XLim','XTick','XTickLabels'});
            linkDecay = linkprop(hAxDecay,{'YLim','YTick','YTickLabels'});
            
%         % Chooose the limits
%             % FittedAmplitudes
%                 exponentValuesAmp = 3:7.5;
%                 yLimsAmp = [10^(exponentValuesAmp(1)) 10^(7.5)];
%                 yTicksAmp = 10.^(exponentValuesAmp);
%                 yTickLabelsAmp = cellstr(strcat(repmat('10^{',length(exponentValuesAmp),1),...
%                     string(exponentValuesAmp)',repmat('}',length(exponentValuesAmp),1)));       
%             % Spatial frequency
%                 xLimsSpatialFreq = [0.15 1.6];
%                 xtickSpatialFreq = [0.05 0.1 0.2 0.4 0.8 1.6 3.2 6.4]; 
%             % Decay rates
%                 exponentValuesDecayRate = -5:-2;
%                 yLimsDecayRate = [10^(exponentValuesDecayRate(1)) 10^(exponentValuesDecayRate(end))];
%                 yTicksDecayRate = 10.^(exponentValuesDecayRate);
%                 yTickLabelsDecayRate = cellstr(strcat(repmat('10^{',length(exponentValuesDecayRate),1),...
%                     string(exponentValuesDecayRate)',repmat('}',length(exponentValuesDecayRate),1)));        
        % Chooose the limits
            % FittedAmplitudes
                exponentValuesAmp = 2:7;
                yLimsAmp = [0.5*10^(exponentValuesAmp(1)) 10^(7.5)];
                yTicksAmp = 10.^(exponentValuesAmp);
                yTickLabelsAmp = cellstr(strcat(repmat('10^{',length(exponentValuesAmp),1),...
                    string(exponentValuesAmp)',repmat('}',length(exponentValuesAmp),1)));       
            % Spatial frequency
                xLimsSpatialFreq = [0.04 3.8];
                xtickSpatialFreq = [0.05 0.1 0.2 0.4 0.8 1.6 3.2 6.4]; 
            % Decay rates
                exponentValuesDecayRate = -5:-1;
                yLimsDecayRate = [0.5*10^(exponentValuesDecayRate(1)) 0.5*10^(exponentValuesDecayRate(end))];
                yTicksDecayRate = 10.^(exponentValuesDecayRate);
                yTickLabelsDecayRate = cellstr(strcat(repmat('10^{',length(exponentValuesDecayRate),1),...
                    string(exponentValuesDecayRate)',repmat('}',length(exponentValuesDecayRate),1)));
            
            
        % Update the axes
            figure(1)
            % Choose the x-ticks for the spatial frequency and y-ticks for the amplitude
                subplot(numRows,numCols,1)
                % Spatial freuqency
                    xlim(xLimsSpatialFreq)
                    xticks(xtickSpatialFreq)
                    xticklabels(xtickSpatialFreq)
                % Example Amplitudes  
                    ylim(yLimsAmp)
                    yticks(yTicksAmp);
                    yticklabels(yTickLabelsAmp);
            % Choose the y-ticks for the decay rate
                subplot(numRows,numCols,2)
                ylim(yLimsDecayRate)
                yticks(yTicksDecayRate);
                yticklabels(yTickLabelsDecayRate);
                 
        figHandle = figure(1);
        % Maximize the image
            figHandle = figure(1);
            figHandle.Position =  [31 659 1782 372];
%         % Move the legends
%             hLeg=findobj(figHandle,'type','legend');
%             set(hLeg,'Position',[0.0005 0.65 0.1 0.3])

%     %% Save the figure
%         % Choose the filename and path for the figure
%             destinationTrack = sprintf(['/Users/Rikki/Documents/'...
%                 '/LamellipodiumSimulation_ForPublication/Figures/'...
%                 'spatialFFTTimeAutocorrelationFitSummary_%s.eps'],...
%                 dataNameForPlottingPrefix);
%         % Save the file
%             print(gcf,destinationTrack,'-depsc','-painters') 
%             
%         % Save a png version
%             % Choose the filename and path for the figure
%             destinationTrack = sprintf(['/Users/Rikki/Documents/'...
%                 '/LamellipodiumSimulation_ForPublication/Figures/'...
%                 'spatialFFTTimeAutocorrelationFitSummary_%s.png'],...
%                 dataNameForPlottingPrefix); 
%             % Save the file
%                 print(gcf,destinationTrack,'-dpng','-painters') 
          
          if numFolders>1
             clearvars -except allFolderPaths numFolders folderNum  ME
          end
        
    catch ME
       'Failed'             
   end
     
 end
    

     

           

            
