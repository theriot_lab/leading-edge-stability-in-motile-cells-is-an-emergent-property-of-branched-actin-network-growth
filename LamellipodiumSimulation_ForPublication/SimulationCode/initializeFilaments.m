function [inputVars] = initializeFilaments(inputVars)

%% This function initializes the filaments
% (Run as part of setUpAndRun_simulateLamellipodiumAndRunXCorrAnalysis.m)

% This code was written by Rikki M. Garner as a companion to the manuscript
% titled "Leading edge stability in motile cells in an emergent property of
% branched actin network growth", and was last updated 2020/08/19.
%%

    % Unpack this structure and clear the original structure
        v2struct(inputVars)
        clear inputVars                   
        
	% Initialize the filaments        
        % Choose the starting number of filaments such that the density is similar
        % to the leading edge of a fish keratocyte (3nm/filament)
            numFilamentsPerMemSegmentInitial = ceil(xSepMemSegmentsInNanometers/3);
        % Choose the maximum number of filaments to simulate before ending 
        % the simulation early (or else MATLAB will crash)
            maxNumFilaments2Sim = numFilamentsPerMemSegmentInitial*10;
        % Initialize the existing filaments
            filamentExists = false(maxNumFilaments2Sim,numMemSegments);
            filamentExists(1:numFilamentsPerMemSegmentInitial,:) = true;
        % Pre-allocate space to record filament variables
            numFilamentsTotal = sum(filamentExists(:));         
        % Pre-allocate space to store the number of filaments of each bead
            numFilamentsPerMemSegment = numFilamentsPerMemSegmentInitial*ones(1,numMemSegments);
        % If filaments will be fixed, choose the initial filament angle
            if initialFilamentAngleOrder
                initialFilamentAngle = branchAngle/2;
            end
        % Initialize all of the filaments as uncapped
            filamentIsCapped = false(maxNumFilaments2Sim,numMemSegments);
        
    % Load these variables into a structure
        inputVars = v2struct();

end