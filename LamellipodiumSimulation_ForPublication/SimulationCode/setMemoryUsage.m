function [inputVars] = setMemoryUsage(inputVars)

%% This function determines how much memory can be kept in the workspace
% (Run as part of setUpAndRun_simulateLamellipodiumAndRunXCorrAnalysis.m)

% This code was written by Rikki M. Garner as a companion to the manuscript
% titled "Leading edge stability in motile cells in an emergent property of
% branched actin network growth", and was last updated 2020/08/19.
%%

    % Unpack this structure and clear the original structure
        v2struct(inputVars)
        clear inputVars                   


    % Choose how often to record datapoints

        % Choose the maximum amount of data to record in bytes
            maxData2Record = (4*10^9); 

        % Choose the maximum amount of membrane data to record in bytes
            % Choose what fraction of the data saved should be membrane
            % data
                maxData2RecordMembInfo = maxData2Record/2;
            % Choose how many types of data to record
                numTypesDataMem = 9;
            % Calculate when to save the membrane data
                if (numMemSegments*numTimeSteps2Sim*8*numTypesDataMem)>maxData2RecordMembInfo
                    maxNumTimeStepsMem2Record = round((maxData2RecordMembInfo)/(numMemSegments*8*numTypesDataMem));
                    numTimeStepsMem2Skip = ceil(numTimeSteps2Sim/maxNumTimeStepsMem2Record);
                else
                    numTimeStepsMem2Skip = 1;
                end
            % Calculate the time indices to record
                idx2RecordMem = 1:numTimeStepsMem2Skip:numTimeSteps2Sim;
            % Calculate the times recorded
                times2RecordInMillisecondsMem = idx2RecordMem*dtRelaxMemInMilliseconds;
            % Calculate the actual number of timesteps to record
                numTimeStepsMem2Record = length(times2RecordInMillisecondsMem);
                clear idx2RecordMem times2RecordInMillisecondsMem

        % Choose the maximum amount of filament data to record in bytes
            % Calculate how much data is left for filament info
                maxData2RecordFilamentInfo = maxData2Record - maxData2RecordMembInfo;
            % Choose how many types of data to record
                numTypesDataFil = 5;
            % Calculate when to save the filament data 
                if (numMemSegments*maxNumFilaments2Sim*numTimeSteps2Sim*8*numTypesDataFil)>maxData2RecordFilamentInfo
                    maxNumTimeStepsFilament2Record = round((maxData2RecordFilamentInfo)/(numMemSegments*maxNumFilaments2Sim*8*numTypesDataFil));
                    numTimeStepsFilament2Skip = ceil(numTimeSteps2Sim/maxNumTimeStepsFilament2Record);
                else
                    numTimeStepsFilament2Skip = 1;
                end
            % Calculate the time indices to record
                idx2RecordFilament = 1:numTimeStepsFilament2Skip:numTimeSteps2Sim;
            % Calculate the times recorded
                times2RecordInMillisecondsFilament = idx2RecordFilament*dtRelaxMemInMilliseconds;
            % Calculate the actual number of timesteps to record
                numTimeStepsFilament2Record = length(times2RecordInMillisecondsFilament);

    % Choose how often to save recorded datapoints

        % Choose the maximum amount of data to record in bytes
            maxData2KeepInWorkspace = (1*10^9); 

        % Choose the maximum amount of membrane data to record in bytes
            % Choose what fraction of the data saved should be membrane
            % data
                maxData2KeepMembInfoInWorkspace = maxData2KeepInWorkspace/2; 
            % Calculate when to save the membrane data
                if (numMemSegments*numTimeStepsMem2Record*8*numTypesDataMem)>maxData2KeepMembInfoInWorkspace
                    numTimeStepsMem2KeepInWorkspace = floor((maxData2KeepMembInfoInWorkspace)/(numMemSegments*8*numTypesDataMem));
                else
                    numTimeStepsMem2KeepInWorkspace = numTimeStepsMem2Record;
                end
            % Initialize the counter to record and save these points
                numTimeStepsRecordedThisIter_MemInfo=1;
                numTimeStepsRecorded_MemInfo=2;

        % Choose the maximum amount of filament data to record in bytes
            % Calculate how much data is left for filament info
                maxData2RecordFilamentInfo = maxData2KeepInWorkspace - maxData2KeepMembInfoInWorkspace;
            % Calculate when to save the filament data 
                if (numMemSegments*maxNumFilaments2Sim*numTimeStepsFilament2Record*8*numTypesDataFil)>maxData2RecordFilamentInfo
                    numTimeStepsFilament2KeepInWorkspace = round((maxData2RecordFilamentInfo)/(numMemSegments*maxNumFilaments2Sim*8*numTypesDataFil));
                else
                    numTimeStepsFilament2KeepInWorkspace = numTimeStepsFilament2Record;
                end
            % Initialize the counter to record and save these points
                numTimeStepsRecordedThisIter_FilamentInfo=1;
                numTimeStepsRecorded_FilamentInfo=2;

    % Choose how often to generate Brownian forces
        
        % Choose the maximum amount of data to record in bytes
            maxData2CalcRand = (1*10^9);
        % Calculate the number of timesteps to general Brownian forces in
        % one go
            if (numMemSegments*numTimeSteps2Sim*8)>maxData2CalcRand
                numTimeStepsMem2CalcBF = floor(maxData2CalcRand/(numMemSegments*8));
            else
                numTimeStepsMem2CalcBF=numTimeSteps2Sim;
            end
        % Initialize the counter to calculate more
            numTimeStepThisIter_CalcBF=1;
        % Initialize the brownian force vector
            % Calculate the number of time steps to equilibrate this iteration
                numTimeSteps2CalcBFThisIteration = min(numTimeStepsMem2CalcBF,numTimeSteps2Sim);       

        % Choosehow many time points to perform FFT and autocrrelation analysis on
            fracOfTotalTime2Start = 0.2;    
            timeIdx2Analyze = floor(numTimeStepsMem2Record.*fracOfTotalTime2Start):numTimeStepsMem2Record;
            numTimePointsMem2Analyze = length(timeIdx2Analyze);
            clear timeIdx2Analyze
            numWaveModesMem2Analyze = floor(numMemSegments/2)+1;
        % Load in data 500MB at a time for rVals calculations
            nModes = numWaveModesMem2Analyze;
            nTimes = numTimePointsMem2Analyze;
            nTimeSep2CalcAutocorr = round(nTimes*3/4); 
            numBytesPerIterationrVals = 5*10^8;
            numBytesTotal = nModes*numTimePointsMem2Analyze*8*2;
            numIterations = ceil(numBytesTotal/numBytesPerIterationrVals);
            numTimeIdxsPerIteration = round(numBytesPerIterationrVals/(nModes*8*2));
            if numTimeIdxsPerIteration <= nTimeSep2CalcAutocorr
                'Need more memory for rVals calculations'
                return                
            end
        % Initialize the counters
            minIdx = 0;
            maxIdx1 = 0;
            maxIdx2 = 0;
            numLoops = 0;
            
        % Initialize the failed simualtion variables
            fail=0;
        
    % Load these variables into a structure
        inputVars = v2struct();

end