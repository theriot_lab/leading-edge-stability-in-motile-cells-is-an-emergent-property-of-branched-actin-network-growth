function [inputVars] = preAllocateVariables(inputVars)      

%% This function pre-allocates space to save the data directly onto the drive
% (Run as part of setUpAndRun_simulateLamellipodiumAndRunXCorrAnalysis.m)

% This code was written by Rikki M. Garner as a companion to the manuscript
% titled "Leading edge stability in motile cells in an emergent property of
% branched actin network growth", and was last updated 2020/08/19.
%%

        
   % Create a template file to copy
        % Create a file to save the results and transfer to the cloud
        % Pull out the file name
            inputVars.fileNameOutTemp = [sprintf(inputVars.globalInfo.fileNameForSprintf,...
                inputVars.globalInfo.dateNum,inputVars.parameterValsNum,1) '_Temp'];
        % Concatenate to make the file path        
            inputVars.filePathOutTemp = [inputVars.globalInfo.folderPathOnLocal '/' inputVars.fileNameOutTemp];        
        % Save the file
            save(inputVars.filePathOutTemp,'-struct','inputVars','-v7.3')
            
    % Unpack this structure and clear the original structure
        v2struct(inputVars)     
        
        % Create the matfile to save the large datasets   
            matFileObj = matfile(filePathOutTemp,'Writable',true);

        % Pre-allocate space to store the membrane data  
            % Pre-allocate space for the membrane segment kinetics
                % Pre-allocate space for the absolute position of the membrane
                % segment
                    matFileObj.yPosMemInNanometersRecord = nan(numTimeStepsMem2Record,numMemSegments);
                % Pre-allocate space for the absolute velocity of the membrane
                % segment
                    matFileObj.vYPosMemInNanometersRecord = nan(numTimeStepsMem2Record,numMemSegments);
                % Pre-allocate space for the external force on the membrane
                % segment
                    matFileObj.F_stretch_bend_mem_Record = nan(numTimeStepsMem2Record,numMemSegments);
                % Pre-allocate space for the force of the filaments on the membrane
                % segment
                    matFileObj.fActinRecord = nan(numTimeStepsMem2Record,numMemSegments);
            % Pre-allocate space for the filament kinetics
                % Pre-allocate space for the number of filaments    
                    matFileObj.numFilamentsRecord = nan(numTimeStepsMem2Record,numMemSegments);
                    matFileObj.numFilamentsRecord(1,:) = numFilamentsPerMemSegment;
                % Pre-allocate space for the number of capped filaments
                    matFileObj.numCappedFilamentsRecord = nan(numTimeStepsMem2Record,numMemSegments);
                    matFileObj.numCappedFilamentsRecord(1,:) = 0;
                % Pre-allocate space for the mean angle (of uncapped filaments)
                    matFileObj.meanThetaValsRecord = nan(numTimeStepsMem2Record,numMemSegments);
                % Pre-allocate space for the mean angle (of uncapped filaments)
                    matFileObj.meanlfRecord = nan(numTimeStepsMem2Record,numMemSegments);  
                %Preallocate space to store FFT data
                    matFileObj.FFTYPosMemInNanometersRecord = ...
                        nan(numTimePointsMem2Analyze,numWaveModesMem2Analyze,'like',complex(nan,nan));
        % Pre-allocate space to store the filament data
            % Barbed end y-positions (absolute)
                matFileObj.filamentBarbedEndYValsRecord = nan(numTimeStepsFilament2Record,numMemSegments*maxNumFilaments2Sim);
            % Barbed end x-positions
                matFileObj.filamentBarbedEndXValsRecord = nan(numTimeStepsFilament2Record,numMemSegments*maxNumFilaments2Sim);
            % Filament angles
                matFileObj.thetaValsRecord = nan(numTimeStepsFilament2Record,numMemSegments*maxNumFilaments2Sim);
            % Filament length
                matFileObj.lfRecord = nan(numTimeStepsFilament2Record,numMemSegments*maxNumFilaments2Sim);  
                matFileObj.lfRecord(1,1:numFilamentsTotal) = l_f_0;
            % Capped state
                matFileObj.filamentIsCappedRecord = false(numTimeStepsFilament2Record,numMemSegments*maxNumFilaments2Sim);
                matFileObj.filamentIsCappedRecord(1,1:numFilamentsTotal) = filamentIsCapped(filamentExists)';  


end