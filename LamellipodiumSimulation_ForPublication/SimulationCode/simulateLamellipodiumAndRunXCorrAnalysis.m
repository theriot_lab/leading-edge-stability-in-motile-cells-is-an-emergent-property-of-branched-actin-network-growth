function [outputVars] = simulateLamellipodiumAndRunXCorrAnalysis(inputVars)

%% The following code performs a stochastic simulation of dendritic branched
% actin network growth which pushes forward a flexible membrane, 
% based on the simulation parameters specified in inputVars. It then (~line 1172)
% performs autocorrelation analysis on the resulting leading edge shape
% fluctuations. Using the default simulation parameters, this code takes
% ~12hrs to run.
% (Run as part of setUpAndRun_simulateLamellipodiumAndRunXCorrAnalysis.m)

% Inputs: inputVars - a structure containing the simulation parameters 

% Outputs: outputVars - a structure containing the input parameters as well
% as the simulation results. ouputVars is saved directly to the hard drive, 
% using the save file path specified in inputVars, and then the structure 
% is deleted (in the MATLAB workspace) if the save is performed without
% errors.

% This code was written by Rikki M. Garner as a companion to the manuscript
% titled "Leading edge stability in motile cells in an emergent property of
% branched actin network growth", and was last updated 2020/08/19

%%

% Create an empty error message
    ME=[];

% Try to run the function, otherwise print the error message
try

    % Unpack the variables
        v2struct(inputVars)
        clear inputVars
    % Reset the random number generator seed
        rng('shuffle')
    % Choose the digits for VPA calculations
        digits(8);
        d1=digits;

    % Create the matfile to save the large datasets   
        matFileObj = matfile(filePathOutOnCloud,'Writable',true);

    % Initialize large (too large to transfer to a remote server quickly) or 
    % random (need a new random number generator seed for each sim) variables
        % Calculate the time indices to record the membrane position
            idx2RecordMem = 1:numTimeStepsMem2Skip:numTimeSteps2Sim;
            % Calculate the times at these recordings
                times2RecordInMillisecondsMem = (idx2RecordMem-1)*dtRelaxMemInMilliseconds;
        % Choose the initial y-positions of the membrane segments        
            yPosMemInNanometers = 10^(-6)*rand([1 numMemSegments]); 
            % Record this membrane position in the file"
                matFileObj.yPosMemInNanometersRecord(1,1:numMemSegments) = yPosMemInNanometers;
        % Choose the x-positions of the initial filament tips
            % Choose tip positions in an interval around the membrane segment, with a
            % closed end to the left, and an open end to the right
                tipXPosInitial = bsxfun(@plus,...
                    xSepMemSegmentsInNanometers*(0.99*rand(numFilamentsPerMemSegmentInitial,numMemSegments)),...
                    xPosMemInNanometers-(xSepMemSegmentsInNanometers/2));
                % Record this in the larger matrix and the file  
                    tipXPos = nan(maxNumFilaments2Sim,numMemSegments);
                    tipXPos(1:numFilamentsPerMemSegmentInitial,:) = tipXPosInitial;
                    matFileObj.filamentBarbedEndXValsRecord(1,1:numFilamentsTotal) = tipXPos(filamentExists)';
            % Calculate the membrane segment index for these filaments
                memSegIdxInitial = ceil(tipXPosInitial/xSepMemSegmentsInNanometers);
                % Record this in the larger matrix and the file  
                    memSegIdx = nan(maxNumFilaments2Sim,numMemSegments);
                    memSegIdx(1:numFilamentsPerMemSegmentInitial,:) = memSegIdxInitial;
                % Create a matrix where the entry for each element is
                % the column number it is in
                    columnNumbers = repmat(1:numMemSegments,[maxNumFilaments2Sim,1]);
                % Check that the indices for each membrane segment
                % were calculated correctly
                    if sum(memSegIdx(:)==columnNumbers(:)) ~= sum(~isnan(tipXPosInitial(:)))
                        ME = 'Membrane segment indices were not calculated correctly';
                        fail=1;
                        return
                    end
        % Record the initial y-positions of the filament tips
            % Initialize the equilibrium filament tip positions relative to the
            % membrane segment and update the file  
                tipYPosRelMem = nan(maxNumFilaments2Sim,numMemSegments);
                tipYPosRelMem(1:numFilamentsPerMemSegmentInitial,:) = tipYPosRelMemInitial;
                matFileObj.filamentBarbedEndYValsRecord(1,1:numFilamentsTotal) = ...
                    yPosMemInNanometers(memSegIdx(filamentExists)) - tipYPosRelMem(filamentExists)';
        % Choose filament initial angles
            % Pre-allocate space to store the filament angles
                thetaVals = nan(maxNumFilaments2Sim,numMemSegments);
            % Choose the filament angles in radians
                if initialFilamentAngleOrder==0
                    % Give a random initial branching angle between
                    % -pi/2 to pi/2
                        thetaVals(1:numFilamentsPerMemSegmentInitial,1:numMemSegments) = ...
                            pi*rand(numFilamentsPerMemSegmentInitial,numMemSegments) - (pi/2);       
                elseif initialFilamentAngleOrder==1   
                    % For a fixed initial filament angle, have it be
                    % random whether it is to the left or the right of
                    % the membrane segment normal
                        thetaVals(1:numFilamentsPerMemSegmentInitial,1:numMemSegments) = initialFilamentAngle*...
                            (((randi(2,[numFilamentsPerMemSegmentInitial,numMemSegments])-1)*2)-1); 
                end
            % Store these initial filament angles in the file
                matFileObj.thetaValsRecord(1,1:numFilamentsTotal) = thetaVals(filamentExists)';
        % Update the filament properties
            % Filament length
                l_f = nan(maxNumFilaments2Sim,numMemSegments);
                l_f(1:numFilamentsPerMemSegmentInitial,:) = l_f_0; 
            % Length of filament added by a monomer in the direction of
            % the membrane segment normal
                deltaVals = l_m*cos(thetaVals);
                deltaValsXDim = l_m*sin(thetaVals);    
            % Effective bending constant perpendicular to the
            % membrane in pN nm^(-1)
                kappaValsPerp = (3*l_p*kT)./(l_f.^3);
                kappaValsPar = (16*l_p^2*kT)./(l_f.^4);
                kappaVals = (kappaValsPerp.*kappaValsPar)...
                    ./((kappaValsPerp.*(cos(thetaVals)).^2)+(kappaValsPar.*(sin(thetaVals)).^2));             
        % Create a random number for all membrane segments for all time points   
            brownianForce = xi*normrnd(0,1,[numTimeStepsMem2CalcBF, numMemSegments]);  

    % Pre-allocate space for the temporary variables 
    % (hold only a certain amount of data in memory at a time)
        % Pre-allocate space for the membrane segment kinetics
            % Time index vector
                timeIdx2SaveIntoMem = false([numTimeStepsMem2Record,1]);
                timeIdx2SaveFromMem = false([numTimeStepsMem2KeepInWorkspace,1]);
            % Pre-allocate space for the absolute position of the membrane
            % segment
                yPosMemInNanometersRecordTemp = nan(numTimeStepsMem2KeepInWorkspace,numMemSegments);
            % Pre-allocate space for the absolute velocity of the membrane
            % segment
                vYPosMemInNanometersRecordTemp = nan(numTimeStepsMem2KeepInWorkspace,numMemSegments);
            % Pre-allocate space for the external force on the membrane
            % segment
                F_stretch_bend_mem_RecordTemp = nan(numTimeStepsMem2KeepInWorkspace,numMemSegments);
            % Pre-allocate space for the force of the filaments on the membrane
            % segment
                fActinRecordTemp = nan(numTimeStepsMem2KeepInWorkspace,numMemSegments);
        % Pre-allocate space for the filament kinetics
            % Pre-allocate space for the number of filaments    
                numFilamentsRecordTemp = nan(numTimeStepsMem2KeepInWorkspace,numMemSegments);
            % Pre-allocate space for the number of capped filaments
                numCappedFilamentsRecordTemp = nan(numTimeStepsMem2KeepInWorkspace,numMemSegments);
            % Pre-allocate space for the mean angle (of uncapped filaments)
                meanThetaValsRecordTemp = nan(numTimeStepsMem2KeepInWorkspace,numMemSegments);
                % Save the mean angle (of uncapped filaments)
                    thetaVals2Avg = nan(maxNumFilaments2Sim,numMemSegments);
            % Pre-allocate space for the mean angle (of uncapped filaments)
                meanlfRecordTemp = nan(numTimeStepsMem2KeepInWorkspace,numMemSegments);
                % Save the mean angle (of uncapped filaments)
                    lf2Avg = nan(maxNumFilaments2Sim,numMemSegments);
        % Pre-allocate space to store the temporary variables
            % Time index vector
                timeIdx2SaveIntoFil = false([numTimeStepsFilament2Record,1]);
                timeIdx2SaveFromFil = false([numTimeStepsFilament2KeepInWorkspace,1]);
                filIdx2SaveFil = false([1 (numMemSegments*maxNumFilaments2Sim)]);
            % Barbed end y-positions (absolute)
                filamentBarbedEndYValsRecordTemp = nan(numTimeStepsFilament2KeepInWorkspace,numMemSegments*maxNumFilaments2Sim);
            % Barbed end x-positions
                filamentBarbedEndXValsRecordTemp = nan(numTimeStepsFilament2KeepInWorkspace,numMemSegments*maxNumFilaments2Sim);
            % Filament angles
                thetaValsRecordTemp = nan(numTimeStepsFilament2KeepInWorkspace,numMemSegments*maxNumFilaments2Sim);
            % Filament length
                lfRecordTemp = nan(numTimeStepsFilament2KeepInWorkspace,numMemSegments*maxNumFilaments2Sim);  
            % Capped state
                filamentIsCappedRecordTemp = false(numTimeStepsFilament2KeepInWorkspace,numMemSegments*maxNumFilaments2Sim);
        % Pre-allocate space for force calculations
            % Membrane 
                % Membrane position
                    yPosMemRK = nan([1,numMemSegments]);
                % Change in membrane position
                    dyPosMemRK = nan([1,numMemSegments]);
                % Second derivative of membrane position
                    ddy_xx = nan([1,numMemSegments]);
                % Fourth derivative of membrane position
                    ddddy_xxxx = nan([1,numMemSegments]);
                % Stretch-bend force
                    F_stretch_bend_mem = nan([1,numMemSegments]);
                % Velocity of the membrane segments
                    vY = nan([1,numMemSegments]);
                % RK storage matrix
                    slopeValsNormY = nan([4,numMemSegments]);   
                % Change in membrane position
                    dYPosMemInNanometers = nan([1,numMemSegments]);
                % Force of actin on the membrane
                    % Force from each filament
                        fActin = nan(maxNumFilaments2Sim,numMemSegments);
                    % Position of the filament relative to the membrane
                        y0ValsRK = nan([maxNumFilaments2Sim,numMemSegments]);
                    % Prefactor of the denominator of the force equation
                        denomPrefactor = nan([maxNumFilaments2Sim,numMemSegments]);
                    % Prefactor of the erf argument
                        erfMult = nan([maxNumFilaments2Sim,numMemSegments]);
                    % Erf argument
                        erfArg = nan([maxNumFilaments2Sim,numMemSegments]);
                    % Erf evulated
                        erfValDenom = nan([maxNumFilaments2Sim,numMemSegments]);
                    % Etc...
                        calculateDenom = nan([maxNumFilaments2Sim,numMemSegments]);               
                        erfArg2 = zeros(maxNumFilaments2Sim,numMemSegments);
                        erfArg2LTZ = false(maxNumFilaments2Sim,numMemSegments);
                        denomPrefactor2 = zeros(maxNumFilaments2Sim,numMemSegments);
                        erfValDenom2 = vpa(zeros(maxNumFilaments2Sim,numMemSegments));
                        calculateDenom2 = vpa(zeros(maxNumFilaments2Sim,numMemSegments));                
                        fActinNew = nan([maxNumFilaments2Sim,numMemSegments]);
                        fActinMax = nan([maxNumFilaments2Sim,numMemSegments]);
                        num2VPA = false([maxNumFilaments2Sim,numMemSegments]);
                        fActinGreaterThanMax = false([maxNumFilaments2Sim,numMemSegments]);
                        sumFActin = nan([1,numMemSegments]);
                        membHasFilsOverMax = false([1,numMemSegments]);
               
        % Actin network growth processes
        
            % Pre-allocate a matrix to store whether filaments performed each 
            % action in any particular timestep 
                filamentAddedMonomer = false([maxNumFilaments2Sim,numMemSegments]);
                filamentRemovedMonomer = false([maxNumFilaments2Sim,numMemSegments]);
                filamentAddedOrRemovedMonomer = false([maxNumFilaments2Sim,numMemSegments]);
                filamentCapped = false([maxNumFilaments2Sim,numMemSegments]);
                filamentBranched = false([maxNumFilaments2Sim,numMemSegments]);
                filamentRemovedFromSimulation = false([maxNumFilaments2Sim,numMemSegments]);
            % Pre-allocate space to store the random numbers for each process
                randAddMonomer = nan([maxNumFilaments2Sim numMemSegments]);
                randRemoveMonomer = nan([maxNumFilaments2Sim numMemSegments]);
                randCap = nan([maxNumFilaments2Sim numMemSegments]);
                randBranch = nan([maxNumFilaments2Sim numMemSegments]);            
            % Rates of each reaction
                % Polymerization
                    % Probability of adding the monomer (given proximity to
                    % membrane)
                        p_on_reduction_by_membrane = nan(maxNumFilaments2Sim,numMemSegments);
                        erfArgDenom = nan([maxNumFilaments2Sim,numMemSegments]);
                        erfArgNumer = nan([maxNumFilaments2Sim,numMemSegments]);
                        erfValNumerP = nan([maxNumFilaments2Sim,numMemSegments]);
                        pAddNew = nan([maxNumFilaments2Sim,numMemSegments]);
                        erfArgDenom2 = zeros(maxNumFilaments2Sim,numMemSegments);
                        erfArgNumer2 = zeros(maxNumFilaments2Sim,numMemSegments);
                        erfArgDenom2LTZ = false([maxNumFilaments2Sim,numMemSegments]);   
                        erfValNumerP2 = vpa(zeros(maxNumFilaments2Sim,numMemSegments));  
                        erfArgNumer2LTZ = false([maxNumFilaments2Sim,numMemSegments]);    
                        pAddNew2 = nan([maxNumFilaments2Sim,numMemSegments]);
                        num2VPAAndInf = false([maxNumFilaments2Sim,numMemSegments]); 
                    % The polymerization rate far from the membrane
                        r_on_0 = k_on*M;
                    % The filament-specific polymerization rate and probability     
                        r_on = nan([maxNumFilaments2Sim,numMemSegments]); 
                        p_on = nan([maxNumFilaments2Sim,numMemSegments]);
                % Depolymerization
                    % The depolymerization rate
                        r_off = k_off;
                    % Calculate the probability of removing a monomer each timestep
                        p_off = 1-exp(-r_off*dtRelaxMemInMilliseconds);
                % Branching    
                    % The branching rate per mother filament length
                        k_branch_0 = k_branch*M;
                    % The length of teh filament in the branching window
                        lengthOfFilamentInBranchZone = nan([maxNumFilaments2Sim,numMemSegments]);
                    % The filament-specific branching rate and probability                        
                        r_branch = nan([maxNumFilaments2Sim numMemSegments]);
                        p_branch = nan([maxNumFilaments2Sim numMemSegments]); 
                    % Space to store the new branch properties
                        newThetaVals= nan([maxNumFilaments2Sim,numMemSegments]);
                        newXPosVals = nan([maxNumFilaments2Sim,numMemSegments]);
                        newMemSegmentIdx = nan([maxNumFilaments2Sim,numMemSegments]);
                        newYPosVals = nan([maxNumFilaments2Sim,numMemSegments]);
                        newFilamentLinearIndexesBranch = nan([maxNumFilaments2Sim,numMemSegments]);  
                        % Count how many new branches are in each column    
                            numNewBranchesInEachColumn = nan([1,numMemSegments]);              
                % Capping                 
                    % Choose the off rate
                        r_cap = k_cap;        
                    % Calculate the probability of capping each timestep
                        p_cap = 1-exp(-r_cap*dtRelaxMemInMilliseconds);  
                % Removing filaments
                    fActinSorted = nan([maxNumFilaments2Sim,numMemSegments]);
                    fActinTotalFrac = nan([maxNumFilaments2Sim,numMemSegments]);        
                    fActinMin = nan([1 numMemSegments]);
                    fActinLessThanMin = false([maxNumFilaments2Sim,numMemSegments]);
                    
            % Determine which filaments both exist and are uncapped
                filamentExistsAndIsUncapped = false(maxNumFilaments2Sim,numMemSegments);
                filamentExistsAndIsUncapped(:) = ((~filamentIsCapped)&filamentExists);
                % Determine the number of existing and uncapped filaments
                    numExistingAndUncappedFilaments = sum(filamentExistsAndIsUncapped(:));       
   
            % Implementing periodic BCs
                tipTooFarRight = false([maxNumFilaments2Sim,numMemSegments]);
                tipTooFarLeft = false([maxNumFilaments2Sim,numMemSegments]);
                memSegmentChangeIdx = false([maxNumFilaments2Sim,numMemSegments]); 
                newFilamentLinearIndexes = nan([maxNumFilaments2Sim,numMemSegments]);
                % Count how many new filaments are in each column    
                    numNewFilsInEachColumn = nan([1,numMemSegments]);



	% Run the simulation
    for numTimeStep = 1:numTimeSteps2Sim

        % Update the membrane position

            % Run the Runge-Kutta lgorithm

                % Reset the Runge-Kutta parameters
                    slopeValsY(:) = nan;

                % Calculate the first RK parameter

                    % Initialize the RK y-position and filament distances
                        yPosMemRK(:) = yPosMemInNanometers;
                        y0ValsRK(:) = tipYPosRelMem;

                    % Calculate the membrane bend-stretch forces
                    if applyMembraneBendStretchForces
                        % Calculate the 2nd spatial derivative
                            % Create a matrix of shifted membrane segment y-positions
                                % Copy the membrane segment y-positions for each shift you
                                % need to calculate
                                      yVals_ddy_xx(:) = yPosMemRK(shift2ndDerIdx);
                            % Multiply the shifted membrane segment y-positions by their
                            % coefficients, sum over all shifts, and divide by the membrane segment separation to determine
                            % the derivative
                                ddy_xx(:) = sum(yVals_ddy_xx.*finiteDiffCoeffs2ndDerCentral)./xSepMemSegmentsInNanometers^2;

                        % Calculate the 4th spatial derivative
                            % Create a matrix of shifted membrane segment y-positions
                                % Copy the membrane segment y-positions for each shift you
                                % need to calculate 
                                      yVals_ddddy_xxxx(:) = yPosMemRK(shift4thDerIdx);
                            % Multiply the shifted membrane segment y-positions by their
                            % coefficients, sum over all shifts, and divide by the membrane segment separation to determine
                            % the derivative
                                ddddy_xxxx(:) = sum(yVals_ddddy_xxxx.*finiteDiffCoeffs4thDerCentral)./xSepMemSegmentsInNanometers^4;

                         % Calculate the membrane force
                            F_stretch_bend_mem(:) = (sigmaVal*ddy_xx) - (kappaVal*ddddy_xxxx);
                    else
                            F_stretch_bend_mem(:) = 0;
                    end

                   % Calculate the force of actin 

                        % Pull out the equation's arguments
                            denomPrefactor(:) = sqrt(pi*kT./(2*kappaVals));
                            erfMult(:) = sqrt(pi/4)./denomPrefactor;
                            erfArg(:) = tipYPosRelMem.*erfMult;

                        % Calculate the force of actin

                            % Find positive values of y0Vals, where the erf is far from -1,
                            % calculate erf directly
                            % For negative values of y0Vals, where erf ~ 1 (and 1+erf ~ 0)
                            % calculate the erf instead such that 1+erf(y0) = 1-erf(abs(y0)) =
                            % erfc(|y0|)
                                erfValDenom(:) = 1+erf(erfArg);
                                erfValDenom(erfArg<=0) = erfc(abs(erfArg(erfArg<=0)));
                            % Add it all up
                                calculateDenom(:) = denomPrefactor.*erfValDenom;
                            % Calculate the force of each filament on the membrane
                                fActinNew(:) = kT.*exp(-erfArg.^2)./calculateDenom;
                            % Filaments growing away from the membrane don't
                            % apply any force
                                fActinNew(deltaVals<0)=0;

                            % Follow up with vpa for when this
                            % calculation fails (usualy when the force
                            % is either very large or very small)
                                % Pull out the bad  calculations
                                    num2VPA(:) = (((isinf(fActinNew))|(isnan(fActinNew)))&filamentExists); 
                                % If any exist, correct with VPA
                                if any(num2VPA(:))
                                    % Subset the data
                                        erfArg2(num2VPA) = erfArg(num2VPA);
                                        denomPrefactor2(num2VPA) =  denomPrefactor(num2VPA);
                                    % For negative values of y0Vals, where erf ~ -1 (and 1+erf ~ 0)
                                    % calculate the erf instead such that 1+erf(y0) = 1-erf(abs(y0)) =
                                    % erfc(y0)
                                        erfValDenom2(num2VPA) = 1+erf(vpa(erfArg2(num2VPA)));
                                        erfArg2LTZ(:) = (num2VPA&(erfArg2<=0));
                                        erfValDenom2(erfArg2LTZ) = erfc(vpa(abs(erfArg2(erfArg2LTZ))));
                                    % Add it all up
                                        calculateDenom2(num2VPA) = denomPrefactor2(num2VPA).*erfValDenom2(num2VPA);
                                    % Calculate the actin signal
                                        fActinNew(num2VPA) = kT.*exp(-vpa(erfArg2(num2VPA).^2))./calculateDenom2(num2VPA);  
                                end

                           % Add these forces to the fActin vector
                                fActin(:)=0;
                                fActin(filamentExists) = fActinNew(filamentExists); 
                           % Sum over all of the filament for each membrane segment
                           % to determine the total force of the filaments on
                           % each membrane segment
                                sumFActin(:) = sum(fActin,1);

                          % For the filaments whose forces couldn't be resolved 
                          % with this large of a  timestep, have only the most forceful filament
                          % push the membrane just past the filament tip
                                % Crate a maximum force per filament for
                                % filaments that are close to or poking the
                                % membrane, such that these filaments could 
                                % maximally push the membrane up to l_m/10 away
                                % from the filament tip in a single timestep. 
                                % For all others, make the max force infinite                                    
                                    fActinMax(:) = Inf*ones(size(fActinNew));
                                    fActinMax(tipYPosRelMem<(l_m/10)) = abs((tipYPosRelMem(tipYPosRelMem<(l_m/10)) - (l_m/10))*gammaVal/dtRelaxMemInMilliseconds);
                                % Pull out the filaments which exceed the
                                % maximum 
                                    fActinGreaterThanMax(:) = ((fActinNew>fActinMax)&filamentExists);
                                if any(fActinGreaterThanMax(:))
                                    % Set these filaments to thir maximum
                                    % force
                                    fActin(fActinGreaterThanMax)=fActinMax(fActinGreaterThanMax);
                                    % Only use the maximum force for
                                    % sumFactin for these segments
                                    membHasFilsOverMax(:) = any(fActinGreaterThanMax);
                                    sumFActin(membHasFilsOverMax) = max(fActin(:,membHasFilsOverMax),[],1);
                                end

                    % Calculate the velocity of each membrane segment
                        vY(:) = (1/gammaVal)*(Fext + F_stretch_bend_mem + sumFActin + brownianForce(numTimeStepThisIter_CalcBF,:)); 

                    % If any are nans exit the sim
                        if any((isnan(vY)|isinf(vY))) 
                            ME = 'velocity calculation failed';
                            fail=1;
                            break
                        end

                        % Record this cvelocity in the RK coefficient matrix
                            slopeValsY(1,:) = vY;                 


                % Run though the other RK-steps
                for RKNum = 2:4

                    % Calculate the new y positions for this Runga-Kutta step
                        dyPosMemRK(:) = dtRK(RKNum).*slopeValsY(RKNum-1,:);
                        yPosMemRK(:) = yPosMemInNanometers + dyPosMemRK;
                    % Pull out the new equillibrium position
                        y0ValsRK(filamentExists) = tipYPosRelMem(filamentExists) + dyPosMemRK(memSegIdx((filamentExists)))';
                        erfArg(:) = y0ValsRK.*erfMult;

                    % Calculate the memrbane bend-stretch forces

                    if applyMembraneBendStretchForces
                        % Calculate the 2nd spatial derivative
                            % Create a matrix of shifted membrane segment y-positions
                                % Copy the membrane segment y-positions for each shift you
                                % need to calculate
                                      yVals_ddy_xx(:) = yPosMemRK(shift2ndDerIdx);
                            % Multiply the shifted membrane segment y-positions by their
                            % coefficients, sum over all shifts, and divide by the membrane segment separation to determine
                            % the derivative
                                ddy_xx(:) = sum(yVals_ddy_xx.*finiteDiffCoeffs2ndDerCentral)./xSepMemSegmentsInNanometers^2;

                        % Calculate the 4th spatial derivative
                            % Create a matrix of shifted membrane segment y-positions
                                % Copy the membrane segment y-positions for each shift you
                                % need to calculate 
                                      yVals_ddddy_xxxx(:) = yPosMemRK(shift4thDerIdx);
                            % Multiply the shifted membrane segment y-positions by their
                            % coefficients, sum over all shifts, and divide by the membrane segment separation to determine
                            % the derivative
                                ddddy_xxxx(:) = sum(yVals_ddddy_xxxx.*finiteDiffCoeffs4thDerCentral)./xSepMemSegmentsInNanometers^4;

                         % Calculate the membrane force
                            F_stretch_bend_mem(:) = (sigmaVal*ddy_xx) - (kappaVal*ddddy_xxxx);
                    else
                            F_stretch_bend_mem(:) = 0;
                    end

                    % Calculate the force of actin

                        % Find positive values of y0Vals, where the erf is far from -1,
                        % calculate erf directly
                        % For negative values of y0Vals, where erf ~ 1 (and 1+erf ~ 0)
                        % calculate the erf instead such that 1+erf(y0) = 1-erf(abs(y0)) =
                        % erfc(|y0|)
                            erfValDenom(:) = 1+erf(erfArg);
                            erfValDenom(erfArg<=0) = erfc(abs(erfArg(erfArg<=0)));
                        % Add it all up
                            calculateDenom(:) = denomPrefactor.*erfValDenom;
                        % Calculate the force of each filament on the membrane
                            fActinNew(:) = kT.*exp(-erfArg.^2)./calculateDenom;
                        % Filaments growing away from the membrane don't
                        % apply any force
                            fActinNew(deltaVals<0)=0;

                        % Follow up with vpa for bad ones
                            % Pull out the bad  calculations
                                num2VPA(:) = (((isinf(fActinNew))|(isnan(fActinNew)))&filamentExists); 
                            % If any exist, correct with VPA
                            if any(num2VPA(:))                                        
                                % Subset the data
                                    erfArg2(num2VPA) = erfArg(num2VPA);
                                    denomPrefactor2(num2VPA) =  denomPrefactor(num2VPA);
                                % For negative values of y0Vals, where erf ~ -1 (and 1+erf ~ 0)
                                % calculate the erf instead such that 1+erf(y0) = 1-erf(abs(y0)) =
                                % erfc(y0)
                                    erfValDenom2(num2VPA) = 1+erf(vpa(erfArg2(num2VPA)));
                                    erfArg2LTZ(:) = (num2VPA&(erfArg2<=0));
                                    erfValDenom2(erfArg2LTZ) = erfc(vpa(abs(erfArg2(erfArg2LTZ))));
                                % Add it all up
                                    calculateDenom2(num2VPA) = denomPrefactor2(num2VPA).*erfValDenom2(num2VPA);
                                % Calculate the actin signal
                                    fActinNew(num2VPA) = kT.*exp(-vpa(erfArg2(num2VPA).^2))./calculateDenom2(num2VPA);

                            end

                       % Add these forces to the fActin vector
                            fActin(:)=0;
                            fActin(filamentExists) = fActinNew(filamentExists); 
                       % Sum over all of the filament for each membrane segment
                       % to determine the total force of the filaments on
                       % each membrane segment
                            sumFActin(:) = sum(fActin,1);

                      % For the filaments whose forces couldn't be resolved 
                      % with this large of a  timestep, have only the most forceful filament
                      % push the membrane just past the filament tip
                            % Pull out the filaments which exceed the
                            % maximum 
                                fActinGreaterThanMax(:) = ((fActinNew>fActinMax)&filamentExists);
                            if any(fActinGreaterThanMax(:))
                                % Add these new forces to the fActin matrix
                                fActin(fActinGreaterThanMax) = fActinMax(fActinGreaterThanMax);
                                % Only use the maximum force for
                                % sumFactin for these segments
                                membHasFilsOverMax(:) = any(fActinGreaterThanMax);
                                sumFActin(membHasFilsOverMax) = max(fActin(:,membHasFilsOverMax),[],1);
                            end

                    % Calculate the velocity of each membrane segment
                        vY(:) = (1/gammaVal)*(Fext + F_stretch_bend_mem + sumFActin + brownianForce(numTimeStepThisIter_CalcBF,:));        

                    % If any are nans exit the sim
                        if any((isnan(vY)|isinf(vY))) 
                            ME = 'velocity calculation failed';
                            fail=1;
                            break
                        end

                    % Record this cvelocity in the RK coefficient matrix
                        slopeValsY(RKNum,:) = vY;   

                end

                % If any are any problesm exit the sim
                    if fail==1 
                        break
                    end

                % Update the membrane position and actin concentration
                    slopeValsNormY(:) = RKMultiplier.*slopeValsY;   
                    dYPosMemInNanometers(:) = sum((dtRelaxMemInMilliseconds/6).*(slopeValsNormY));
                    yPosMemInNanometers(:) = yPosMemInNanometers + dYPosMemInNanometers;

                % Update the filament tip positions relative to the membrane segment
                    tipYPosRelMem(filamentExists) = tipYPosRelMem(filamentExists) + dYPosMemInNanometers(memSegIdx(filamentExists))';       


        % Calculate the associated probability of adding a monomer

            % Pull out the data only for existing
            % filaments
                erfArgDenom(:) = tipYPosRelMem.*erfMult;
                erfArgNumer(:) = (tipYPosRelMem-deltaVals).*erfMult;

            % Calculate the denominator of the force and monomer addition probability
                % Find positive values of y0Vals, where the erf is far from -1,
                % calculate erf directly
                % For negative values of y0Vals, where erf ~ 1 (and 1+erf ~ 0)
                % calculate the erf instead such that 1+erf(y0) = 1-erf(abs(y0)) =
                % erfc(y0)
                    erfValDenom(:) = 1+erf(erfArgDenom);
                    erfValDenom(erfArgDenom<=0) = erfc(abs(erfArgDenom(erfArgDenom<=0)));

           % Calculate the numerator of the monomer addition probability 
                % Find positive values of y0Vals, where the erf is far from -1,
                % calculate erf directly
                % For negative values of y0Vals, where erf ~ 1 (and 1+erf ~ 0)
                % calculate the erf instead such that 1+erf(y0) = 1-erf(abs(y0)) =
                % erfc(y0)
                    erfValNumerP(:) = 1+erf(erfArgNumer);
                    erfValNumerP(erfArgNumer<=0) = erfc(abs(erfArgNumer(erfArgNumer<=0)));

            % Calculate the probabiltiy of adding a monomer
                pAddNew(:) = erfValNumerP./erfValDenom;
            % Filaments growing away from the membrane aren't impeded
                pAddNew(deltaVals<0)=1;

                % Follow up with vpa for bad ones
                    % Pull out the bad  calculations
                        num2VPA(:) = ((isnan(pAddNew)|isinf(pAddNew)|(pAddNew>1))&filamentExists); 

                    % If any exist, correct with VPA
                    if any(num2VPA(:))              

                        % Subset the data
                            erfArgDenom2(num2VPA) = erfArgDenom(num2VPA);
                            erfArgNumer2(num2VPA) = erfArgNumer(num2VPA);

                        % Calculate the denomenator
                            % Find positive values of y0Vals, where the erf is far from -1,
                            % calculate erf directly
                            % For negative values of y0Vals, where erf ~ 1 (and 1+erf ~ 0)
                            % calculate the erf instead such that 1+erf(y0) = 1-erf(abs(y0)) =
                            % erfc(y0)
                                erfValDenom2(num2VPA) = 1+erf(vpa(erfArgDenom2(num2VPA)));
                                erfArgDenom2LTZ(:) = (num2VPA&(erfArgDenom2<=0));                                        
                                erfValDenom2(erfArgDenom2LTZ) = erfc(vpa(abs(erfArgDenom2(erfArgDenom2LTZ))));

                        % Calculate the numerator
                            % Find positive values of y0Vals, where the erf is far from -1,
                            % calculate erf directly
                            % For negative values of y0Vals, where erf ~ 1 (and 1+erf ~ 0)
                            % calculate the erf instead such that 1+erf(y0) = 1-erf(abs(y0)) =
                            % erfc(y0)
                                erfValNumerP2(num2VPA) = 1+erf(vpa(erfArgNumer2(num2VPA)));
                                erfArgNumer2LTZ(:) = (num2VPA&(erfArgNumer2<=0));             
                                erfValNumerP2(erfArgNumer2LTZ) = erfc(vpa(abs(erfArgNumer2(erfArgNumer2LTZ))));

                        % Calculate the probabiltiy of adding a monomer
                            pAddNew2(num2VPA) = erfValNumerP2(num2VPA)./erfValDenom2(num2VPA);
                            num2VPAAndInf(:) = (num2VPA&(isinf(pAddNew2)));
                            pAddNew2(num2VPAAndInf) = 0;
                            pAddNew(num2VPA) = pAddNew2(num2VPA);

                    end


            % If any are nans exit the sim
                if any((isnan(pAddNew(filamentExists))|isinf(pAddNew(filamentExists))|(pAddNew(filamentExists)>1))) 
                    ME = 'pAdd calculation failed';
                    fail=1;
                    break
                end

            % Add the probability of adding a monomer to the pAdd matrix 
                p_on_reduction_by_membrane(:) = 0;
                p_on_reduction_by_membrane(filamentExists) = pAddNew(filamentExists);  

        % Determine whether filaments polymerize, depolymerize, cap, and branch during this timestep 
        
            % Choose whether each filament adds a monomer
                % Calculate a random number for each existing and uncapped
                % filament
                    randAddMonomer(filamentExistsAndIsUncapped) = rand([1,numExistingAndUncappedFilaments]);
                % Compute the rate of monomer addition rate given this probability
                % of adding a monomer and the on rate
                    r_on(filamentExistsAndIsUncapped) = r_on_0.*p_on_reduction_by_membrane(filamentExistsAndIsUncapped);
                % Calculate the probability of adding a monomer this timestep
                    p_on(filamentExistsAndIsUncapped) = 1-exp(-r_on(filamentExistsAndIsUncapped)*dtRelaxMemInMilliseconds);
                % Clear the filamentAddedMonomer variable from last time
                    filamentAddedMonomer(:) = false;
                % Determine which filaments remove a monomer  
                    filamentAddedMonomer(filamentExistsAndIsUncapped) = randAddMonomer(filamentExistsAndIsUncapped)<p_on(filamentExistsAndIsUncapped);

            % Choose whether each filament removes a monomer
                % Calculate a random number for each existing and uncapped
                % filament
                    randRemoveMonomer(filamentExistsAndIsUncapped) = rand([1,numExistingAndUncappedFilaments]);
                % Clear the filamentRemovedMonomer variable from last time
                    filamentRemovedMonomer(:) = false;
                % Determine which filaments remove a monomer    
                    filamentRemovedMonomer(filamentExistsAndIsUncapped) = randRemoveMonomer(filamentExistsAndIsUncapped)<p_off;            

            % Choose whether each filament caps
                % Calculate a random number for each existing and uncapped
                % filaments
                    randCap(filamentExistsAndIsUncapped) = rand([1,numExistingAndUncappedFilaments]);
                % Clear the filamentCapped variable from last time
                    filamentCapped(:) = false;
                % Determine which filaments cap    
                    filamentCapped(filamentExistsAndIsUncapped) = randCap(filamentExistsAndIsUncapped)<p_cap;

            % Choose whether each filament branches
                % Calculate a random number for each existing filament
                    randBranch(filamentExists) = rand([1,numFilamentsTotal]);
                % Determine the branch rate
                    % Branch based on distance to the surface
                        % Determine the length of the filament available
                        % for branching (If the filament is growing towards the leading edge, then anything )
                            lengthOfFilamentInBranchZone(:) = min(l_f,(branchWindowSize-tipYPosRelMem)./cos(thetaVals)).*(cos(thetaVals)>0) + ...
                                l_f.*(cos(thetaVals)<=0);
                            lengthOfFilamentInBranchZone(:) = lengthOfFilamentInBranchZone.*(tipYPosRelMem<branchWindowSize);
                        % Calculate the branching rate
                            r_branch(:) = k_branch_0.*lengthOfFilamentInBranchZone.*(1-sigmf(tipYPosRelMem,[branchWindowSwitchStrengthVal branchWindowSize]));
                            % Correct for nan if switching strength is infinite
                                r_branch(isnan(r_branch)) = ...
                                    k_branch_0.*lengthOfFilamentInBranchZone(isnan(r_branch))/2;
                % Calculate the probability of branching
                    p_branch(filamentExists) = 1 - exp(-r_branch(filamentExists).*dtRelaxMemInMilliseconds);
                % Clear the filamentCapped variable from last time
                    filamentBranched(:) = false;
                % Determine which filaments branch
                    filamentBranched(filamentExists) = ...
                        randBranch(filamentExists)<p_branch(filamentExists);  
                    
            % Choose which filaments to remove from the simulation
                % For each membrane segment, remove all filaments which
                % cumulatively produce less than 0.1% of the total filament
                % force on the membrane segment
                    % Sort the actin forces in descending order
                        fActinSorted(:) = sort(fActin,1,'descend');
                    % Calculate the cumulative fraction each filament contributes to the
                    % total force of the filaments on the membrane segment
                        fActinTotalFrac(:) = 1-cumsum(fActinSorted,1,'omitnan')./max(cumsum(fActinSorted,1,'omitnan'));
                    % Set 0-force entries equal to nan
                        fActinTotalFrac(fActinTotalFrac==0) = nan;
                    % Determine whether any filaments will be depolymerized
                    if any((fActinTotalFrac(:)<10^(-3))&(filamentIsCapped(:)))   
                        % Remove all forces above the 10^(-3) threshold
                            fActinSorted((fActinTotalFrac>=10^(-3))|(isnan(fActinTotalFrac)))=0;
                        % Determine the maximum force below this threshold
                            fActinMin(:) = max(fActinSorted,[],1);                       
                        % Determine which filaments lie below this
                        % threshold
                            fActinLessThanMin(:) = bsxfun(@le, fActin, fActinMin);             
                        % Find the capped filaments producing too small of a force
                        % (or have a negative length) and mark them for removal
                            filamentRemovedFromSimulation(:) = ((fActinLessThanMin&filamentIsCapped)|(l_f<l_m)); 
                   else
                       filamentRemovedFromSimulation(:) = (l_f<l_m);                         
                   end      
                
        % Update the actin network accordingly        
  
            % Branch the filaments (Do this first, bcause the new branch
            % position depends on the length of the filaments)
            if any(filamentBranched(:))
                % Determine the properties of the new filaments    
                    % Add the branch angle of the newly branched filaments
                        % Pull out the new branch angle (random side of
                        % filament)
                            newThetaVals(filamentBranched) = thetaVals(filamentBranched) + ...
                                branchAngle*((randi(2,sum(filamentBranched(:)),1)-1)*2 - 1) + ...
                                branchAngleDeviation*randn(sum(filamentBranched(:)),1);
                    % Add the tip positions of the newly branched filaments
                        % Put the new branch at the tip of the mother filament
                            % X-positions
                                newXPosVals(:)=nan;
                                newXPosVals(filamentBranched) = tipXPos(filamentBranched) + l_m*sin(newThetaVals(filamentBranched));
                                % Update the membrane segment position values at the endpoints so they're
                                % next to their nearest membrane segments
                                    tipTooFarRight(:) = ((newXPosVals>=leadingEdgeLengthInNanometers)&filamentBranched);
                                    tipTooFarLeft(:) = ((newXPosVals<0)&filamentBranched);
                                    newXPosVals(tipTooFarRight) = ...
                                        newXPosVals(tipTooFarRight) - leadingEdgeLengthInNanometers;
                                    newXPosVals(tipTooFarLeft) = ...
                                        newXPosVals(tipTooFarLeft) + leadingEdgeLengthInNanometers;
                                    if allowSpread
                                        newMemSegmentIdx(:) = ceil(newXPosVals/xSepMemSegmentsInNanometers);
                                    else
                                        newMemSegmentIdx(:) = nan;
                                        newMemSegmentIdx(filamentBranched) = memSegIdx(filamentBranched);
                                    end
                            % Y-positions
                                newYPosVals(filamentBranched) = tipYPosRelMem(filamentBranched) - l_m*cos(newThetaVals(filamentBranched));
                                newYPosVals(filamentBranched) = max(newYPosVals(filamentBranched)',0.*ones(size(newYPosVals(filamentBranched)')));
                % Pull out the new indices to save the branches                
                    % Count how many new branches are in each column  
                        numNewBranchesInEachColumn(:) = accumarray([newMemSegmentIdx(filamentBranched);...
                            (1:numMemSegments)'],1)-1;
                    % Loop through each column and find the open indices for the new branches    
                    for numCol = find(numNewBranchesInEachColumn>0)
                        newFilamentLinearIndexesBranch(newMemSegmentIdx==numCol) = ...
                            ((numCol-1)*maxNumFilaments2Sim) + ...
                            find(~filamentExists(:,numCol),numNewBranchesInEachColumn(numCol),'first')';
                    end
                % Initialize these new filaments
                    % Record these new angles
                        thetaVals(newFilamentLinearIndexesBranch(filamentBranched)) = newThetaVals(filamentBranched);
                    % Record the new membrane segment index
                        memSegIdx(newFilamentLinearIndexesBranch(filamentBranched)) = newMemSegmentIdx(filamentBranched);
                    % Record these new tip positions
                        tipYPosRelMem(newFilamentLinearIndexesBranch(filamentBranched)) = newYPosVals(filamentBranched);
                        tipXPos(newFilamentLinearIndexesBranch(filamentBranched)) = newXPosVals(filamentBranched);
                    % Add the length of the newly branched filaments
                        l_f(newFilamentLinearIndexesBranch(filamentBranched)) = l_m;   
                    % Add the newly branched filaments to teh capped
                    % filaments vector
                        filamentIsCapped(newFilamentLinearIndexesBranch(filamentBranched)) = false;
                    % Update the vector to store the current
                    % filament indexes
                        filamentExists(newFilamentLinearIndexesBranch(filamentBranched)) = true;
                    % Add the monomer increase delta val
                        deltaVals(newFilamentLinearIndexesBranch(filamentBranched)) = l_m*cos(thetaVals(newFilamentLinearIndexesBranch(filamentBranched)));
                        deltaValsXDim(newFilamentLinearIndexesBranch(filamentBranched)) = l_m*sin(thetaVals(newFilamentLinearIndexesBranch(filamentBranched)));
                    % Update the effective bending constant perpendicular to the
                    % membrane in pN nm^(-1)
                        kappaValsPerp(newFilamentLinearIndexesBranch(filamentBranched)) = (3*l_p*kT)./(l_f(newFilamentLinearIndexesBranch(filamentBranched)).^3);
                        kappaValsPar(newFilamentLinearIndexesBranch(filamentBranched)) = (16*l_p^2*kT)./(l_f(newFilamentLinearIndexesBranch(filamentBranched)).^4);
                        kappaVals(newFilamentLinearIndexesBranch(filamentBranched)) = (kappaValsPerp(newFilamentLinearIndexesBranch(filamentBranched)).*kappaValsPar(newFilamentLinearIndexesBranch(filamentBranched)))...
                            ./((kappaValsPerp(newFilamentLinearIndexesBranch(filamentBranched)).*(cos(thetaVals(newFilamentLinearIndexesBranch(filamentBranched)))).^2)+...
                            (kappaValsPar(newFilamentLinearIndexesBranch(filamentBranched)).*(sin(thetaVals(newFilamentLinearIndexesBranch(filamentBranched)))).^2));

            end

        
            % Update the filament length
            
                % Pull out the filaments that changed lengths
                    filamentAddedOrRemovedMonomer(:) = (filamentAddedMonomer|filamentRemovedMonomer);
                % Update the filament y-positions relative to the membrane segment
                    tipYPosRelMem(filamentAddedOrRemovedMonomer) = tipYPosRelMem(filamentAddedOrRemovedMonomer) - ...
                        (deltaVals(filamentAddedOrRemovedMonomer).*(filamentAddedMonomer(filamentAddedOrRemovedMonomer)-filamentRemovedMonomer(filamentAddedOrRemovedMonomer)));
                % Update the filament x-positions
                    tipXPos(filamentAddedOrRemovedMonomer) = tipXPos(filamentAddedOrRemovedMonomer) + ...
                        deltaValsXDim(filamentAddedOrRemovedMonomer).*(filamentAddedMonomer(filamentAddedOrRemovedMonomer)-filamentRemovedMonomer(filamentAddedOrRemovedMonomer));
                    % Update the membrane segment position values at the endpoints so they're
                    % next to their nearest membrane segments
                        % Too far too the left
                            % Find the filaments
                                tipTooFarLeft(:) = (tipXPos<0);
                            % Move them to the other side
                                tipXPos(tipTooFarLeft) = ...
                                    tipXPos(tipTooFarLeft) + leadingEdgeLengthInNanometers;
                        % Too far too the right
                            % Find the filaments
                                tipTooFarRight(:) = (tipXPos>=leadingEdgeLengthInNanometers);
                            % Move them to the other side
                                tipXPos(tipTooFarRight) = ...
                                    tipXPos(tipTooFarRight) - leadingEdgeLengthInNanometers;
                    % Update the membrane segment index (we will move them
                    % at the end)
                        if allowSpread
                            memSegIdx(filamentAddedOrRemovedMonomer) = ceil(tipXPos(filamentAddedOrRemovedMonomer)/xSepMemSegmentsInNanometers);
                        end
                % Update the properties of the actin filament
                    % Update the length of the filaments
                        l_f(filamentAddedOrRemovedMonomer) = l_f(filamentAddedOrRemovedMonomer) + ...
                            (l_m.*(filamentAddedMonomer(filamentAddedOrRemovedMonomer)-filamentRemovedMonomer(filamentAddedOrRemovedMonomer)));
                    % Update the effective bending constant perpendicular to the
                    % membrane in pN nm^(-1)
                        kappaValsPerp(filamentAddedOrRemovedMonomer) = (3*l_p*kT)./(l_f(filamentAddedOrRemovedMonomer).^3);
                        kappaValsPar(filamentAddedOrRemovedMonomer) = (16*l_p^2*kT)./(l_f(filamentAddedOrRemovedMonomer).^4);
                        kappaVals(filamentAddedOrRemovedMonomer) = (kappaValsPerp(filamentAddedOrRemovedMonomer).*kappaValsPar(filamentAddedOrRemovedMonomer))...
                            ./((kappaValsPerp(filamentAddedOrRemovedMonomer).*(cos(thetaVals(filamentAddedOrRemovedMonomer))).^2) + ...
                            (kappaValsPar(filamentAddedOrRemovedMonomer).*(sin(thetaVals(filamentAddedOrRemovedMonomer))).^2));
            
            % Cap the filaments
                filamentIsCapped(filamentCapped) = true;  
                   
            % Remove filaments that don't provide force                
                % Add the filaments with length <= 0
                    filamentRemovedFromSimulation(l_f<=0) = true;  
                % Remove the index
                    filamentExists(filamentRemovedFromSimulation)=false;
                % Remove tip positions
                    % Y-vals
                        tipYPosRelMem(filamentRemovedFromSimulation)=nan;
                    % X-vals
                        tipXPos(filamentRemovedFromSimulation)=nan;
                    % membrane segment idx
                        memSegIdx(filamentRemovedFromSimulation)=nan;
                % Remove the branch angle
                    thetaVals(filamentRemovedFromSimulation)=nan;
                % Remove the length of the newly branched filaments
                    l_f(filamentRemovedFromSimulation)=nan;
                % Remove the capped filaments
                    filamentIsCapped(filamentRemovedFromSimulation)=false;
                    deltaVals(filamentRemovedFromSimulation)=nan;
                    deltaValsXDim(filamentRemovedFromSimulation)=nan;
                    kappaValsPerp(filamentRemovedFromSimulation)=nan;
                    kappaValsPar(filamentRemovedFromSimulation)=nan;
                    kappaVals(filamentRemovedFromSimulation)=nan;

            % Switch filaments to their new membrane segment column is needed        
            if allowSpread
                
                % Determine which filaments need to be switched
                    memSegmentChangeIdx(:) = ((memSegIdx~=columnNumbers)&~isnan(memSegIdx));

                if any(memSegmentChangeIdx(:))
                    % Determine where to move the new filaments
                        newMemSegmentIdx(:)=nan;
                        newMemSegmentIdx(memSegmentChangeIdx) = memSegIdx(memSegmentChangeIdx);                         
                    % Count how many new filaments are in each column    
                        numNewFilsInEachColumn(:) = accumarray([newMemSegmentIdx(memSegmentChangeIdx);...
                            (1:numMemSegments)'],1)-1;                        
                    for numCol = find(numNewFilsInEachColumn>0)
                        newFilamentLinearIndexes(newMemSegmentIdx==numCol) = ...
                            [((numCol-1)*maxNumFilaments2Sim)+ ...
                            find(~filamentExists(:,numCol),numNewFilsInEachColumn(numCol),'first')'];
                    end
                    % Update the matrices
                        % Remove the index
                            filamentExists(newFilamentLinearIndexes(memSegmentChangeIdx))=filamentExists(memSegmentChangeIdx);
                            filamentExists(memSegmentChangeIdx)=false;
                        % Remove tip positions
                            % Y-vals
                                tipYPosRelMem(newFilamentLinearIndexes(memSegmentChangeIdx))=tipYPosRelMem(memSegmentChangeIdx) - ...
                                    yPosMemInNanometers(columnNumbers(memSegmentChangeIdx))' + ...
                                    yPosMemInNanometers(memSegIdx(memSegmentChangeIdx))';
                                tipYPosRelMem(memSegmentChangeIdx)=nan;
                            % X-vals
                                tipXPos(newFilamentLinearIndexes(memSegmentChangeIdx))=tipXPos(memSegmentChangeIdx);
                                tipXPos(memSegmentChangeIdx)=nan;
                            % membrane segment idx
                                memSegIdx(newFilamentLinearIndexes(memSegmentChangeIdx))=memSegIdx(memSegmentChangeIdx);
                                memSegIdx(memSegmentChangeIdx)=nan;
                        % Remove the branch angle
                            thetaVals(newFilamentLinearIndexes(memSegmentChangeIdx))=thetaVals(memSegmentChangeIdx);
                            thetaVals(memSegmentChangeIdx)=nan;
                        % Remove the length of the newly branched filaments
                            l_f(newFilamentLinearIndexes(memSegmentChangeIdx))=l_f(memSegmentChangeIdx);
                            l_f(memSegmentChangeIdx)=nan;
                        % Remove the capped filaments
                            filamentIsCapped(newFilamentLinearIndexes(memSegmentChangeIdx))=filamentIsCapped(memSegmentChangeIdx);
                            filamentIsCapped(memSegmentChangeIdx)=false;                    
                       % Add the monomer increase delta val
                            deltaVals(newFilamentLinearIndexes(memSegmentChangeIdx)) = deltaVals(memSegmentChangeIdx);
                            deltaVals(memSegmentChangeIdx) = nan;
                            deltaValsXDim(newFilamentLinearIndexes(memSegmentChangeIdx)) = deltaValsXDim(memSegmentChangeIdx);
                            deltaValsXDim(memSegmentChangeIdx) = nan;
                        % Update the effective bending constant perpendicular to the
                        % membrane in pN nm^(-1)
                            kappaValsPerp(newFilamentLinearIndexes(memSegmentChangeIdx)) = kappaValsPerp(memSegmentChangeIdx);
                            kappaValsPerp(memSegmentChangeIdx) = nan;
                            kappaValsPar(newFilamentLinearIndexes(memSegmentChangeIdx)) = kappaValsPar(memSegmentChangeIdx);
                            kappaValsPar(memSegmentChangeIdx) = nan;
                            kappaVals(newFilamentLinearIndexes(memSegmentChangeIdx)) = kappaVals(memSegmentChangeIdx);
                            kappaVals(memSegmentChangeIdx) = nan;

                end
            end
            
            % Update the summary variables             
                % Update the number of filaments per membrane segment
                    numFilamentsPerMemSegment(:) = sum(filamentExists);
                    % If there are too many or no filaments, stop the
                    % simulation
                        if any(numFilamentsPerMemSegment>maxNumFilaments2Sim)
                           ME = 'Reached max number of simulated filaments';
                           fail=1;
                           break
                        % If more than 10% of membrane segments have no filaments left
                        elseif (sum(numFilamentsPerMemSegment==0)/numMemSegments)>0.25
                            ME = 'All filaments are depolymerized';
                            fail=1;
                            break
                        end  
                % Determine the total number of filaments
                    numFilamentsTotal(:) = sum(filamentExists(:));
                % Update the existing and uncapped filament matrix
                    filamentExistsAndIsUncapped(:) = ((~filamentIsCapped)&filamentExists);
                    % Determine the tital number of existing and uncapped filaments
                        numExistingAndUncappedFilaments(:) = sum(filamentExistsAndIsUncapped(:)); 

        % Save the membrane info, is it's time
            if mod(numTimeStep,numTimeStepsMem2Skip)==0

                % Save the membrane segment kinetics
                    % Save the absolute position of the membrane
                    % segment
                        yPosMemInNanometersRecordTemp(numTimeStepsRecordedThisIter_MemInfo,:) = yPosMemInNanometers;
                    % Save the absolute velocity of the membrane
                    % segment
                        vYPosMemInNanometersRecordTemp(numTimeStepsRecordedThisIter_MemInfo,:) = dYPosMemInNanometers./dtRelaxMemInMilliseconds;
                    % Save the external force on the membrane
                    % segment
                        F_stretch_bend_mem_RecordTemp(numTimeStepsRecordedThisIter_MemInfo,:) = F_stretch_bend_mem;
                    % Save the force of the filaments on the membrane
                    % segment
                        fActinRecordTemp(numTimeStepsRecordedThisIter_MemInfo,:) = sumFActin;
                % Save the filament kinetics
                    % Save the number of filaments    
                        numFilamentsRecordTemp(numTimeStepsRecordedThisIter_MemInfo,:) = numFilamentsPerMemSegment;
                    % Save the number of capped filaments
                        numCappedFilamentsRecordTemp(numTimeStepsRecordedThisIter_MemInfo,:) = sum(filamentIsCapped);
                    % Save the mean angle (of uncapped filaments)
                        thetaVals2Avg(:) = mod(thetaVals,2*pi);
                        thetaVals2Avg(thetaVals2Avg>pi) = abs((2*pi) - thetaVals2Avg(thetaVals2Avg>pi));
                        thetaVals2Avg((~filamentExists)|filamentIsCapped) = nan;
                        meanThetaValsRecordTemp(numTimeStepsRecordedThisIter_MemInfo,:) = nanmean(thetaVals2Avg,1);
                    % Save the mean angle (of uncapped filaments)
                        lf2Avg(:) = l_f;
                        lf2Avg((~filamentExists)|filamentIsCapped) = nan;
                        meanlfRecordTemp(numTimeStepsRecordedThisIter_MemInfo,:) = nanmean(lf2Avg,1);

                if numTimeStepsRecordedThisIter_MemInfo == numTimeStepsMem2KeepInWorkspace
                    % Calculate the indices to store the tempeorary
                    % variable in the file
                        timeIdx2SaveIntoMem(:) = false;
                        timeIdx2SaveIntoMem((numTimeStepsRecorded_MemInfo-numTimeStepsMem2KeepInWorkspace+1):numTimeStepsRecorded_MemInfo)=true;
                    % Save the temporary variables to the file
                        matFileObj.yPosMemInNanometersRecord(find(timeIdx2SaveIntoMem),1:numMemSegments) = yPosMemInNanometersRecordTemp;
                        matFileObj.vYPosMemInNanometersRecord(find(timeIdx2SaveIntoMem),1:numMemSegments) = vYPosMemInNanometersRecordTemp;
                        matFileObj.F_stretch_bend_mem_Record(find(timeIdx2SaveIntoMem),1:numMemSegments) = F_stretch_bend_mem_RecordTemp;
                        matFileObj.fActinRecord(find(timeIdx2SaveIntoMem),1:numMemSegments) = fActinRecordTemp;   
                        matFileObj.numFilamentsRecord(find(timeIdx2SaveIntoMem),1:numMemSegments) = numFilamentsRecordTemp; 
                        matFileObj.numCappedFilamentsRecord(find(timeIdx2SaveIntoMem),1:numMemSegments) = numCappedFilamentsRecordTemp; 
                        matFileObj.meanThetaValsRecord(find(timeIdx2SaveIntoMem),1:numMemSegments) = meanThetaValsRecordTemp;
                        matFileObj.meanlfRecord(find(timeIdx2SaveIntoMem),1:numMemSegments) = meanlfRecordTemp;
                    % Re-set the temporary variables
                        yPosMemInNanometersRecordTemp(:) = nan;
                        vYPosMemInNanometersRecordTemp(:) = nan;
                        F_stretch_bend_mem_RecordTemp(:) = nan;
                        fActinRecordTemp(:) = nan;
                        numFilamentsRecordTemp(:) = nan;
                        numCappedFilamentsRecordTemp(:) = nan;
                        meanThetaValsRecordTemp(:) = nan;
                        meanlfRecordTemp(:) = nan;
                    % Initialize the counter to record and save these points
                        numTimeStepsRecordedThisIter_MemInfo=0;
                end

                % Update the counter
                    numTimeStepsRecordedThisIter_MemInfo = numTimeStepsRecordedThisIter_MemInfo + 1;
                    numTimeStepsRecorded_MemInfo = numTimeStepsRecorded_MemInfo + 1;
            end


        % Save the filament info, is it's time
            if mod(numTimeStep,numTimeStepsFilament2Skip)==0

                % Save the filament info
                    % Calculate the total number of filaments
                        numFilamentsTotal = sum(filamentExists(:));
                    % Save the barbed end y-positions (absolute)
                        filamentBarbedEndYValsRecordTemp(numTimeStepsRecordedThisIter_FilamentInfo,1:numFilamentsTotal) = yPosMemInNanometers(memSegIdx(filamentExists))' - tipYPosRelMem(filamentExists);
                    % Save the barbed end x-positions
                        filamentBarbedEndXValsRecordTemp(numTimeStepsRecordedThisIter_FilamentInfo,1:numFilamentsTotal) = tipXPos(filamentExists);
                    % Save the filament angles
                        thetaValsRecordTemp(numTimeStepsRecordedThisIter_FilamentInfo,1:numFilamentsTotal) = thetaVals(filamentExists);
                    % Save the filament lengths
                        lfRecordTemp(numTimeStepsRecordedThisIter_FilamentInfo,1:numFilamentsTotal) = l_f(filamentExists);
                    % save the capped state of the filaments
                        filamentIsCappedRecordTemp(numTimeStepsRecordedThisIter_FilamentInfo,1:numFilamentsTotal) = filamentIsCapped(filamentExists);


                if numTimeStepsRecordedThisIter_FilamentInfo == numTimeStepsFilament2KeepInWorkspace
                    % Calculate the indices to store the tempeorary
                    % variable in the file
                        % Time
                            timeIdx2SaveIntoFil(:) = false;
                            timeIdx2SaveIntoFil((numTimeStepsRecorded_FilamentInfo-numTimeStepsFilament2KeepInWorkspace+1):numTimeStepsRecorded_FilamentInfo)=true;
                        % Filaments
                            filIdx2SaveFil(:) = false;
                            maxNumFilamentsThisIter = find(any(~isnan(filamentBarbedEndYValsRecordTemp)),1,'last');
                            filIdx2SaveFil(1:maxNumFilamentsThisIter) = true;
                    % Save the temporary variables to the file
                        matFileObj.filamentBarbedEndYValsRecord(find(timeIdx2SaveIntoFil),find(filIdx2SaveFil)) = filamentBarbedEndYValsRecordTemp(:,find(filIdx2SaveFil));
                        matFileObj.filamentBarbedEndXValsRecord(find(timeIdx2SaveIntoFil),find(filIdx2SaveFil)) = filamentBarbedEndXValsRecordTemp(:,find(filIdx2SaveFil));
                        matFileObj.thetaValsRecord(find(timeIdx2SaveIntoFil),find(filIdx2SaveFil)) = thetaValsRecordTemp(:,find(filIdx2SaveFil));
                        matFileObj.lfRecord(find(timeIdx2SaveIntoFil),find(filIdx2SaveFil)) = lfRecordTemp(:,find(filIdx2SaveFil));   
                        matFileObj.filamentIsCappedRecord(find(timeIdx2SaveIntoFil),find(filIdx2SaveFil)) = filamentIsCappedRecordTemp(:,find(filIdx2SaveFil)); 
                    % Re-set the temporary variables
                        filamentBarbedEndYValsRecordTemp(:) = nan;
                        filamentBarbedEndXValsRecordTemp(:) = nan;
                        thetaValsRecordTemp(:) = nan;
                        lfRecordTemp(:) = nan; 
                        filamentIsCappedRecordTemp(:) = false;
                    % Initialize the counter to record and save these points
                        numTimeStepsRecordedThisIter_FilamentInfo=0;
                end

                % Update the counter
                    numTimeStepsRecordedThisIter_FilamentInfo = numTimeStepsRecordedThisIter_FilamentInfo + 1;
                    numTimeStepsRecorded_FilamentInfo = numTimeStepsRecorded_FilamentInfo + 1;
            end

            % Check in on BF calcs
                % If we've reached the max, recalculate and start over    
                if numTimeStepThisIter_CalcBF == numTimeStepsMem2CalcBF
                    % Calculate the Brownian flucts for each membrane segment in this iteration of
                    % equilibration   
                        % Create a random number for all membrane segments for all time points   
                            brownianForce(:) = xi*normrnd(0,1,[numTimeStepsMem2CalcBF, numMemSegments]);
                        % Reset the counter
                            numTimeStepThisIter_CalcBF = 0;

                end
                % Update how many BF calcs have been used this iteration
                    numTimeStepThisIter_CalcBF = numTimeStepThisIter_CalcBF + 1; 



    end


    clear brownianForce

    % Store the remaining membrane data
    if numTimeStepsRecordedThisIter_MemInfo>1
        numTimeStepsRecorded_MemInfo = numTimeStepsRecorded_MemInfo-1;
        numTimeStepsRecordedThisIter_MemInfo = numTimeStepsRecordedThisIter_MemInfo - 1;
        % Calculate the indices to store the temporary
        % variable in the file
            timeIdx2SaveIntoMem(:) = false;
            timeIdx2SaveIntoMem((numTimeStepsRecorded_MemInfo-numTimeStepsRecordedThisIter_MemInfo+1):numTimeStepsRecorded_MemInfo)=true;
            timeIdx2SaveFromMem(:) = false;
            timeIdx2SaveFromMem(1:numTimeStepsRecordedThisIter_MemInfo) = true;
        % Save the temporary variables to the file
            matFileObj.yPosMemInNanometersRecord(find(timeIdx2SaveIntoMem),1:numMemSegments) = yPosMemInNanometersRecordTemp(find(timeIdx2SaveFromMem),:);
            matFileObj.vYPosMemInNanometersRecord(find(timeIdx2SaveIntoMem),1:numMemSegments) = vYPosMemInNanometersRecordTemp(find(timeIdx2SaveFromMem),:);
            matFileObj.F_stretch_bend_mem_Record(find(timeIdx2SaveIntoMem),1:numMemSegments) = F_stretch_bend_mem_RecordTemp(find(timeIdx2SaveFromMem),:);
            matFileObj.fActinRecord(find(timeIdx2SaveIntoMem),1:numMemSegments) = fActinRecordTemp(find(timeIdx2SaveFromMem),:);   
            matFileObj.numFilamentsRecord(find(timeIdx2SaveIntoMem),1:numMemSegments) = numFilamentsRecordTemp(find(timeIdx2SaveFromMem),:); 
            matFileObj.numCappedFilamentsRecord(find(timeIdx2SaveIntoMem),1:numMemSegments) = numCappedFilamentsRecordTemp(find(timeIdx2SaveFromMem),:); 
            matFileObj.meanThetaValsRecord(find(timeIdx2SaveIntoMem),1:numMemSegments) = meanThetaValsRecordTemp(find(timeIdx2SaveFromMem),:);
            matFileObj.meanlfRecord(find(timeIdx2SaveIntoMem),1:numMemSegments) = meanlfRecordTemp(find(timeIdx2SaveFromMem),:);
    end

    % Store the remaining filament data
    if numTimeStepsRecordedThisIter_FilamentInfo>1

        numTimeStepsRecordedThisIter_FilamentInfo = numTimeStepsRecordedThisIter_FilamentInfo - 1;
        numTimeStepsRecorded_FilamentInfo = numTimeStepsRecorded_FilamentInfo - 1;

        % Calculate the indices to store the tempeorary
        % variable in the file
            % Time
                timeIdx2SaveIntoFil(:) = false;
                timeIdx2SaveIntoFil((numTimeStepsRecorded_FilamentInfo-numTimeStepsRecordedThisIter_FilamentInfo+1):numTimeStepsRecorded_FilamentInfo) = true;
                timeIdx2SaveFromFil(:) = false;
                timeIdx2SaveFromFil(1:numTimeStepsRecordedThisIter_FilamentInfo) = true;
            % Filaments
                filIdx2SaveFil(:) = false;
                maxNumFilamentsThisIter = find(any(~isnan(filamentBarbedEndYValsRecordTemp)),1,'last');
                filIdx2SaveFil(1:maxNumFilamentsThisIter) = true;
        % Save the temporary variables to the file
            matFileObj.filamentBarbedEndYValsRecord(find(timeIdx2SaveIntoFil),find(filIdx2SaveFil)) = filamentBarbedEndYValsRecordTemp(find(timeIdx2SaveFromFil),find(filIdx2SaveFil));
            matFileObj.filamentBarbedEndXValsRecord(find(timeIdx2SaveIntoFil),find(filIdx2SaveFil)) = filamentBarbedEndXValsRecordTemp(find(timeIdx2SaveFromFil),find(filIdx2SaveFil));
            matFileObj.thetaValsRecord(find(timeIdx2SaveIntoFil),find(filIdx2SaveFil)) = thetaValsRecordTemp(find(timeIdx2SaveFromFil),find(filIdx2SaveFil));
            matFileObj.lfRecord(find(timeIdx2SaveIntoFil),find(filIdx2SaveFil)) = lfRecordTemp(find(timeIdx2SaveFromFil),find(filIdx2SaveFil));   
            matFileObj.filamentIsCappedRecord(find(timeIdx2SaveIntoFil),find(filIdx2SaveFil)) = filamentIsCappedRecordTemp(find(timeIdx2SaveFromFil),find(filIdx2SaveFil)); 
    end

        % Clear the temporary variables
            clear yPosMemInNanometersRecordTemp
            clear vYPosMemInNanometersRecordTemp
            clear F_stretch_bend_mem_RecordTemp
            clear fActinRecordTemp   
            clear numFilamentsRecordTemp; 
            clear numCappedFilamentsRecordTemp; 
            clear meanThetaValsRecordTemp;
            clear meanlfRecordTemp;
            clear filamentBarbedEndYValsRecordTemp;
            clear filamentBarbedEndXValsRecordTemp;
            clear thetaValsRecordTemp;
            clear lfRecordTemp;   
            clear filamentIsCappedRecordTemp; 

catch errorMessage
    ME = errorMessage;
end

% Pack the data into a structure
    outputVars = v2struct;
% Clear the remaining variables
    clearvars -except outputVars
    try
        
        % Save the data
            save(outputVars.filePathOutOnCloud,'-append','-struct','outputVars');
            
        % Keep the file path to save teh results
            filePathOutOnCloud = outputVars.filePathOutOnCloud;
            ME = outputVars.ME;
        
        % Clear outputVars
            clear outputVars
            
    catch ME2
        outputVars.ME2 = ME2;
        try
            save(outputVars.fileNameOut,'-struct','outputVars','-v7.3');
        catch ME3
            outputVars.ME3 = ME3;
        end
    end
    
% Run mode decorrelation analysis
    if isempty(ME)     
        % Prepare the data for autocotorrelation analysis   
            % Load the variables
                load(filePathOutOnCloud,'numTimeStepsMem2Record')
                load(filePathOutOnCloud,'numTimeStepsMem2Skip')
                load(filePathOutOnCloud,'dtRelaxMemInMilliseconds')
                load(filePathOutOnCloud,'numMemSegments')
                load(filePathOutOnCloud,'xSepMemSegmentsInNanometers')
                load(filePathOutOnCloud,'leadingEdgeLengthInNanometers')
                load(filePathOutOnCloud,'yPosMemInNanometersRecord')
            % Rename the variables to be more generic 
                numTimePoints = numTimeStepsMem2Record;
                yPosMemInNanometers = yPosMemInNanometersRecord;
                nanometersPerXValInc = xSepMemSegmentsInNanometers;
            % Calculate the number of milliseconds in between membrane position
            % measurements
                millisecondsPerFrame = numTimeStepsMem2Skip.*dtRelaxMemInMilliseconds; 
            % Choose the number of timesteps to skip (none)
                numTimePoints2SkipAutocorr = 1;
            % Choose the timepoint to start the analysis (after reaching steady state)   
                timePoint2StartAutocorr = round(10000/millisecondsPerFrame);
            % Choose whether to plot the result
                doPlot=0;
                
        if ~isempty(timePoint2StartAutocorr:numTimePoints2SkipAutocorr:numTimePoints)

        % Run the autocorrelation analysis        
            spatialFFTModeTimeAutocorrelationAnalysis      

        % Save the input data to a structure   
            spatialFFTModeData = struct();
            spatialFFTModeData.spatialFrequencyInInverseNanometers = spatialFrequencyInInverseNanometers;
            spatialFFTModeData.numTimePoints2SkipAutocorr = numTimePoints2SkipAutocorr;
            spatialFFTModeData.timePoint2StartAutocorr = timePoint2StartAutocorr;
            spatialFFTModeData.FFTYPosMemInNanometers = FFTYPosMemInNanometers;
        % Save the autocorrelations to a structure    
            spatialFFTModeTimeAutocorrData = struct();
            spatialFFTModeTimeAutocorrData.rVals = rVals;
            spatialFFTModeTimeAutocorrData.lagPosInMilliseconds = lagPosInMilliseconds;
            spatialFFTModeTimeAutocorrData.numTimePoints2SkipAutocorr = numTimePoints2SkipAutocorr;
            spatialFFTModeTimeAutocorrData.timePoint2StartAutocorr = timePoint2StartAutocorr;
        % Save the fit results to a structure     
            spatialFFTModeTimeAutocorrFits = struct();
            spatialFFTModeTimeAutocorrFits.spatialFrequencyInInverseNanometers = spatialFrequencyInInverseNanometers;
            spatialFFTModeTimeAutocorrFits.expDecayConst = expDecayConst;
            spatialFFTModeTimeAutocorrFits.expAmpConst = expAmpConst;   
            spatialFFTModeTimeAutocorrFits.numTimeLags2Fit = numTimeLags2Fit;    
            spatialFFTModeTimeAutocorrFits.MEAutocorrFit = MEAutocorrFit;                       

        % Pack the data into a structure
            outputVars = struct;
            outputVars.spatialFFTModeData = spatialFFTModeData;
            outputVars.spatialFFTModeTimeAutocorrData = spatialFFTModeTimeAutocorrData;
            outputVars.spatialFFTModeTimeAutocorrFits = spatialFFTModeTimeAutocorrFits;

        % Save the data
            save(filePathOutOnCloud,'-append','-struct','outputVars');
        end

        % Load the parallel checker
            load(filePathOutOnCloud,'globalInfo')
        if globalInfo.runInParallel
            % Load the current job
                job = getCurrentJob;
            % Detele the job       
                delete(job);
        end

    end 
    
% Pack the data into a structure
    outputVars = v2struct;
% Clear the remaining variables
    clearvars -except outputVars   

    
end