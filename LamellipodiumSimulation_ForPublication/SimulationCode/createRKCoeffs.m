
function [finiteDiffCoeffs2ndDerCentral, shift2ndDer, shift2ndDerIdx, finiteDiffCoeffs4thDerCentral, shift4thDer, shift4thDerIdx] =  createRKCoeffs(orderOfAccuracy,numMemBeads)


%% This function creates the variables used for the Runge-Kutta algorithm 
% (Run as part of setUpAndRun_simulateLamellipodiumAndRunXCorrAnalysis.m)

% This code was written by Rikki M. Garner as a companion to the manuscript
% titled "Leading edge stability in motile cells in an emergent property of
% branched actin network growth", and was last updated 2020/08/19.
%%

        if mod(orderOfAccuracy,2)==0
        % Calculate the central difference coefficients for the 2nd
        % derivative
            % Calculate the number of beads in the sliding window for this
            % order of accuracy
                numPoints2ndDer = 3 + 2*((orderOfAccuracy/2)-1);
            % Compute all of the coefficients
                finiteDiffCoeffs2ndDer = ufdwt(1,numPoints2ndDer,2);
            % Only pull out the central coefficients
                finiteDiffCoeffs2ndDerCentral = finiteDiffCoeffs2ndDer(ceil(length(finiteDiffCoeffs2ndDer)/2),:);
            % Store a matrix of these coefficients for each bead    
                finiteDiffCoeffs2ndDerCentral =  bsxfun(@times,ones(numPoints2ndDer,numMemBeads),finiteDiffCoeffs2ndDerCentral');
            % Compute a shift vector for each row of the position matrix
                shift2ndDer = -floor(numPoints2ndDer/2):floor(numPoints2ndDer/2);
                shift2ndDer(shift2ndDer<0) = shift2ndDer(shift2ndDer<0)+numMemBeads;
            % Compute a linear index for a shift matrix  
                shift2ndDerIdx=[];
                for n = 1:numMemBeads
                    newshift2ndDer = shift2ndDer+n;
                    newshift2ndDer(newshift2ndDer<0) = newshift2ndDer(newshift2ndDer<0)+numMemBeads;
                    newshift2ndDer(newshift2ndDer>numMemBeads) = newshift2ndDer(newshift2ndDer>numMemBeads)-numMemBeads;
                    shift2ndDerIdx = [shift2ndDerIdx newshift2ndDer];
                end
                
        % Calculate the central difference coefficients for the 4th
        % derivative
            % Calculate the number of beads in the sliding window for this
            % order of accuracy
                numPoints4thDer = 5 + 2*((orderOfAccuracy/2)-1);
            % Compute all of the coefficients
                finiteDiffCoeffs4thDer = ufdwt(1,numPoints4thDer,4);
            % Only pull out the central coefficients
                finiteDiffCoeffs4thDerCentral = finiteDiffCoeffs4thDer(ceil(length(finiteDiffCoeffs4thDer)/2),:);
            % Store a matrix of these coefficients for each bead    
                finiteDiffCoeffs4thDerCentral =  bsxfun(@times,ones(numPoints4thDer,numMemBeads),finiteDiffCoeffs4thDerCentral');
            % Compute a shift vector for each row of the position matrix
                shift4thDer = -floor(numPoints4thDer/2):floor(numPoints4thDer/2);
                shift4thDer(shift4thDer<0) = shift4thDer(shift4thDer<0)+numMemBeads;
            % Compute a linear index for a shift matrix  
                shift4thDerIdx=[];
                for n = 1:numMemBeads
                    newshift4thDer = shift4thDer+n;
                    newshift4thDer(newshift4thDer<0) = newshift4thDer(newshift4thDer<0)+numMemBeads;
                    newshift4thDer(newshift4thDer>numMemBeads) = newshift4thDer(newshift4thDer>numMemBeads)-numMemBeads;
                    shift4thDerIdx = [shift4thDerIdx newshift4thDer];
                end
                
                
        else
            'Order of accuracy must be even'
            return
        end