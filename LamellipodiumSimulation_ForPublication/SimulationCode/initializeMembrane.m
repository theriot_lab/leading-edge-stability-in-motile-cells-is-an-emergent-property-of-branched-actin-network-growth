function [inputVars] = initializeMembrane(inputVars)

%% This function initializes the membrane
% (Run as part of setUpAndRun_simulateLamellipodiumAndRunXCorrAnalysis.m)

% This code was written by Rikki M. Garner as a companion to the manuscript
% titled "Leading edge stability in motile cells in an emergent property of
% branched actin network growth", and was last updated 2020/08/19.
%%

    % Unpack this structure and clear the original structure
        v2struct(inputVars)
        clear inputVars        
        
    % Initialize the spatial scales of the membrane          
    
        % Choose the spatial discretization and create vectors to store
        % this positional information
            % Calculate the separation between membane segment centroids in nanometers
                xSepMemSegmentsInNanometers = leadingEdgeLengthInNanometers/numMemSegments;
            % Calculate the membrane segment centroid x-positions in nanometers
                xPosMemInNanometers = (xSepMemSegmentsInNanometers/2):xSepMemSegmentsInNanometers:leadingEdgeLengthInNanometers;
            % Calculate the possible wavenumbers
                kmax = numMemSegments-1;
                wavNums = 0:kmax;

    % Initialize the physical properties of the membrane

        % Calculate the effective stretching constant for this discretization
        % (in picoNewtons/nanometer * discretization * membrane height)
            sigmaVal = sigmaVal*xSepMemSegmentsInNanometers*leadingEdgeHeightInNanometers;
        % Calculate the effective bending constant for this discretization
        % (in picoNewton nanometers * discretization * membrane height)
            kappaVal = kappaVal*xSepMemSegmentsInNanometers*leadingEdgeHeightInNanometers;
        % Calculate the thermal energy in (picoNewton nanometers)
            % Calcuate the temperature in K
                T_K = 273.15 + T_C;
            % kT   
                kT = k_B*T_K;
        % Calculate the viscosity of water at this temperature
            etaVal = 2.414*10^(-5)*10^(-3)*10^(247.8/(T_K-140));
        % Calculate the Stokes drag for this segment size (assuming the dynamic
        % viscoty of water (in picoNewtons milliseconds / nanometer)
            gammaVal = 6*pi*etaVal*xSepMemSegmentsInNanometers;
            % Increase this viscosity to account for the viscosity of the
            % agarose
                gammaVal = gammaVal*gammaMult;    
                
                
    % Initialize the membrane timescales

        % Determine the minimal/maximal timestep to capture the highest/lowest wavemode behavior
            % Calculate the predicted relaxation rates
                if (sigmaVal~=0)||(kappaVal~=0)        
                    % Calculate the predicted relaxation rates (in milliseconds^(-1))
                        decayRates = (sigmaVal./gammaVal).*(2.*pi.*wavNums./leadingEdgeLengthInNanometers).^2 ... 
                                + (kappaVal./gammaVal).*(2.*pi.*wavNums./leadingEdgeLengthInNanometers).^4;
                    % Convert these rates into timescales (in milliseconds)
                        decayTimes = 1./decayRates;
                    % Determine the halflife of the fastest relaxation time
                        decayTimeScaleMinMem = min(decayTimes(2:end));
                    % Determine the halflife of the longest relaxation time
                        decayTimeScaleMaxMem = max(decayTimes(2:end)); 
                else %(if sigma and kappa are both zero)
                    decayTimeScaleMinMem = (kT/(gammaVal*xSepMemSegmentsInNanometers^2))/10;
                    decayTimeScaleMaxMem = (kT/(gammaVal*xSepMemSegmentsInNanometers^2))*10;    
                end
                    
    
    % Load these variables into a structure
        inputVars = v2struct();

end