%% The following code allows you to delete jobs on your local server 
% (if you happened to be running batch jobs in parallel)

% This code was written by Rikki M. Garner as a companion to the manuscript
% titled "Leading edge stability in motile cells in an emergent property of
% branched actin network growth", and was last updated 2020/08/19.
    
% Choose the cluster    
    parallel.defaultClusterProfile('local');
    myCluster = parcluster;
    
%% Delete all completed jobs

   finishedJobs = findJob(myCluster,'State','finished');
     %   finishedJobs = findJob(myCluster,'State','failed');
     %   finishedJobs = findJob(myCluster,'State','pending');

    for numJob = 1:length(finishedJobs)
        numJob
        job = finishedJobs(numJob,1);
        delete(job)
    end   
    
%% Optionally delete all jobs on the cluster

    allJobs = findJob(myCluster);
    for numJob = 1:length(allJobs)
        job = allJobs(numJob,1);
        numJob
        delete(job) 
    end
