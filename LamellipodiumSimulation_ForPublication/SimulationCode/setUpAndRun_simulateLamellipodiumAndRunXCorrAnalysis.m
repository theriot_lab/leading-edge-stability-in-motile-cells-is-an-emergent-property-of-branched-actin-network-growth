
%%%%%% Run chooseModelParameters.m before running this code %%%%%

%% The following script loads the simulation parameters, pre-allocates 
% space on the drive to save the output variables, and then runs the main
% simulation code simulateLamellipodiumAndRunXCorrAnalysis.m. You have the
% option to iterate through a range of parameter values for one variable,
% as well as replicates for each choice of parameter value.

% This version assumes no parallelization of the code. Simulations for
% different parameter values and replicates run in series (so take this into 
% account that each simulation takes ~12hrs to run).

% This code was written by Rikki M. Garner as a companion to the manuscript
% titled "Leading edge stability in motile cells in an emergent property of
% branched actin network growth", and was last updated 2020/08/19.

%%

% Clear the system
    close all; 
    clear all;
    
% Don't run in parallel    
    runInParallel = false;
    
% Choose which parameters to scan through (you will specific which variable 
% it is later on inside the for loop)
    parameterVals = [10 12 15];
    
% Choose the file path of the parameter set
    baseParametersFilePath = ['/Users/Rikki/Documents/'...
        'LamellipodiumSimulation_ForPublication/basicModelParameters.mat'];

% Create a folder to save the results
    % Pull out the date and convert to string for folder naming convention
        str = date();
        dateNum = datestr(str,'yyyy/mm/dd');
        dateNum = dateNum(dateNum~='/');
    % Choose the parent folder path to create the new folder in
        folderParentPathOnLocal = ['/Users/Rikki/Documents/'...
            'LamellipodiumSimulation_ForPublication/SimulationResults/'];
    % Create the new folder name and path, assuming it doesn't already
    % exist, otherwise append a number to the end of the date until we have
    % a new folder
        folderNameOnLocal = sprintf('Results%s',dateNum);
        folderPathOnLocal = [folderParentPathOnLocal folderNameOnLocal];
        if ~exist(folderPathOnLocal,'dir')
            mkdir(folderPathOnLocal);
        else
            n=1;
            while exist(folderPathOnLocal,'dir')
                folderNameOnLocal = sprintf('Results%s_%i',dateNum,n);
                folderPathOnLocal = [folderParentPathOnLocal folderNameOnLocal];
                n=n+1;
            end
            mkdir(folderPathOnLocal);
        end

    % Record this file save location (the strange nomenclature is for more 
    % general applications wusing parallelization)
        folderPathOnCloud = folderPathOnLocal;
        destinationFolderPath = folderPathOnCloud;

% Create a file prefix to save the results %% Change variable name "M" as
% needed %%
    fileNameForSprintf = 'Results_%s_LamellipodiumSimulation_M%i_%i';
                    
% Choose the number of replicates
    numReplicates = 1;
    
% Put this info into a structure
    % Make the structure
        globalInfo = v2struct;
    % Clear the remaining variables
        clearvars -except globalInfo
        
% Submit the jobs        
for parameterValsNum = 1:length(globalInfo.parameterVals)
    
    % Set up the simulation for this parameter set (excluding any variables
    % that need to be chosen randomly, like the initial filament positions)
    
        % Load the base set of parameters
            load(globalInfo.baseParametersFilePath)

        % Adjust whichever parameters we're scanning through
            % Free monomer concentration in uM
                M = globalInfo.parameterVals(parameterValsNum);
                
%         % Optionally adjust other variables (can be any parameter defined
%         % in chooseModelParameters.m)
%             % Examples: 
%                 % Temperature in C
%                     T_C = 25;
                % Choose the total time to sim in milliseconds
                    % (note autocorrelation analysis will only run on data collected
                    % after 10,000ms of simulated time (when the simulation 
                    % reaches steady state), but this can be edited around line 
                    % ~1193 of simulateLamellipodiumAndRunXCorrAnalysis.m)
                    totalTime2SimInMilliseconds = 100;

        % Load these variables into a structure
            inputVars = v2struct();
            % Clear all other variables
                clearvars -except globalInfo parameterValsNum inputVars

        % Initialize the simulation (must be in this order)
            % Initialize the membrane
                inputVars = initializeMembrane(inputVars);
            % Initialize the filaments
                inputVars = initializeFilaments(inputVars);
            % Set the timescale
                inputVars = setTimescaleRungeKuttaAndBrownianForce(inputVars);
            % Set how often to record
                inputVars = setMemoryUsage(inputVars);
            % Pre-allocate space for large variables in a file    
                inputVars = preAllocateVariables(inputVars);

    % Submit this function as a batch job for each replicate
        for replicateNum = 1:globalInfo.numReplicates
            
            % Print which loop we're on
                sprintf('Running replicate %i of %i for parameter set %i of %i',...
                    replicateNum,globalInfo.numReplicates,parameterValsNum,length(globalInfo.parameterVals))
                
            % Copy the file for this replicate
                inputVars = copyFileForReplicate(inputVars,globalInfo,parameterValsNum,replicateNum);
                
            % Run the function
               simulateLamellipodiumAndRunXCorrAnalysis(inputVars);
               
        end
        
        delete([inputVars.filePathOutTemp '.mat'])

    % Clear the remaining variables
        clearvars -except globalInfo parameterValsNum
        
end

