function [inputVars] = copyFileForReplicate(inputVars,globalInfo,parameterValsNum,replicateNum)

%% This function copies initialized files for different replicates
% (Run as part of setUpAndRun_simulateLamellipodiumAndRunXCorrAnalysis.m)

% This code was written by Rikki M. Garner as a companion to the manuscript
% titled "Leading edge stability in motile cells in an emergent property of
% branched actin network growth", and was last updated 2020/08/19.
%%

    % Create a file to save the results and transfer to the cloud
        % Pull out the file name
            fileNameOut = sprintf(globalInfo.fileNameForSprintf,...
                globalInfo.dateNum,parameterValsNum,replicateNum);
        % Concatenate to make the file path        
            filePathOutOnLocal = [globalInfo.folderPathOnLocal '/' fileNameOut];
            filePathOutOnCloud = [globalInfo.folderPathOnCloud '/' fileNameOut];
        % Add the file names to the inputParams structure
            inputVars.replicateNum = replicateNum;
            inputVars.fileNameOut = fileNameOut;
            inputVars.filePathOutOnLocal = filePathOutOnLocal;
            inputVars.filePathOutOnCloud = filePathOutOnCloud;
        % Save the file
            copyfile([inputVars.filePathOutTemp '.mat'], [filePathOutOnLocal '.mat'])
            save(filePathOutOnLocal,'-append','-struct','inputVars')

end