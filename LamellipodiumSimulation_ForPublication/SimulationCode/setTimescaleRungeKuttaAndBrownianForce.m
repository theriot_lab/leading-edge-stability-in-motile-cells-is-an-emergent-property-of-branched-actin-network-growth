function [inputVars] = setTimescaleRungeKuttaAndBrownianForce(inputVars)

%% This function chooses the timestep and then calculates the Runge-Kutta 
% and Brownian force parameters
% (Run as part of setUpAndRun_simulateLamellipodiumAndRunXCorrAnalysis.m)

% This code was written by Rikki M. Garner as a companion to the manuscript
% titled "Leading edge stability in motile cells in an emergent property of
% branched actin network growth", and was last updated 2020/08/19.
%%

    % Unpack this structure and clear the original structure
        v2struct(inputVars)
        clear inputVars                   

    % Choose the simulation parameters

        % Choose the timescales 
            % Choose the time step
                % Calculate the relevant timescales based on the rates 
                % (Make sure the time step is small enough that probability of
                % any event is always less than 0.1)
                    allTimeScales = [2*kT*gammaVal/Fext 0.1/k_off 0.1/k_cap 0.1/(k_on*M) 0.1/(k_branch*1000) decayTimeScaleMinMem];
                % Set the timestep to be some fraction of the minimum time scale
                    minTimeScale = min(allTimeScales((~isinf(allTimeScales))&(~isnan(allTimeScales))));
                    dtRelaxMemInMilliseconds = minTimeScale.*fracOfMinTimeScale;
            % Choose the number of time steps
                numTimeSteps2Sim = max(1001,ceil(totalTime2SimInMilliseconds/dtRelaxMemInMilliseconds));
                
        % Prepare the Runge-Kutta parameters

            % Pre-allocate space for the Runge-Kutta multiplier
                RKMultiplier = bsxfun(@times,ones(4,numMemSegments),[1 2 2 1]');
                dtRK = [0 (dtRelaxMemInMilliseconds/2) (dtRelaxMemInMilliseconds/2) dtRelaxMemInMilliseconds];
            % Pre-allocate space for the central difference coefficients
                orderOfAccuracy = 8;
                [finiteDiffCoeffs2ndDerCentral, shift2ndDer, shift2ndDerIdx, finiteDiffCoeffs4thDerCentral, shift4thDer, shift4thDerIdx] = ...
                    createRKCoeffs(orderOfAccuracy,numMemSegments);
            % Pre-allocate space for the Runge-Kutta calculations
                slopeValsY = NaN(4,numMemSegments);
            % Pre-allocate space to store the derivatives
                yVals_ddy_xx = nan(length(shift2ndDer),numMemSegments);
                yVals_ddddy_xxxx = nan(length(shift4thDer),numMemSegments);                
                
        % Set up the Brownian forces on the membrane 
            % Calculate the standard deviation of the Brownian force
                xi = 0*sqrt(2*kT*gammaVal/dtRelaxMemInMilliseconds);   
                
    % Load these variables into a structure
        inputVars = v2struct();

end