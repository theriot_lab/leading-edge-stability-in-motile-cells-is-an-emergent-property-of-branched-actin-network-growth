%% This script sets the default simulation parameters

% This code was written by Rikki M. Garner as a companion to the manuscript
% titled "Leading edge stability in motile cells in an emergent property of
% branched actin network growth", and was last updated 2020/08/19.

%% Set up the system

    % Clear the system
        close all; 
        clear all;

    % Choose where to store the parameters
        saveFilePath = ['/Users/Rikki/Documents/LamellipodiumSimulation_ForPublication/'...
            'basicModelParameters.mat'];
     
%% Choose properties not specifically related to either actin or the membrane

    % Choose the properties governing thermal energy in (picoNewton nanometers)
        % Boltzman constant in pN nm / K
            k_B = 0.0138;
        % Temperature in C
            T_C = 37;  
            
    % Choose the total time to sim in milliseconds
        % (note autocorrelation analysis will only run on data collected
        % after 10,000ms of simulated time (when the simulation 
        % reaches steady state), but this can be edited around line 
        % ~1193 of simulateLamellipodiumAndRunXCorrAnalysis.m)
        totalTime2SimInMilliseconds = 100000;      
    % Choose the fraction of the minimum relaxation timescale to set the
    % timestep of the simulation
        fracOfMinTimeScale = 1;
        
%% Choose the membrane properties
 
    % Choose the spatial scales of the membrane
        % Choose the length of the leading edge in nanometers
            leadingEdgeLengthInNanometers = 20000;
        % Choose the height of the membrane    
            leadingEdgeHeightInNanometers = 200;
        % Choose the number of discretized membrane segments to make up the leading edge 
            numMemSegments = 200;
    % Choose the bend/stretch properties
        % Choose whether to calculate these forces at all
            applyMembraneBendStretchForces = true;
        % Choose the stretching constant (in picoNewtons/nanometer)
            sigmaVal = 0.03;
        % Choose the bending constant (in picoNewton nanometers)
            kappaVal = 140;
        % Choose the viscosity (gammaVal) multiplier due to agarose/lipid
        % transport
            gammaMult = 3000;

%% Choose the actin filament properties

    % Choose the physical properties of the filaments
        % Persistence length of the filament in nm
            l_p = 1000;
        % Monomer length in nm
            l_m = 2.7;   
    % Choose the parameters that control linear filament growth
        % Free monomer concentration in uM
            M = 15;
        % Off rate of monomers in ms^(-1)
            k_off = 10^(-3);
        % On rate of monomers during free growth (far from the membrane) in ms^-1 uM^-1
            k_on = 11*10^(-3);
        % Capping rate in ms^(-1)
            k_cap = 3*10^(-3);        
    % Choose the properties that control branching
        % Branching rate in ms^(-1)
            k_branch = 4.5*10^(-5);  
        % Distance from the membrane at which branching no
        % longer occurs (in nm)
            branchWindowSize = 15;
        % (Sigmoidal) sitching strength for the branching cutoff
            branchWindowSwitchStrengthVal = Inf;
        % Branching angle in radians
            branchAngle = 70*pi/180;
        % Standard deviation of branching angle in radians
            branchAngleDeviation = 10*pi/180;
    % Choose whether to allow spreading of filament tips from one
    % membrane segment to another as the filament tip growth
        allowSpread = true;
        
%% Choose the initial filament properties

    % Inital length of each filament in nm
        l_f_0 = 2.7;     
    % Choose the initial y-positions of the filament tips (relative to the
    % membrane, such that when tipYPosRelMemInitial<0, filaments poke the membrane)
        tipYPosRelMemInitial = l_f_0/2;           
    % Choose the initial filament angle distribution
    % (0 for random or 1 for fixed)
        initialFilamentAngleOrder = 0;

%% Choose the external force on each bead 

    % External force in pN (positive pushes with actin force,
    % negative pushes against actin force)
        Fext=0;
    
%% Save the file

    % Save all parameters to a structure and clear all of the remaining variables
        inputVars = v2struct;
        % Clear all other variables
            clearvars -except inputVars

    % Save the file
        save(inputVars.saveFilePath,'-struct','inputVars','-v7.3')
